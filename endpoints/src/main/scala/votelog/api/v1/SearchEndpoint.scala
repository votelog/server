package votelog.api.v1

import cats.effect.IO
import io.circe.{Decoder, Encoder}
import votelog.api.v0.circe.deriveCodec
import sttp.capabilities.fs2.Fs2Streams
import sttp.tapir.*
import sttp.tapir.generic.auto.*
import sttp.tapir.json.circe.jsonBody
import votelog.domain.authentication.Token
import votelog.domain.crudi.ReadOnlyStore.Index
import votelog.domain.data.Paging
import votelog.domain.politics
import votelog.domain.politics.{Business, Context, Language}
import votelog.domain.search.Search.{CuriaVistaDocument, Entity}
import input.{context, maybeLanguage, securedEndpoint}
import votelog.api.v0.circe.domain.search.implicits.curiaVistaDocumentCodec
import votelog.api.v0.circe.implicits.indexCodec
import votelog.api.v0.circe.domain.politics.implicits.*
import SearchEndpoint.id
import votelog.domain.search.Search
import votelog.orphans.tapir.implicits.businessIdentityTextCodec

import java.nio.charset.StandardCharsets
import java.util.UUID
import scala.util.Try

object SearchEndpoint {

  val entityTypeMapping =
    Map(
      Entity.Type.Business -> "business",
      Entity.Type.Bill -> "bill",
      Entity.Type.Parliamentarian -> "parliamentarian",
      Entity.Type.Vote -> "vote",
    )

  val entityTypeAsString: Entity.Type => String = entityTypeMapping.apply
  val entityTypeFromString: String => Entity.Type = entityTypeMapping.map { case (key, value) => (value, key) }.apply

  given Codec[String, Entity.Type, CodecFormat.TextPlain] = Codec.string.map(entityTypeFromString)(entityTypeAsString)

  def toEntity(entityType: Entity.Type, id: String): Entity = entityType match {
    case Entity.Type.Business =>
      Try(Entity.Business(politics.Business.Id(id.toInt)))
        .getOrElse(sys.error(s"invalid value '$id' for business; integer expected"))
    case Entity.Type.Parliamentarian =>
      Try(Entity.Parliamentarian(politics.Parliamentarian.Id(id.toInt)))
        .getOrElse(sys.error(s"invalid value '$id' for parliamentarian; integer expected" ))
    case Entity.Type.Bill =>
      Try(Entity.Bill(politics.Bill.Id(UUID.fromString(id))))
        .getOrElse(sys.error(s"invalid value '$id' for bill; UUID expected"))
    case Entity.Type.Vote =>
      Try(Entity.Vote(politics.Vote.Id(id.toInt)))
        .getOrElse(sys.error(s"invalid value '$id' for vote; integer expected"))
  }

  def fromEntity(entity: Entity): (Entity.Type, String) = entity match {
    case Entity.Bill(politics.Bill.Id(value)) => (Entity.Type.Bill, value.toString)
    case Entity.Parliamentarian(politics.Parliamentarian.Id(value)) => (Entity.Type.Parliamentarian, value.toString)
    case Entity.Business(politics.Business.Id(value)) => (Entity.Type.Business, value.toString)
    case Entity.Vote(politics.Vote.Id(value)) => (Entity.Type.Vote, value.toString)
  }


  val location: EndpointInput[Unit] = root / "search"
  val entityCodec: io.circe.Codec[CuriaVistaDocument] = summon[io.circe.Codec[CuriaVistaDocument]](using curiaVistaDocumentCodec)
  val componentSchema: Schema[CuriaVistaDocument] = Schema.derived
  val indexSchema: Schema[Index[String, CuriaVistaDocument]] = Schema.derived
  val identityCodec: io.circe.Codec[String] = io.circe.Codec.from(Decoder.decodeString, Encoder.encodeString)
  val responseCodec: io.circe.Codec[Index[String, CuriaVistaDocument]] = indexCodec(using entityCodec, identityCodec)

  val id: EndpointInput.Query[String] = query[String]("id")
  val entityType: EndpointInput.Query[Entity.Type] = query[Entity.Type]("type")

  val searchEntityMapping =
    Mapping.from[(Entity.Type, String), Search.Entity]({
      case (entityType, id) =>
        toEntity(entityType, id)
      })({
      case (entity: Search.Entity) =>
        val (entityType, id) = fromEntity(entity)
        (entityType, id)
      }
    )

  val querySearchEntity: EndpointInput[Search.Entity] = (entityType `and` id).map(searchEntityMapping)
  val queryEntityTypes: EndpointInput.Query[Set[Entity.Type]] = query[Set[Entity.Type]]("type")

  val search: Endpoint[Option[Token], (Option[Language], Paging, String), String, Index[String, CuriaVistaDocument], Any] =
    securedEndpoint
      .get
      .in(location)
      .in(maybeLanguage)
      .in(input.paging)
      .in(query[String]("qs"))
      .out(jsonBody[Index[String, CuriaVistaDocument]](responseCodec, responseCodec, indexSchema))

  val indexEntities =
    securedEndpoint
      .post
      .in(location)
      .in(context)
      .in(queryEntityTypes)
      .out(plainBody[String])

  val indexBusinessEntity =
    securedEndpoint
      .post
      .in(location / "-" / "index")
      .in(querySearchEntity)
      .in(context)
      .out(jsonBody[CuriaVistaDocument])
}
