package votelog.api.v1

import sttp.model.{Header, MediaType}
import sttp.tapir.*

object DocumentationEndpoint {

  val location: EndpointInput[Unit] = root / "doc"

  val jsonDocumentation: PublicEndpoint[Unit, Unit, String, Any] =
    endpoint.
      get
      .in( location / "endpoint-documentation.json")
      .out(header(Header.contentType(MediaType.ApplicationJson)))
      .out(stringBody)

  val yamlDocumentation: PublicEndpoint[Unit, Unit, String, Any] =
    endpoint.
      get
      .in( location / "endpoint-documentation.yaml")
      .out(header(Header.contentType(MediaType.TextPlain)))
      .out(stringBody)
}
