package votelog.api
import sttp.tapir.*

package object v1 {
  val version: String = "v1"
  val root: EndpointInput[Unit] = "api" / version
  type StringCodec[A] = Codec[String, A, CodecFormat.TextPlain]
}
