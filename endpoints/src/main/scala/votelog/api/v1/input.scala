package votelog.api.v1

import cats.implicits.*
import sttp.tapir.*
import votelog.domain.authentication.Token
import votelog.domain.data.Paging
import votelog.domain.data.Paging.{Offset, PageSize}
import votelog.domain.politics.{Context, Language, LegislativePeriod}

object input {

  val empty: EndpointIO.Empty[Unit] = EndpointIO.Empty(
    Codec.idPlain(),
    EndpointIO.Info.empty)

  val pageSize: EndpointInput.Query[PageSize] = query[Int]("ps").mapTo[PageSize]
  val offset: EndpointInput.Query[Offset] = query[Long]("os").mapTo[Offset]

  val bearerToken: EndpointInput.Auth[Option[Token],EndpointInput.AuthType.Http] = auth.bearer[Option[String]]().map(p => p.map(Token.apply))(p => p.map(_.value))

  val securedEndpoint: Endpoint[Option[Token], Unit, String, Unit, Any] =
    endpoint
      .securityIn(bearerToken)
      .errorOut(stringBody)

  val paging: EndpointInput[Paging] =
    (offset `and` pageSize).map { case (a, b) => Paging.apply(a, b) }{ p => (p.offset, p.pageSize)}

  val language: EndpointInput.Query[Language] = query[String]("lang")
    .map(Language.fromIso639_1Unsafe)(_.iso639_1.toLowerCase)

  val legislativePeriod: EndpointInput.Query[LegislativePeriod.Id] = query[Int]("lp")
    .map(LegislativePeriod.Id.apply)(_.value)
  
  val context: EndpointInput[Context] = {
    val query: EndpointInput[(LegislativePeriod.Id, Language)] = (legislativePeriod `and` language)

    val encode: ((LegislativePeriod.Id, Language)) => Context =
      (Context.apply).tupled

    val decode: Context => (LegislativePeriod.Id, Language) =
      c => (c.legislativePeriod, c.language)

    query.map(encode)(decode)
  }

  val maybeLanguage: EndpointInput.Query[Option[Language]] = query[Option[String]]("lang")
    .map(q => q.map( a => Language.fromIso639_1Unsafe(a)))(_.map(_.iso639_1))
  /**
   * url parameter query for comma separated ids with key 'ids'
   *
   * @param ev
   * @tparam T
   * @return
   */
  def idsQuery[T](implicit ev: Codec[String, T, CodecFormat.TextPlain]): EndpointInput.Query[Set[T]] =
    query[Option[String]]("ids")
      .mapDecode { s =>
        val results = s.toSeq.map(_.split(",").map(ev.decode))
        DecodeResult.sequence(results.flatten).map(_.toSet)
      }(_.toList.map(ev.encode).toNel.map(_.toList.mkString(",")))

}
