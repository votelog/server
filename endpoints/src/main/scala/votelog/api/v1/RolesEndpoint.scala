package votelog.api.v1


import sttp.tapir.{Codec, CodecFormat, EndpointInput, Schema}
import io.circe.generic.semiauto.*
import sttp.tapir.*
import sttp.tapir.generic.auto.*
import votelog.api.v0.circe.implicits.*
import io.circe.Codec.*
import sttp.tapir.json.circe.jsonBody
import votelog.api.v1.input.securedEndpoint
import votelog.domain.authorization.Role.Permission
import votelog.domain.authorization.{Capability, Component, Role}
import votelog.domain.crudi.ReadOnlyStore
import votelog.domain.data.Paging
import votelog.orphans.tapir.implicits.*

import java.util.UUID



object RolesEndpoint
  extends StoreEndpoint[Role, Role.Id, Role.Template, String, Unit, Paging] {

  override val recipeCodec: io.circe.Codec[Role.Template] = implicitly
  override val recipeSchema: Schema[Role.Template] = Schema.derived
  override val location: EndpointInput[Unit] = root / "role"

  implicit val capabilitySchema: Schema[Capability] = Schema.derived
  implicit val permissionSchema: Schema[Permission] = Schema.derived

  override val entityCodec: io.circe.Codec[Role] = implicitly
  override val entitySchema: Schema[Role] = Schema.derived
  override val partialCodec: io.circe.Codec[String] = io.circe.Codec.from(io.circe.Decoder.decodeString, io.circe.Encoder.encodeString)
  override val partialSchema: Schema[String] = implicitly
  override val identityCodec: io.circe.Codec[Role.Id] = implicitly
  override val identitySchema: Schema[Role.Id] = Schema.derived

  override val readParameter: EndpointInput[Unit] = input.empty
  override val indexParameter: EndpointInput[Paging] = input.paging

  override val partialIndexSchema: Schema[ReadOnlyStore.Index[Role.Id, String]] = Schema.derived

  val grantPermission =
    securedEndpoint
      .put
      .in(location / path[Role.Id]("id")(identityTextCodec) / "-" / "grant-permission")
      .in(jsonBody[Permission])

  val revokePermission =
    securedEndpoint
      .put
      .in(location / path[Role.Id]("id")(identityTextCodec) / "-" / "revoke-permission")
      .in(jsonBody[Permission])

}
