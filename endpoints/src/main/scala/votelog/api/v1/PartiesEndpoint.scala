package votelog.api.v1

import sttp.tapir.*
import sttp.tapir.generic.auto.*
import votelog.domain.crudi.ReadOnlyStore.Index
import votelog.domain.politics.*
import input.{context, language}
import votelog.api.v0.circe.implicits.*
import votelog.orphans.tapir.implicits.*

object PartiesEndpoint
  extends ReadOnlyEndpoint[
    Party,
    Party.Id,
    Party,
    Language,
    Context
  ] {

  override val location: EndpointInput[Unit] = root / "party"

  override val entityCodec: io.circe.Codec[Party] = implicitly
  override val entitySchema: Schema[Party] = implicitly
  override val partialCodec: io.circe.Codec[Party] = implicitly
  override val partialSchema: Schema[Party] = implicitly
  override val identityCodec: io.circe.Codec[Party.Id] = implicitly
  override val identitySchema: Schema[Party.Id] = implicitly

  override val readParameter: EndpointInput[Language] = language
  override val indexParameter: EndpointInput[Context] = context

  override val partialIndexSchema: Schema[Index[Party.Id, Party]] = Schema.derived
  override val identityTextCodec: Codec[String, Party.Id, CodecFormat.TextPlain] = implicitly
}
