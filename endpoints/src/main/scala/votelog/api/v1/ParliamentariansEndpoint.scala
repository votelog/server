package votelog.api.v1

import sttp.tapir.*
import sttp.tapir.generic.auto.*
import votelog.domain.authentication.Token
import votelog.domain.crudi.ReadOnlyStore.{Index, IndexQueryParameters}
import votelog.domain.politics.*
import votelog.api.TapirCirceCoding.*
import input.{context, language, securedEndpoint}
import votelog.api.v0.circe.implicits.*
import votelog.orphans.tapir.implicits.*


object ParliamentariansEndpoint
  extends ReadOnlyEndpoint[
    Parliamentarian,
    Parliamentarian.Id,
    PersonPartial,
    Language,
    IndexQueryParameters[Context, Parliamentarian.Field, Parliamentarian.Field]
  ] {

  override val location: EndpointInput[Unit] = root / "parliamentarian"

  override val entityCodec: io.circe.Codec[Parliamentarian] = implicitly
  override val entitySchema: Schema[Parliamentarian] = implicitly
  override val partialCodec: io.circe.Codec[PersonPartial] = implicitly
  override val partialSchema: Schema[PersonPartial] = implicitly
  override val identityCodec: io.circe.Codec[Parliamentarian.Id] = implicitly
  override val identitySchema: Schema[Parliamentarian.Id] = implicitly

  override val readParameter: EndpointInput[Language] = language
  override val indexParameter: EndpointInput[IndexQueryParameters[Context, Parliamentarian.Field, Parliamentarian.Field]] =
    buildPartialIndexParameter[Context, Parliamentarian.Field](context)

  override val partialIndexSchema: Schema[Index[Parliamentarian.Id, PersonPartial]] = Schema.derived
  override val identityTextCodec: Codec[String, Parliamentarian.Id, CodecFormat.TextPlain] = implicitly

  lazy val readDecisions: Endpoint[Option[Token], (Context, Parliamentarian.Id), String, List[(Vote.Id, Decision)], Any] =
    securedEndpoint
      .get
      .in(context)
      .in(location / path[Parliamentarian.Id]("id")(identityTextCodec) / "decisions")
      .out(jsonBody[List[(Vote.Id, Decision)]])
}
