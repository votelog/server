package votelog.api.v1

import sttp.tapir.*
import sttp.tapir.generic.auto.*
import votelog.domain.crudi.ReadOnlyStore.{Index, PagedIndexQueryParameters}
import votelog.domain.politics.*
import input.{context, idsQuery, language, securedEndpoint}
import votelog.api.TapirCirceCoding.jsonBody
import votelog.api.v0.circe.implicits.*
import votelog.domain.authentication.Token
import votelog.orphans.tapir.implicits.*

object VotesEndpoint
  extends ReadOnlyEndpoint[
    Vote,
    Vote.Id,
    Vote.Partial,
    Language,
    PagedIndexQueryParameters[Context],
  ] {

  override val location: EndpointInput[Unit] = root / "vote"

  override val entityCodec: io.circe.Codec[Vote] = implicitly
  override val entitySchema: Schema[Vote] = implicitly
  override val partialCodec: io.circe.Codec[Vote.Partial] = implicitly
  override val partialSchema: Schema[Vote.Partial] = implicitly
  override val identityCodec: io.circe.Codec[Vote.Id] = implicitly
  override val identitySchema: Schema[Vote.Id] = implicitly

  override val readParameter: EndpointInput[Language] = language

  override val indexParameter: EndpointInput[PagedIndexQueryParameters[Context]] =
    buildPagedIndexParameter(context)

  lazy val fetchByIds: Endpoint[Option[Token], (Language, Set[Vote.Id]), String, List[(Vote.Id, Vote)], Any] =
    securedEndpoint
      .get
      .in(readParameter)
      .in(idsQuery[Vote.Id])
      .in(location / "-" / "byIds")
      .out(jsonBody[List[(Vote.Id, Vote)]])

  override val partialIndexSchema: Schema[Index[Vote.Id, Vote.Partial]] = Schema.derived
  override val identityTextCodec: Codec[String, Vote.Id, CodecFormat.TextPlain] = implicitly
}
