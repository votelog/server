package votelog.api.v1

import io.circe
import sttp.tapir.*
import sttp.tapir.generic.auto.*
import sttp.tapir.json.circe.jsonBody
import votelog.domain.authentication.User
import votelog.domain.crudi.ReadOnlyStore.{Index, IndexQueryParameters}
import votelog.api.v0.circe.implicits.*
import votelog.domain.authorization.Role
import votelog.orphans.tapir.implicits.*
import votelog.persistence.UserStore
import votelog.persistence.UserStore.Password

object UsersEndpoint
  extends ReadOnlyEndpoint[User, User.Id, User.Partial, Unit, IndexQueryParameters[Unit, User.Field, User.Field]]
    with StoreEndpoint[User, User.Id, UserStore.Recipe, User.Partial, Unit, IndexQueryParameters[Unit, User.Field, User.Field]] {

  import input.*

  override val location: EndpointInput[Unit] = root / "user"

  implicit val passwordSchema: Schema[Password.Clear] = Schema.derived
  implicit val emailSchema: Schema[User.Email] = Schema.derived

  override val entityCodec: io.circe.Codec[User] = userCodec
  override val entitySchema: Schema[User] = implicitly
  override val partialCodec: io.circe.Codec[User.Partial] = implicitly
  override val partialSchema: Schema[User.Partial] = implicitly
  override val identityCodec: io.circe.Codec[User.Id] = implicitly
  override val identitySchema: Schema[User.Id] = implicitly

  override val readParameter: EndpointInput[Unit] = input.empty
  override val indexParameter: EndpointInput[IndexQueryParameters[Unit, User.Field, User.Field]] =
    buildPartialIndexParameter(input.empty)

  override val partialIndexSchema: Schema[Index[User.Id, User.Partial]] = Schema.derived
  override val identityTextCodec: Codec[String, User.Id, CodecFormat.TextPlain] = implicitly
  override val recipeCodec: circe.Codec[UserStore.Recipe] = implicitly

  override val recipeSchema: Schema[UserStore.Recipe] = Schema.derived

  val findByName =
    securedEndpoint
      .get
      .in(location / "-" / "search")
      .in(query[String]("name"))
      .out(jsonBody[Option[(User.Id, User)]])

  val grantRole =
    securedEndpoint
      .put
      .in(location / path[User.Id]("id")(identityTextCodec) / "-" / "grant-role")
      .in(jsonBody[Role.Id])

  val revokeRole =
    securedEndpoint
      .put
      .in(location / path[User.Id]("id")(identityTextCodec) / "-" / "revoke-role")
      .in(jsonBody[Role.Id])

}
