package votelog.api.v1

import sttp.tapir.*
import sttp.tapir.generic.auto.*
import votelog.domain.crudi.ReadOnlyStore.Index
import votelog.domain.politics.*
import input.{context, language}
import votelog.api.v0.circe.implicits.*
import votelog.orphans.tapir.implicits.*

object FactionsEndpoint
  extends ReadOnlyEndpoint[
    Faction,
    Faction.Id,
    Faction,
    Language,
    Context
  ] {

  override val location: EndpointInput[Unit] = root / "faction"

  override val entityCodec: io.circe.Codec[Faction] = implicitly
  override val entitySchema: Schema[Faction] = implicitly
  override val partialCodec: io.circe.Codec[Faction] = implicitly
  override val partialSchema: Schema[Faction] = implicitly
  override val identityCodec: io.circe.Codec[Faction.Id] = implicitly
  override val identitySchema: Schema[Faction.Id] = implicitly

  override val readParameter: EndpointInput[Language] = language
  override val indexParameter: EndpointInput[Context] = context

  override val partialIndexSchema: Schema[Index[Faction.Id, Faction]] = Schema.derived
  override val identityTextCodec: Codec[String, Faction.Id, CodecFormat.TextPlain] = implicitly
}
