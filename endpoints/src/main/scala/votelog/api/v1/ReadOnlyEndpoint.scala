package votelog.api.v1

import io.circe
import sttp.tapir.{Codec, CodecFormat, Endpoint, EndpointInput, Schema, path}
import votelog.domain.authentication.Token
import votelog.domain.crudi.ReadOnlyStore.Index
import input.securedEndpoint
import votelog.api.TapirCirceCoding.*
import votelog.api.v0.circe.implicits.indexCodec

trait ReadOnlyEndpoint[
  Entity,
  Identity: StringCodec,
  Partial,
  ReadParameter,
  IndexParameter,
] {

  val location: EndpointInput[Unit]
  val identityTextCodec: Codec[String, Identity, CodecFormat.TextPlain] = implicitly
  val entityCodec: circe.Codec[Entity]
  val entitySchema: Schema[Entity]

  val partialCodec: circe.Codec[Partial]
  val partialSchema: Schema[Partial]

  val identityCodec: io.circe.Codec[Identity]
  val identitySchema: Schema[Identity]

  val readParameter: EndpointInput[ReadParameter]
  val indexParameter: EndpointInput[IndexParameter]
  lazy val partialIndexCodec: circe.Codec[Index[Identity, Partial]] =
    indexCodec(using partialCodec, identityCodec)

  val partialIndexSchema: Schema[Index[Identity, Partial]]

  lazy val read: Endpoint[Option[Token], (ReadParameter, Identity), String, Entity, Any] =
      securedEndpoint
        .get
        .in(readParameter)
        .in(location / path[Identity]("id")(implicitly))
        .out(jsonBody[Entity](entityCodec, entityCodec, entitySchema))

    lazy val index: Endpoint[Option[Token], IndexParameter, String, Index[Identity, Partial], Any] =
      securedEndpoint
        .get
        .in(location)
        .in(indexParameter)
        .out(jsonBody[Index[Identity, Partial]](partialIndexCodec, partialIndexCodec, partialIndexSchema))

    lazy val initialIndex: Endpoint[Option[Token], Unit, String, Index[Identity, Partial], Any] =
      securedEndpoint
        .get
        .in(location)
        .out(jsonBody[Index[Identity, Partial]](partialIndexCodec, partialIndexCodec, partialIndexSchema))
}
