package votelog.api.v1

import sttp.tapir.*
import sttp.tapir.generic.auto.*
import votelog.domain.authentication.Token
import votelog.domain.crudi.ReadOnlyStore.{Index, IndexQueryParameters}
import votelog.domain.politics.*
import votelog.api.TapirCirceCoding.*
import input.{context, idsQuery, language, securedEndpoint}
import votelog.api.v0.circe.implicits.*
import votelog.orphans.tapir.implicits.*

object BusinessesEndpoint
  extends ReadOnlyEndpoint[
    Business,
    Business.Id,
    Business.Partial,
    Language,
    IndexQueryParameters[Context, Business.Field, Business.Field]
  ] {

  override val location: EndpointInput[Unit] = root / "business"

  override val entityCodec: io.circe.Codec[Business] = implicitly
  override val entitySchema: Schema[Business] = implicitly
  override val partialCodec: io.circe.Codec[Business.Partial] = implicitly
  override val partialSchema: Schema[Business.Partial] = implicitly
  override val identityCodec: io.circe.Codec[Business.Id] = implicitly
  override val identitySchema: Schema[Business.Id] = implicitly

  override val readParameter: EndpointInput[Language] = language
  override val indexParameter: EndpointInput[IndexQueryParameters[Context, Business.Field, Business.Field]] =
    buildPartialIndexParameter[Context, Business.Field](context)

  override val partialIndexSchema: Schema[Index[Business.Id, Business.Partial]] = Schema.derived
  override val identityTextCodec: Codec[String, Business.Id, CodecFormat.TextPlain] = implicitly

  lazy val billsForBusiness: Endpoint[Option[Token], (Context, Business.Id), String, List[(Bill.Id, Bill.Partial)], Any] =
    securedEndpoint
      .get
      .in(context)
      .in(location / path[Business.Id]("id")(identityTextCodec) / "bills")
      .out(jsonBody[ List[(Bill.Id, Bill.Partial)]])


  lazy val fetchByIds: Endpoint[Option[Token], (Language, Set[Business.Id]), String, List[(Business.Id, Business)], Any] =
    securedEndpoint
      .get
      .in(readParameter)
      .in(idsQuery[Business.Id])
      .in(location / "-" / "byIds")
      .out(jsonBody[List[(Business.Id, Business)]])

}
