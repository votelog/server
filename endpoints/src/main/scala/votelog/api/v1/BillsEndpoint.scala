package votelog.api.v1

import cats.*
import cats.implicits.*
import sttp.tapir.*
import sttp.tapir.generic.auto.*
import votelog.domain.authentication.Token
import votelog.domain.crudi.ReadOnlyStore.{Index, IndexQueryParameters}
import votelog.domain.politics.*
import votelog.api.TapirCirceCoding.jsonBody
import input.{context, idsQuery, language, securedEndpoint}
import votelog.api.v0.circe.implicits.*
import votelog.orphans.tapir.implicits.*


object BillsEndpoint
  extends ReadOnlyEndpoint[
    Bill,
    Bill.Id,
    Bill.Partial,
    Language,
    IndexQueryParameters[Context, Bill.Field, Bill.Field]
  ] {

  override val location: EndpointInput[Unit] = root / "bill"

  implicit val votesForBillSchema: Schema[Map[Vote.Id, Vote.Partial]] =
    Schema.schemaForMap[Vote.Id, Vote.Partial](_.value.toString)

  override val entityCodec: io.circe.Codec[Bill] = implicitly
  override val entitySchema: Schema[Bill] = implicitly
  override val partialCodec: io.circe.Codec[Bill.Partial] = implicitly
  override val partialSchema: Schema[Bill.Partial] = implicitly
  override val identityCodec: io.circe.Codec[Bill.Id] = implicitly
  override val identitySchema: Schema[Bill.Id] = implicitly

  override val readParameter: EndpointInput[Language] = language
  override val indexParameter: EndpointInput[IndexQueryParameters[Context, Bill.Field, Bill.Field]] =
    buildPartialIndexParameter[Context, Bill.Field](context)

  override val partialIndexSchema: Schema[Index[Bill.Id, Bill.Partial]] = Schema.derived
  override val identityTextCodec: Codec[String, Bill.Id, CodecFormat.TextPlain] = implicitly

  lazy val votesForBill: Endpoint[Option[Token], (Language, Bill.Id), String, Map[Vote.Id, Vote.Partial], Any] =
    securedEndpoint
      .get
      .in(readParameter)
      .in(location / path[Bill.Id]("id")(identityTextCodec) / "votes")
      .out(jsonBody[Map[Vote.Id, Vote.Partial]])

  lazy val fetchByIds: Endpoint[Option[Token], (Language, Set[Bill.Id]), String, List[(Bill.Id, Bill)], Any] =
    securedEndpoint
      .get
      .in(readParameter)
      .in(idsQuery[Bill.Id])
      .in(location / "-" / "byIds")
      .out(jsonBody[List[(Bill.Id, Bill)]])
}
