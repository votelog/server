package votelog.api.v1

import sttp.tapir.generic.auto.*
import sttp.tapir.json.circe.jsonBody
import sttp.tapir.model.UsernamePassword
import votelog.domain.authentication.{Token, User}
import sttp.tapir.*
import votelog.api.v0.circe.implicits.*
import votelog.orphans.tapir.implicits.*
import input.securedEndpoint

object SessionEndpoint {

  val location: EndpointInput[Unit] = root / "session"

  val read: Endpoint[Option[Token], Unit, String, (User, Token), Any] =
    securedEndpoint
      .get
      .in(location)
      .out(jsonBody[(User, Token)])

  val delete: Endpoint[Option[Token], Unit, String, Unit, Any] =
    securedEndpoint
      .delete
      .in(location)

  val createSession: Endpoint[Unit, UsernamePassword, String, (User, Token), Any] =
    endpoint
      .post
      .in(location)
      .in(auth.basic[UsernamePassword]())
      .out(jsonBody[(User, Token)])
      .errorOut(stringBody)
}
