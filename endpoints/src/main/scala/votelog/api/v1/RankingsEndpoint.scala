package votelog.api.v1

import sttp.tapir.generic.auto.*
import votelog.api.v0.circe.implicits.*
import votelog.orphans.tapir.implicits.*
import sttp.tapir.*
import sttp.tapir.json.circe.jsonBody
import votelog.domain.authentication.Token
import votelog.domain.politics.{LegislativePeriod, Ngo, Parliamentarian}
import votelog.domain.politics.Rankings.{OrganisationRankingResult, OrganisationRankings, ParliamentarianRankingResult, ParliamentarianRankings}
import input.*

object RankingsEndpoint {
  implicit val rankingMapSchema: Schema[Map[Parliamentarian.Id, OrganisationRankingResult]] =
    Schema.schemaForMap[Parliamentarian.Id, OrganisationRankingResult](_.value.toString)
  implicit val rankingSchema: Schema[OrganisationRankings] = Schema.derived

  implicit val rankingsForParliamentarianMapSchema: Schema[Map[Ngo.Id, ParliamentarianRankingResult]] =
    Schema.schemaForMap[Ngo.Id, ParliamentarianRankingResult](_.value.toString)
  implicit val parliamentarianRankingSchema: Schema[ParliamentarianRankings] = Schema.derived

  val rankings: Endpoint[Option[Token], (Ngo.Id, LegislativePeriod.Id), String, OrganisationRankings, Any] =
    securedEndpoint
      .get
      .in(root / "ranking" / path[Ngo.Id])
      .in(legislativePeriod)
      .out(jsonBody[OrganisationRankings])

  val rankingsForParliamentarian: Endpoint[Option[Token], (Parliamentarian.Id, LegislativePeriod.Id), String, ParliamentarianRankings, Any] =
    securedEndpoint
      .get
      .in(root / "ranking" / "-" / "byParliamentarian" / path[Parliamentarian.Id])
      .in(legislativePeriod)
      .out(jsonBody[ParliamentarianRankings])
}
