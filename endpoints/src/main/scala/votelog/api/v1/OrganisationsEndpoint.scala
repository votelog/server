package votelog.api.v1

import io.circe
import sttp.tapir.*
import sttp.tapir.generic.auto.*
import votelog.domain.crudi.ReadOnlyStore.{Index, IndexQueryParameters}
import votelog.domain.politics.*
import votelog.api.TapirCirceCoding.jsonBody
import input.{legislativePeriod, securedEndpoint}
import votelog.api.v0.circe.deriveCodec
import votelog.api.v0.circe.implicits.*
import votelog.domain.organisation.OrganisationService
import votelog.orphans.tapir.implicits.*
import votelog.orphans.tapir.implicits.slugCodec
import votelog.persistence.OrganisationStore
import votelog.persistence.OrganisationStore.DecisionPreference
import votelog.persistence.UserStore.Password
import votelog.util.Slug
import votelog.domain.authentication.Token
import votelog.domain.data.Paging

object OrganisationsEndpoint
  extends ReadOnlyEndpoint[Ngo, Ngo.Id, Ngo.Partial, Unit, IndexQueryParameters[Unit, Ngo.Field, Ngo.Field]]
    with StoreEndpoint[Ngo, Ngo.Id, OrganisationStore.Recipe, Ngo.Partial, Unit, IndexQueryParameters[Unit, Ngo.Field, Ngo.Field]] {

  override val location: EndpointInput[Unit] = root / "organisation"

  override val entityCodec: io.circe.Codec[Ngo] = implicitly
  override val entitySchema: Schema[Ngo] = implicitly
  override val partialCodec: io.circe.Codec[Ngo.Partial] = implicitly
  override val partialSchema: Schema[Ngo.Partial] = implicitly
  override val identityCodec: io.circe.Codec[Ngo.Id] = implicitly
  override val identitySchema: Schema[Ngo.Id] = implicitly

  override val readParameter: EndpointInput[Unit] = input.empty
  override val indexParameter: EndpointInput[IndexQueryParameters[Unit, Ngo.Field, Ngo.Field]] =
    buildPartialIndexParameter(input.empty)

  override val partialIndexSchema: Schema[Index[Ngo.Id, Ngo.Partial]] = Schema.derived
  override val identityTextCodec: Codec[String, Ngo.Id, CodecFormat.TextPlain] = implicitly
  override val recipeCodec: io.circe.Codec[OrganisationStore.Recipe] = implicitly

  override val recipeSchema: Schema[OrganisationStore.Recipe] = Schema.derived

  implicit val serviceUserRecipeCodec: circe.Codec[OrganisationService.Recipe.User] = deriveCodec
  implicit val serviceOrganisationRecipeCodec: circe.Codec[OrganisationService.Recipe.Organisation] = deriveCodec
  implicit val serviceRecipeCodec: circe.Codec[OrganisationService.Recipe] = deriveCodec
  implicit val preferencesSchema: Schema[Map[Vote.Id, DecisionPreference]] =
    Schema.schemaForMap[Vote.Id, DecisionPreference](_.value.toString)

  val indexPreferences: Endpoint[Option[Token], (LegislativePeriod.Id, Ngo.Id), String,List[(Vote.Id, DecisionPreference)],Any] =
    securedEndpoint
      .get
      .in(legislativePeriod)
      .in(location / path[Ngo.Id]("id")(identityTextCodec) / "preferences")
      .out(jsonBody[List[(Vote.Id, DecisionPreference)]])

  val bySlug: Endpoint[Option[Token],Slug,String,(Ngo.Id, Ngo),Any] =
    securedEndpoint
      .get
      .in(location / "-" / "bySlug" / path[Slug]("slug"))
      .out(jsonBody[(Ngo.Id, Ngo)])

  val readPreference: Endpoint[Option[Token],(Ngo.Id, Vote.Id),String,DecisionPreference,Any] =
    securedEndpoint
      .get
      .in(location / path[Ngo.Id]("id")(identityTextCodec) / "preferences" / path[Vote.Id])
      .out(jsonBody[DecisionPreference])

  val updatePreference: Endpoint[Option[Token],(Ngo.Id, Vote.Id, Decision),String,Unit,Any] =
    securedEndpoint
      .put
      .in(location / path[Ngo.Id]("id")(identityTextCodec) / "preferences" / path[Vote.Id])
      .in(jsonBody[Decision])

  val deletePreference: Endpoint[Option[Token], (Ngo.Id, Vote.Id), String, Unit, Any] =
    securedEndpoint
      .delete
      .in(location / path[Ngo.Id]("id")(identityTextCodec) / "preferences" / path[Vote.Id])

  val createOrganisation =
    securedEndpoint
      .post
      .in(location / "-" / "new")
      .in(jsonBody[OrganisationService.Recipe])
      .out(jsonBody[Ngo.Id])
}
