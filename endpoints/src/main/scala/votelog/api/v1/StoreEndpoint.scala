package votelog.api.v1

import sttp.tapir.{Endpoint, Schema, path}
import votelog.domain.authentication.Token
import votelog.api.TapirCirceCoding.jsonBody
import input.securedEndpoint

trait StoreEndpoint[
  Entity,
  Identity,
  Recipe: io.circe.Codec: Schema,
  Partial,
  ReadParameter,
  IndexParameter,
] extends ReadOnlyEndpoint[Entity, Identity, Partial, ReadParameter, IndexParameter] {

  val recipeCodec: io.circe.Codec[Recipe] = implicitly
  val recipeSchema: Schema[Recipe] = implicitly

  lazy val create: Endpoint[Option[Token], Recipe, String, Identity, Any] =
    securedEndpoint
      .post
      .in(location)
      .in(jsonBody[Recipe](recipeCodec, recipeCodec, recipeSchema))
      .out(jsonBody[Identity](identityCodec, identityCodec, identitySchema))

  lazy val update: Endpoint[Option[Token], (Identity, Recipe), String, Entity, Any] =
    securedEndpoint
      .put
      .in(location / path[Identity]("id")(identityTextCodec))
      .in(jsonBody[Recipe](recipeCodec, recipeCodec, recipeSchema))
      .out(jsonBody[Entity](entityCodec, entityCodec, entitySchema))

  lazy val delete: Endpoint[Option[Token], Identity, String, Unit, Any] =
    securedEndpoint
      .get
      .in(location / path[Identity]("id")(identityTextCodec))

}
