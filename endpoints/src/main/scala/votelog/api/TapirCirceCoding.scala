package votelog.api

import io.circe.Printer
import sttp.tapir.json.circe.TapirJsonCirce

object TapirCirceCoding extends TapirJsonCirce {
  override def jsonPrinter: Printer = Printer.noSpaces.copy(dropNullValues = true)
}