package votelog.orphans.tapir

import sttp.tapir.Codec.set
import sttp.tapir.{Codec, CodecFormat, DecodeResult, EndpointInput, query}
import votelog.api.v1.input.{offset, pageSize}
import sttp.tapir.{ValidationResult, Validator}
import votelog.domain.crudi.ReadOnlyStore.{IndexQueryParameters, PagedIndexQueryParameters}
import votelog.domain.data.Paging
import votelog.domain.data.Paging.{Offset, PageSize}
import votelog.domain.data.Sorting.Direction
import votelog.orphans.tapir.domain.authentication.{RoleInstances, UserInstances}
import votelog.orphans.tapir.domain.politics.*
import votelog.orphans.tapir.domain.search.SearchInstances
import votelog.util.Slug

object implicits
  extends BillInstances,
    BusinessInstances,
    FactionInstances,
    NgoInstances,
    PartyInstances,
    PersonInstances,
    RoleInstances,
    UserInstances,
    VoteInstances,
    SearchInstances {

  implicit val slugCodec: Codec[String, Slug, CodecFormat.TextPlain] =
    Codec.string
      .map(Slug.slugify)(_.value)
      .validate(
        Validator.custom { raw =>
          val slugified = Slug.slugify(raw.value)
          if (slugified == raw) ValidationResult.Valid
          else ValidationResult.Invalid(s"$raw is not a proper slug value since it can be reduced to $slugified")
        }
    )

  def buildPagedIndexParameter[Context](
    contextParam: EndpointInput[Context]
  ):  EndpointInput[PagedIndexQueryParameters[Context]] = {

    val decode: ((Offset, PageSize, Context)) => PagedIndexQueryParameters[Context] = {
      case (offset: Offset, pageSize: PageSize, context) =>
        PagedIndexQueryParameters(Paging(offset, pageSize), context)
    }

    val encode: (PagedIndexQueryParameters[Context]) => (Offset, PageSize, Context) =
      (p: PagedIndexQueryParameters[Context]) =>
        (p.paging.offset, p.paging.pageSize, p.context)

    val params: EndpointInput[(Offset, PageSize, Context)] =
      offset
        `and` pageSize
        `and` contextParam

    params.map(decode)(encode)

  }

  def buildPartialIndexParameter[Context, Field](
    contextParam: EndpointInput[Context],
  )(
    implicit ev0: Codec[String, Field, CodecFormat.TextPlain],
  ): EndpointInput[IndexQueryParameters[Context, Field, Field]] = {

    val params: EndpointInput[(List[(Field, Direction)], PageSize, Offset, Context, Set[Field])] =
      orderingParam[Field]
        `and` pageSize
        `and` offset
        `and` contextParam
        `and` fieldsParam(set[String, Field, CodecFormat.TextPlain])

    val decode: ((List[(Field, Direction)], PageSize, Offset, Context, Set[Field])) => IndexQueryParameters[Context, Field, Field] = {
      case (ordering, pageSize, offset, context, fields) =>
        IndexQueryParameters[Context, Field, Field](Paging(offset, pageSize), context, ordering, fields)
    }

    val encode: IndexQueryParameters[Context, Field, Field] => (List[(Field, Direction)], PageSize, Offset, Context, Set[Field]) =
      (params: IndexQueryParameters[Context, Field, Field]) =>
        (params.orderings, params.paging.pageSize, params.paging.offset, params.indexContext, params.fields)

    params.map(decode)(encode)
  }

  def fieldsParam[Field](implicit ev: Codec[List[String], Set[Field], CodecFormat.TextPlain]): EndpointInput.Query[Set[Field]] =
    query[Set[Field]]("fields")

  def orderingFromString[T](s: String)(implicit ev: Codec[String, T, CodecFormat.TextPlain]): DecodeResult[(T, Direction)] =
    s.split("/").toList match {
      case Nil => DecodeResult.Missing
      case _ :: Nil => DecodeResult.Missing
      case ordering :: direction :: Nil =>
        for {
          ordering <- ev.decode(ordering)
          direction <- decodeDirection(direction)
        } yield (ordering, direction)
      case otherwise => DecodeResult.Multiple(otherwise)
    }

  def decodeDirection(s: String): DecodeResult[Direction] = s match {
    case "Desc" => DecodeResult.Value(Direction.Descending)
    case "Asc" => DecodeResult.Value(Direction.Ascending)
    case otherwise => DecodeResult.Error(otherwise, new Throwable("unknown value"))
  }

  def encodeDirection(d: Direction): String = d match {
    case Direction.Descending => "Desc"
    case Direction.Ascending => "Asc"
  }

  def orderingParam[T](implicit ev: Codec[String, T, CodecFormat.TextPlain]): EndpointInput.Query[List[(T, Direction)]] =
    query[List[String]]("orderBy")
      .mapDecode
      { strings =>
        val encoded = strings.map( s => orderingFromString[T](s))
        DecodeResult.sequence(encoded).map(_.toList)
      }
      { values =>
        values.map { case (order, direction) => ev.encode(order) + "/" + encodeDirection(direction) }
      }

}
