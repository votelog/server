package votelog.orphans.tapir.domain.politics

import sttp.tapir.{Codec, CodecFormat, DecodeResult, ValidationError}
import votelog.domain.politics.Bill

import scala.util.Try

trait BillInstances {

  implicit val billIdentityTextCodec: Codec[String, Bill.Id, CodecFormat.TextPlain] = Codec.uuid.map(i => Bill.Id(i))(_.value)

  implicit val billFieldTextCodec: Codec[String, Bill.Field, CodecFormat.TextPlain] = Codec.string.mapDecode {
    in => Try(Bill.Field.unsafeFromString(in))
      .map(DecodeResult.Value.apply).getOrElse(DecodeResult.Error(in, new Throwable("invalid value")))
  }{ _.toString }

}
