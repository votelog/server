package votelog.orphans.tapir.domain.politics

import sttp.tapir.{Codec, CodecFormat, DecodeResult, ValidationError}
import votelog.domain.politics.Parliamentarian

import scala.util.Try

trait PersonInstances {

  implicit val personIdentityTextCodec: Codec[String, Parliamentarian.Id, CodecFormat.TextPlain] = Codec.int.map(i => Parliamentarian.Id(i))(_.value)

  implicit val personFieldCodec: Codec[String, Parliamentarian.Field, CodecFormat.TextPlain] = Codec.string.mapDecode {
    in => Try(Parliamentarian.Field.unsafeFromString(in))
      .map(DecodeResult.Value.apply).getOrElse(DecodeResult.Error(in, new Throwable("invalid value")))
  }{ _.toString }

}
