package votelog.orphans.tapir.domain.politics

import sttp.tapir.{Codec, CodecFormat, DecodeResult, ValidationError}
import votelog.domain.politics.Business

import scala.util.Try

trait BusinessInstances {

  implicit val businessIdentityTextCodec: Codec[String, Business.Id, CodecFormat.TextPlain] = Codec.int.map(i => Business.Id(i))(_.value)

  implicit val businessFieldTextCodec: Codec[String, Business.Field, CodecFormat.TextPlain] = Codec.string.mapDecode {
    in => Try(Business.Field.unsafeFromString(in))
      .map(DecodeResult.Value.apply)
      .getOrElse(DecodeResult.Error(in, new Throwable(s"invalid value: expected one of ${Business.Field.values.mkString(",")}")))
  }{ _.toString }

}
