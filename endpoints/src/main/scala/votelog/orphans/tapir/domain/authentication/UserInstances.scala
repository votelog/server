package votelog.orphans.tapir.domain.authentication

import sttp.tapir.{Codec, CodecFormat, DecodeResult, Schema, ValidationError}
import votelog.domain.authentication.User
import votelog.domain.authorization.Component

import scala.util.Try

trait UserInstances {

  implicit val componentSchema: Schema[Component] = Schema.schemaForString.map(s => Some(Component(s)))(_.location)

  implicit val userIdentityTextCodec: Codec[String, User.Id, CodecFormat.TextPlain] = Codec.uuid.map(i => User.Id(i))(_.value)

  implicit val userFieldTextCodec: Codec[String, User.Field, CodecFormat.TextPlain] = Codec.string.mapDecode {
    in =>
      val valid = User.Field.values.map(_.toString).mkString("", ",", "and")

      Try(User.Field.unsafeFromString(in))
        .map(DecodeResult.Value.apply).getOrElse(DecodeResult.Error(in, new Throwable(s"invalid value '$in' for user field. Expected: $valid")))
  } {
    _.toString
  }

}
