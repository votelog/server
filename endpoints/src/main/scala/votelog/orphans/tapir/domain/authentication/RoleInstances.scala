package votelog.orphans.tapir.domain.authentication

import sttp.tapir.{Codec, CodecFormat}
import votelog.domain.authorization.Role

import java.util.UUID

trait RoleInstances {
  implicit val roleIdTextIdentityCodec: Codec[String, Role.Id, CodecFormat.TextPlain] =
    implicitly[Codec[String, UUID, CodecFormat.TextPlain]]
      .map(uuid => Role.Id(uuid))(_.value)
}
