package votelog.orphans.tapir.domain.politics

import sttp.tapir.{Codec, CodecFormat, DecodeResult}
import votelog.domain.politics.Vote

import java.util.UUID
import scala.util.Try

trait VoteInstances {
  implicit val voteIdentityTextCodec: Codec[String, Vote.Id, CodecFormat.TextPlain] = {
    Codec.string.mapDecode {
      i => DecodeResult.fromEitherString(s"i", Try(i.toInt).toEither.left.map(_.getMessage).map(Vote.Id.apply))
    }(_.value.toString)
  }
}
