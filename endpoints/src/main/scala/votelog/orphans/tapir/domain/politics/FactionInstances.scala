package votelog.orphans.tapir.domain.politics

import sttp.tapir.{Codec, CodecFormat}
import votelog.domain.politics.Faction

trait FactionInstances {
  implicit val factionIdentityTextCodec: Codec[String, Faction.Id, CodecFormat.TextPlain] = Codec.int.map(i => Faction.Id(i))(_.value)
}
