package votelog.orphans.tapir.domain.politics

import sttp.tapir.{Codec, CodecFormat}
import votelog.domain.politics.Party

trait PartyInstances {

  implicit val partyIdentityTextCodec: Codec[String, Party.Id, CodecFormat.TextPlain] = Codec.int.map(i => Party.Id(i))(_.value)
}
