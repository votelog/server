package votelog.orphans.tapir.domain.politics

import sttp.tapir.{Codec, CodecFormat, DecodeResult, ValidationError}
import votelog.domain.politics.Ngo

import java.util.UUID
import scala.util.Try

trait NgoInstances {

  implicit val ngoIdentityTextCodec: Codec[String, Ngo.Id, CodecFormat.TextPlain] = Codec.uuid.map(Ngo.Id.apply)(_.value)

  implicit val ngoFieldTextCodec: Codec[String, Ngo.Field, CodecFormat.TextPlain] = Codec.string.mapDecode {
    in => Try(Ngo.Field.unsafeFromString(in))
      .map(DecodeResult.Value.apply).getOrElse(DecodeResult.Error(in, new Throwable("invalid for field")))
  }{ _.toString }

}
