# VoteLog

[![Health](https://status.votelog.ch/api/v1/endpoints/backend_api-(get--business-id-bills)/health/badge.svg)](https://status.votelog.ch/endpoints/backend_api-(get--business-id-bills))
[![Uptime (24h)](https://status.votelog.ch/api/v1/endpoints/backend_api-(get--business-id-bills)/uptimes/24h/badge.svg)](https://status.votelog.ch/endpoints/backend_api-(get--business-id-bills))
[![Response time (24h)](https://status.votelog.ch/api/v1/endpoints/backend_api-(get--business-id-bills)/response-times/24h/badge.svg)](https://status.votelog.ch/endpoints/backend_api-(get--business-id-bills))

**`votelog`** is a web application that provides a convenient interface for the Swiss parliaments's [Curia Vista database](https://www.parlament.ch/en/ratsbetrieb/curia-vista). Its code is divided into three main components:

- The **backend** server included in this repository.
- The web frontend which is currently live on [`prototype.votelog.ch`](https://prototype.votelog.ch/) and whose repository is found [here](https://gitlab.com/votelog/web/).
- The Curia Vista database import script written in Python whose repository is found [here](https://gitlab.com/votelog/data-import-curia-vista).

## Development progress

Work on the M4 feature set is ongoing.

Up to M3 the following features are planned or implemented:

- [X] Creation of user accounts
- [X] Protection of resources using permission framework
- [X] Creation of organisational accounts
- [X] Assignment of permissions using roles/capabilites
- [X] Retrieval of CuriaVista entities such as parliamentarians, affairs, bills and votes
- [X] Search index for CuriaVista entities
- [X] Assignment of 'preferred' voting results by organisations
- [X] Ranking of politicians according to organisations' voting preferences

## Setup VoteLog server for local development

The server part of `votelog` is written in Scala and uses [`sbt`](https://www.scala-sbt.org) for building the software. The following instructions assume that `sbt` has been [installed](https://www.scala-sbt.org/1.x/docs/Setup.html)

Clone and checkout repository

```bash
$ git clone git@gitlab.com:votelog/server.git
```

## HTTP server configuration

The `votelog` HTTP server application expects its configuration at `webserver/src/main/resources/application.conf`.

Example configurations can be found next to it, `application.conf.example` and `application.conf.example-env`.

The latter allows the configuration to be overwritten by setting the corresponding environment variables.

| Environment Variable           | Function                                                     |
|--------------------------------|--------------------------------------------------------------|
| `CURIAVISTA_DATABASE_URL`      | CuriaVista database URL                                      |
| `CURIAVISTA_DATABASE_PASSWORD` | CuriaVista database password                                 |
| `CURIAVISTA_DATABASE_USER`     | CuriaVista database user                                     |
| `SECURITY_PASSWORD_SALT`       | application security salt                                    |
| `SECURITY_SECRET`              | application security secret                                  |
| `VOTELOG_DATABASE_URL`         | votelog database URL                                         |
| `VOTELOG_DATABASE_USER`        | votelog database user                                        |
| `VOTELOG_DATABASE_PASSWORD`    | votelog database password                                    |
| `INDEX_DIRECTORY`              | file system directory location for lucene search index files |
| `HTTP_DOMAIN`                  | http domain (defaults to `votelog.ch` )                      |

Either way, copy an example and adjust it to your needs.

## Running VoteLog web server

Launch into the sbt build environment

```bash
$ cd server
$ sbt
```

Start webserver application:

```sbtshell
sbt:root> project webserver
sbt:webserver> run
```

## VoteLog REST interface

Documentation for the VoteLog REST API can be found at https://api.votelog.ch/api/v1/doc/

## Starting local Docker container for testing

```bash
docker-compose -f testing/docker-compose.yml up postgres
```

## License

This project is licensed under the [`AGPL-3.0-or-later`](https://spdx.org/licenses/AGPL-3.0-or-later.html). See [LICENSE.md](LICENSE.md).

This project includes parts that are licensed under the MIT License (MIT).
The corresponding source files have an appropriate license header.
