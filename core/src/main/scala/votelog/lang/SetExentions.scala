package votelog.lang

extension [T](set: Set[T]) {
  def nonEmptyOr(alternative: => Set[T]): Set[T] = {
    if set.nonEmpty then set
    else alternative
  }
}