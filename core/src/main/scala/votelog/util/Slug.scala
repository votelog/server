package votelog.util

import java.text.Normalizer

/**
 * Gratuitously from https://gist.github.com/sam/5213151
 *
 * Many thanks to https://gist.github.com/sam
 */
case class Slug(value: String)

object Slug {

  def apply(input:String): Slug = slugify(input)

  def slugify(input: String): Slug = {
    val slug =
      Normalizer.normalize(input, Normalizer.Form.NFD)
        .replaceAll("[^\\w\\s-]", "") // Remove all non-word, non-space or non-dash characters
        .replace('-', ' ')            // Replace dashes with spaces
        .trim                         // Trim leading/trailing whitespace (including what used to be leading/trailing dashes)
        .replaceAll("\\s+", "-")      // Replace whitespace (including newlines and repetitions) with single dashes
        .toLowerCase

    new Slug(slug)// Lowercase the final results
  }
}
