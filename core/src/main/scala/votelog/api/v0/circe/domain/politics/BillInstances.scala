package votelog.api.v0.circe.domain.politics

import votelog.domain.politics.Bill
import votelog.domain.politics.Bill.Type.*
import io.circe.*
import votelog.api.v0.circe.{deriveCodec, deriveUnwrappedCodec}

import java.util.UUID

trait BillInstances { self: BusinessInstances =>

  implicit val billTypeCodec: Codec[Bill.Type] = Codec.from(
    Decoder.decodeString.map {
      case "FederalLaw" => FederalLaw
      case "Ordinance" => Ordinance
      case "FederalDecision" => FederalDecision
      case "SimpleFederalDecision" => SimpleFederalDecision
    },
    Encoder.encodeString.contramap {
      case FederalLaw => "FederalLaw"
      case Ordinance => "Ordinance"
      case FederalDecision => "FederalDecision"
      case SimpleFederalDecision => "SimpleFederalDecision"
    }
  )

  implicit val billIdCodec: Codec[Bill.Id] = deriveUnwrappedCodec
  implicit val billFieldKeyDecoder: KeyDecoder[Bill.Field] = KeyDecoder[String].map(Bill.Field.unsafeFromString)
  implicit val billIdKeyEncoder: KeyEncoder[Bill.Id] = KeyEncoder[UUID].contramap(_.value)
  implicit val billIdKeyDecoder: KeyDecoder[Bill.Id] = KeyDecoder[UUID].map(Bill.Id.apply)

  implicit val billCodec: Codec[Bill] = deriveCodec
  implicit val billPartialCoder: Codec[Bill.Partial] = deriveCodec
}
