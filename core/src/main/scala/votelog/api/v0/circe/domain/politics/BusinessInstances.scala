package votelog.api.v0.circe.domain.politics

import votelog.domain.politics.Business
import io.circe.*
import votelog.api.v0.circe.{deriveCodec, deriveUnwrappedCodec}

trait BusinessInstances {

  implicit val businessCodec: Codec[Business] = deriveCodec
  implicit val businessPartialCodec: Codec[Business.Partial] = deriveCodec
  implicit val businessIdKeyDecoder: KeyDecoder[Business.Id] = KeyDecoder.decodeKeyInt.map(Business.Id.apply)
  implicit val businessIdKeyEncoder: KeyEncoder[Business.Id] = KeyEncoder.encodeKeyInt.contramap(_.value)
  implicit val businessIdCodec: Codec[Business.Id] = deriveUnwrappedCodec

  implicit val businessOrderingKeyDecoder: KeyDecoder[Business.Field] =
    KeyDecoder.decodeKeyString.map(Business.Field.unsafeFromString)
  implicit val businessOrderingKeyEncoder: KeyEncoder[Business.Field] =
    KeyEncoder.encodeKeyString.contramap(_.toString)

}

object implicits extends BillInstances with BusinessInstances