package votelog.api.v0.circe.domain.politics

import io.circe.{Codec, Decoder, Encoder}
import votelog.domain.politics.Decision

trait DecisionInstances {
  implicit val decisionCodec: Codec[Decision] = Codec.from(
    Decoder.decodeString.map {
      case "yes" => Decision.Yes
      case "no" => Decision.No
      case "abstain" => Decision.Abstain
      case "absent" => Decision.Absent
      case "presidentialAbstain" => Decision.PresidentialAbstain
      case "excused" => Decision.Excused
    },
    Encoder.encodeString.contramap[Decision] {
      case Decision.Yes => "yes"
      case Decision.No => "no"
      case Decision.Abstain => "abstain"
      case Decision.Absent => "absent"
      case Decision.PresidentialAbstain => "presidentialAbstain"
      case Decision.Excused => "excused"
    }
  )
}
