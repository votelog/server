package votelog.api.v0.circe.domain.politics

import io.circe.{Codec, KeyDecoder}
import votelog.api.v0.circe.{deriveUnwrappedCodec, deriveCodec}
import votelog.domain.politics.Party

trait PartyInstances {
  implicit val partyIdKeyDecoder: KeyDecoder[Party.Id] = KeyDecoder.decodeKeyInt.map(Party.Id.apply)
  implicit val partyIdCodec: Codec[Party.Id] = deriveUnwrappedCodec
  implicit val partyNameCodec: Codec[Party.Name] = deriveUnwrappedCodec
  implicit val partyAbbreviationCodec: Codec[Party.Abbreviation] = deriveUnwrappedCodec
  implicit val partyCodec: Codec[Party] = deriveCodec
}
