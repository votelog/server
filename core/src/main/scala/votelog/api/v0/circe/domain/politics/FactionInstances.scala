package votelog.api.v0.circe.domain.politics

import io.circe.{Codec, KeyDecoder}
import votelog.api.v0.circe.{deriveCodec, deriveUnwrappedCodec}
import votelog.domain.politics.Faction

trait FactionInstances {
  implicit val factionIdKeyDecoder: KeyDecoder[Faction.Id] = KeyDecoder.decodeKeyInt.map(Faction.Id.apply)
  implicit val factionIdCodec: Codec[Faction.Id] = deriveUnwrappedCodec
  implicit val factionCode: Codec[Faction] = deriveUnwrappedCodec
}
