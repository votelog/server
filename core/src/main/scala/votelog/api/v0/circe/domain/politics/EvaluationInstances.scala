package votelog.api.v0.circe.domain.politics

import io.circe.Codec
import votelog.api.v0.circe.{deriveCodec, deriveUnwrappedCodec}
import votelog.domain.politics.DecisionEvaluation.Score
import votelog.domain.politics.Rankings.{OrganisationRankingResult, OrganisationRankings, ParliamentarianRankingResult, ParliamentarianRankings}

trait EvaluationInstances { self: PersonInstances & OrganisationInstances & PartyInstances & FactionInstances =>

  lazy implicit val rankingResultCodec: Codec[OrganisationRankingResult] = deriveCodec
  lazy implicit val parliamentarianRankingResultCodec: Codec[ParliamentarianRankingResult] = deriveCodec
  lazy implicit val scoreCodec: Codec[Score] = deriveUnwrappedCodec

  lazy implicit val organisationRankingCodec: Codec[OrganisationRankings] = deriveCodec
  lazy implicit val parliamentarianRankingCodec: Codec[ParliamentarianRankings] = deriveCodec

}
