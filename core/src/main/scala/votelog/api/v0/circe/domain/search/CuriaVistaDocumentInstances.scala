package votelog.api.v0.circe.domain.search

import io.circe.*
import votelog.api.v0.circe.deriveCodec
import votelog.domain.search.Search

trait CuriaVistaDocumentInstances {

  given curiaVistaDocumentCodec: Codec[Search.CuriaVistaDocument] = deriveCodec
  given entityCodec: Codec[Search.Entity.Type] = deriveCodec
}

object implicits extends CuriaVistaDocumentInstances
