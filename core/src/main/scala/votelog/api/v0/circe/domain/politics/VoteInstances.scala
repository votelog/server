package votelog.api.v0.circe.domain.politics

import votelog.domain.politics.{Vote, *}
import io.circe.*
import votelog.api.v0.circe.{deriveCodec, deriveUnwrappedCodec}

trait VoteInstances { self: BillInstances & BusinessInstances =>

  implicit val voteIdCodec: Codec[Vote.Id] = deriveUnwrappedCodec
  implicit val voteRegistrationCodec: Codec[Vote.Registration] = deriveUnwrappedCodec
  implicit val voteIdKeyEncoder: KeyEncoder[Vote.Id] = KeyEncoder[Int].contramap(_.value)
  implicit val voteIdKeyDecoder: KeyDecoder[Vote.Id] = KeyDecoder[Int].map(Vote.Id.apply)
  implicit val voteRegistrationKeyEncoder: KeyEncoder[Vote.Registration] = KeyEncoder[Int].contramap(_.number)
  implicit val voteRegistrationKeyDecoder: KeyDecoder[Vote.Registration] = KeyDecoder[Int].map(Vote.Registration.apply)

  implicit val meaningCodec: Codec[Vote.Meaning] = deriveCodec

  implicit val votePartialCodec: Codec[Vote.Partial] = deriveCodec
  implicit val voteCodec: Codec[Vote] = deriveCodec
}
