package votelog.api.v0.circe.domain.politics

import io.circe.{Codec, KeyDecoder, KeyEncoder}
import votelog.api.v0.circe.{deriveCodec, deriveUnwrappedCodec}
import votelog.domain.politics.Ngo
import votelog.persistence.OrganisationStore

trait OrganisationInstances {

  implicit val ngoCodec: Codec[Ngo] = deriveCodec
  implicit val ngoHCodec: Codec[Ngo.Partial] = deriveCodec
  implicit val ngoIdCodec: Codec[Ngo.Id] = deriveUnwrappedCodec
  implicit val ngoStoreRecipeCodec: Codec[OrganisationStore.Recipe] = deriveCodec
  implicit val ngoIdCirceKeyDecoder: KeyDecoder[Ngo.Id] = KeyDecoder.decodeKeyUUID.map(Ngo.Id.apply)
  implicit val ngoOrderingKeyDecoder: KeyDecoder[Ngo.Field] = KeyDecoder.decodeKeyString.map(Ngo.Field.unsafeFromString)
  implicit val keyEncoderNgoId: KeyEncoder[Ngo.Id] = KeyEncoder.encodeKeyUUID.contramap(_.value)
}
