package votelog.api.v0.circe.domain.authorization

import io.circe.{Codec, KeyDecoder, Decoder, Encoder}
import votelog.api.v0.circe.{deriveCodec, deriveUnwrappedCodec}
import votelog.domain.authorization.Role.Permission
import votelog.domain.authorization.{Capability, Component, Role}

trait RoleInstances {

  implicit val componentCodec: Codec[Component] =
    Codec.from(
      Decoder.decodeString.map(Component.Impl.apply),
      Encoder.encodeString.contramap(_.location),
    )

  implicit val capabilityCodec: Codec[Capability] = deriveCodec
  implicit val permissionCodec: Codec[Permission] = deriveCodec
  implicit val roleTemplateCodec: io.circe.Codec[Role.Template] = deriveCodec
  implicit val roleCodec: io.circe.Codec[Role] = deriveCodec
  implicit val roleIdCodec: io.circe.Codec[Role.Id] = deriveUnwrappedCodec

}
