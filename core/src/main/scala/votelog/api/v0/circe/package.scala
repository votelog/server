package votelog.api.v0

import io.circe.Codec
import io.circe.generic.semiauto

import scala.deriving.Mirror

package object circe {

  inline def deriveCodec[T](using inline M: Mirror.Of[T]): Codec[T] = semiauto.deriveCodec(using M)

  inline def deriveUnwrappedCodec[T <: Product, A](
    using
      inline M: Mirror.ProductOf[T] {
        type MirroredElemTypes = (A *: EmptyTuple.type)
        type MirroredMonoType <: T
      },
    inline E: io.circe.Encoder[A],
    inline D: io.circe.Decoder[A]
  ): Codec[T] = {

    val e = E.contramap((t: T) => head(Tuple.fromProductTyped(t)))
    val d = D.map(a  => M.fromProduct(Tuple(a)))

    Codec.from(d, e)
  }

  def head[A, X <: A *: Tuple](tuple: X): A = tuple match {
    case Tuple1(a: A) => a
    case (a, _) => a
  }

}
