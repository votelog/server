package votelog.api.v0.circe

import io.circe.{Codec, KeyDecoder, KeyEncoder, Json, HCursor}
import io.circe.Decoder.Result
import io.circe.Encoder
import votelog.domain.authentication.{Token, User}
import votelog.domain.crudi.ReadOnlyStore.Index
import votelog.domain.politics.*
import votelog.domain.authorization.Role.Permission
import votelog.domain.authorization.{Capability, Component}
import votelog.domain.crudi.ReadOnlyStore.Index
import votelog.domain.politics.*
import votelog.persistence.OrganisationStore.DecisionPreference
import votelog.persistence.UserStore
import votelog.util.Slug

object implicits
  extends domain.politics.PartyInstances,
    domain.politics.BillInstances,
    domain.politics.BusinessInstances,
    domain.politics.VoteInstances,
    domain.politics.DecisionInstances,
    domain.politics.FactionInstances,
    domain.politics.PersonInstances,
    domain.politics.EvaluationInstances,
    domain.politics.OrganisationInstances,
    domain.authorization.RoleInstances,
    domain.search.CuriaVistaDocumentInstances {

  implicit val keyEncoderUserId: KeyEncoder[User.Id] = KeyEncoder.encodeKeyUUID.contramap(_.value)
  implicit val userIdCirceKeyDecoder: KeyDecoder[User.Id] = KeyDecoder.decodeKeyUUID.map(User.Id.apply)
  implicit val userIdDecoder: Codec[User.Id] = deriveUnwrappedCodec
  implicit val userEmailCodec: Codec[User.Email] = deriveUnwrappedCodec

  override implicit val capabilityCodec: Codec[Capability] = deriveCodec
  override implicit val permissionCodec: Codec[Permission] = deriveCodec

  implicit val slugCodec: Codec[Slug] = deriveUnwrappedCodec
  implicit val tokenCode: Codec[Token] = deriveUnwrappedCodec

  implicit val userCodec: Codec[User] = new Codec[User] {
    override def apply(c: HCursor): Result[User] =
      for {
        name <- c.downField("name").as[String]
        email <- c.downField("email").as[String]
      } yield User(name, User.Email(email), "", Set.empty, Set.empty)

    override def apply(a: User): Json =
      Json.obj(
        "name" -> Encoder.encodeString(a.name),
        "email" -> Encoder.encodeString(a.email.value),
        "permissions" -> Encoder.encodeSet[Permission].apply(a.permissions)
      )
  }

  implicit val userHCodec: Codec[User.Partial] = deriveCodec
  implicit val userOrderingKeyDecoder: KeyDecoder[User.Field] =
    KeyDecoder.decodeKeyString.map(User.Field.unsafeFromString)

  implicit val userStoreRecipeCodec: Codec[UserStore.Recipe] = deriveCodec
  implicit val decisionPreferenceCodec: Codec[DecisionPreference] = deriveCodec

  implicit val partyIdCirceKeyDecoder: KeyDecoder[Party.Id] = KeyDecoder.decodeKeyInt.map(Party.Id.apply)
  implicit val lpKeyDecoder: KeyDecoder[LegislativePeriod.Id] = KeyDecoder.decodeKeyInt.map(LegislativePeriod.Id.apply)
  implicit val langKeyDecoder: KeyDecoder[Language] = (key: String) => Language.fromIso639_1(key)

  implicit def indexCodec[T, Id](using tc: Codec[T], idc: Codec[Id]): Codec[Index[Id, T]] =
    new Codec[Index[Id, T]] {
      override def apply(index: Index[Id, T]): Json = Json.obj(
        ("totalEntities", Json.fromInt(index.totalEntities)),
        ("entities", Encoder.encodeList[(Id, T)].apply(index.entities))
      )

      override def apply(c: HCursor): Result[Index[Id, T]] =
        for {
          totalEntities <- c.downField("totalEntities").as[Int]
          entities <- c.downField("entities").as[List[(Id, T)]]
        } yield Index(totalEntities, entities)
    }
}
