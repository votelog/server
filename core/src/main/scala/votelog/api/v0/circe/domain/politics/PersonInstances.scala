package votelog.api.v0.circe.domain.politics

import io.circe.{Codec, Decoder, Encoder, KeyDecoder, KeyEncoder}
import votelog.api.v0.circe.{deriveCodec, deriveUnwrappedCodec}
import votelog.domain.politics.{Canton, Parliamentarian, PersonPartial}
import votelog.domain.politics.Parliamentarian.Gender.{Female, Male}
import votelog.persistence.UserStore.Password

trait PersonInstances { self: PartyInstances & FactionInstances =>

  implicit val personIdCirceKeyDecoder: KeyDecoder[Parliamentarian.Id] = KeyDecoder.decodeKeyInt.map(Parliamentarian.Id.apply)
  implicit val personIdCirceKeyEncoder: KeyEncoder[Parliamentarian.Id] = KeyEncoder.encodeKeyInt.contramap(_.value)

  implicit val personFieldKeyDecoder: KeyDecoder[Parliamentarian.Field] = KeyDecoder.decodeKeyString.map(Parliamentarian.Field.unsafeFromString)
  implicit val personFieldKeyEncoder: KeyEncoder[Parliamentarian.Field] = KeyEncoder.encodeKeyString.contramap(_.toString)

  implicit val personGenderDecoder: Decoder[Parliamentarian.Gender] =
    Decoder.decodeString.map {
      case "female" => Female
      case "male" => Male
    }

  implicit val personGenderEncoder: Encoder[Parliamentarian.Gender] =
    Encoder.encodeString.contramap {
      case Female => "female"
      case Male => "male"
    }

  implicit val personImageCodec: Codec[Parliamentarian.Image] = deriveUnwrappedCodec
  implicit val personIdCodec: Codec[Parliamentarian.Id] = deriveUnwrappedCodec
  implicit val personNameCodec: Codec[Parliamentarian.Name] = deriveUnwrappedCodec
  implicit val passwordClearCodec: Codec[Password.Clear] = deriveUnwrappedCodec
  implicit val cantonCodec: Codec[Canton] = deriveUnwrappedCodec

  implicit val personCodec: Codec[Parliamentarian] = deriveCodec
  implicit val personPartialCodec: Codec[PersonPartial] = deriveCodec
}
