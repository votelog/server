package votelog.persistence


import java.util.UUID
import cats.data.Validated.{Invalid, Valid}
import cats.data.{NonEmptyList, Validated}
import votelog.domain.crudi.ReadOnlyStore.IndexQueryParameters
import votelog.domain.crudi.Store
import votelog.domain.politics.{Bill, Business, Decision, LegislativePeriod, Ngo, Vote}
import votelog.persistence.OrganisationStore.{DecisionPreference, Recipe}
import votelog.util.Slug

import java.time.LocalDateTime


trait OrganisationStore[F[_]]
  extends Store[F, Ngo, Ngo.Id, Recipe, Ngo.Partial, Unit, IndexQueryParameters[Unit, Ngo.Field, Ngo.Field]] {

  def bySlug(slug: Slug): F[(Ngo.Id, Ngo)]
  // these preference methods look awfully like store methods.
  // can we reuse store semantics with id (Ngo.Id, Vote.Registration)
  // figure out how to accommodate DecisionPreference
  def preferences(lp: LegislativePeriod.Id, ngo: Ngo.Id): F[List[(Vote.Id, DecisionPreference)]]
  def updatePreference(ngo: Ngo.Id, vote: Vote.Id, preference: Decision): F[Unit]
  def deletePreference(ngo: Ngo.Id, vote: Vote.Id): F[Unit]
  def readPreference(ngo: Ngo.Id, vote: Vote.Id): F[DecisionPreference]
}

object OrganisationStore {
  case class DecisionPreference(business: Business.Id, bill: Option[Bill.Id], preference: Decision)

  case class Recipe(
    name: String,
    slug: String,
    picture: Option[String],
    mail: String,
    about: String,
    createAt: LocalDateTime,
  )

  def validateRecipe(
    name: String,
    slug: String,
    picture: Option[String],
    mail: String,
    about: String,
    createdAt: LocalDateTime
  ): Validated[NonEmptyList[(String, String)], Recipe] =
    if (name.nonEmpty) Valid(Recipe(name, slug, picture, mail, about, createdAt))
    else Invalid("name" -> "must not be empty").toValidatedNel

}