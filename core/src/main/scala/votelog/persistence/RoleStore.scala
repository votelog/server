package votelog.persistence

import votelog.domain.authorization.Role.Permission
import votelog.domain.authorization.{Capability, Component, Role}
import votelog.domain.crudi.Store
import votelog.domain.data.Paging

trait RoleStore[F[_]] extends Store[
  F,
  Role,
  Role.Id,
  Role.Template,
  String,
  Unit,
  Paging,
] {

  type ReadParameters = Unit
  type IndexParameters = Paging

  def grantPermission(roleId: Role.Id, permission: Permission): F[Unit]
  def revokePermission(roleId: Role.Id, permission: Permission): F[Unit]
}

