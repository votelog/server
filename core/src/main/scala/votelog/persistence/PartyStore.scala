package votelog.persistence

import votelog.domain.crudi.ReadOnlyStore
import votelog.domain.politics.{Context, Language, Party}

trait PartyStore[F[_]]
  extends ReadOnlyStore[F, Party, Party.Id, Party, Language, Context]
