package votelog.persistence

import votelog.domain.crudi.ReadOnlyStore
import votelog.domain.crudi.ReadOnlyStore.PagedIndexQueryParameters
import votelog.domain.politics.{Business, Context, Language, Vote}

trait VoteStore[F[_]] extends ReadOnlyStore[
  F,
  Vote,
  Vote.Id,
  Vote.Partial,
  Language,
  PagedIndexQueryParameters[Context],
] {

  type ReadParameters = Language
  type IndexParameters = PagedIndexQueryParameters[Context]

  def fetchByIds(language: Language, ids: Set[Vote.Id]): F[List[(Vote.Id, Vote)]]
}
