package votelog.persistence

import votelog.domain.crudi.ReadOnlyStore
import votelog.domain.crudi.ReadOnlyStore.IndexQueryParameters
import votelog.domain.politics.{Bill, Business, Context, Language}

trait BusinessStore[F[_]] extends ReadOnlyStore[
  F,
  Business,
  Business.Id,
  Business.Partial,
  Language,
  IndexQueryParameters[Context, Business.Field, Business.Field]] {

  type ReadParameters = Language
  type IndexParameters = IndexQueryParameters[Context, Business.Field, Business.Field]

  def fetchByIds(language: Language, ids: Set[Business.Id]): F[List[(Business.Id, Business)]]
  def billsForBusiness(context: Context, business: Business.Id): F[List[(Bill.Id, Bill.Partial)]]
}
