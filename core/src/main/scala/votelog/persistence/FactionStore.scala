package votelog.persistence

import votelog.domain.crudi.ReadOnlyStore
import votelog.domain.politics.{Context, Faction, Language}

trait FactionStore[F[_]]
  extends ReadOnlyStore[F, Faction, Faction.Id, Faction, Language, Context]
