package votelog.persistence

import votelog.domain.crudi.ReadOnlyStore
import votelog.domain.crudi.ReadOnlyStore.IndexQueryParameters
import votelog.domain.politics.Vote.Registration
import votelog.domain.politics.{Bill, Context, Language, Vote}


trait BillStore[F[_]] extends ReadOnlyStore[
  F,
  Bill,
  Bill.Id,
  Bill.Partial,
  Language,
  IndexQueryParameters[Context, Bill.Field, Bill.Field]] {

  type ReadParameters = Language
  type IndexParameters = IndexQueryParameters[Context, Bill.Field, Bill.Field]

  def fetchByIds(language: Language, ids: Set[Bill.Id]): F[List[(Bill.Id, Bill)]]
  def votesForBill(language: Language, bill: Bill.Id): F[Map[Vote.Id, Vote.Partial]]
}
