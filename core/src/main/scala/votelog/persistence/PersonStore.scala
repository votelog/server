package votelog.persistence

import votelog.domain.crudi.ReadOnlyStore
import votelog.domain.crudi.ReadOnlyStore.IndexQueryParameters
import votelog.domain.politics.{Context, Language, Parliamentarian, PersonPartial}

trait PersonStore[F[_]] extends ReadOnlyStore[
  F,
  Parliamentarian,
  Parliamentarian.Id,
  PersonPartial,
  Language,
  IndexQueryParameters[Context, Parliamentarian.Field, Parliamentarian.Field]] {

  type IndexParameters = IndexQueryParameters[Context, Parliamentarian.Field, Parliamentarian.Field]

}

