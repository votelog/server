package votelog.persistence

import cats.Applicative

import java.util.UUID
import cats.implicits.*
import cats.data.{NonEmptyList, Validated}
import cats.data.Validated.{Invalid, Valid}
import votelog.domain.authentication.User
import votelog.domain.authentication.User.Email
import votelog.domain.authorization.{Capability, Component, Role}
import votelog.domain.crudi.ReadOnlyStore.IndexQueryParameters
import votelog.domain.crudi.Store

trait UserStore[F[_]] extends Store[
  F,
  User,
  User.Id,
  UserStore.Recipe,
  User.Partial,
  Unit,
  IndexQueryParameters[Unit, User.Field, User.Field]
] {

  def findByName(name: String): F[Option[(User.Id, User)]]
  def grantRole(user: User.Id, role: Role.Id): F[Unit]
  def revokeRole(user: User.Id, role: Role.Id): F[Unit]
}

object UserStore {

  case class Recipe(name: String, email: User.Email, password: Password.Clear) {
    def prepare(passwordHash: Password.Hashed): PreparedRecipe =
      PreparedRecipe(name, email, passwordHash)
  }

  case class PreparedRecipe(name: String, email: User.Email, password: Password.Hashed)

  trait Password {
    val value: String
    override def toString: String = value
  }

  object Password {

    case class Clear(value: String) extends Password {
      override def toString: String = "[password in clear]"
    }

    case class Hashed(value: String) extends Password
  }

  def validateRecipe(
    name: String,
    email: Email,
    password: Password.Clear,
    confirmPassword: Password.Clear
  ): Validated[NonEmptyList[(String, String)], Recipe] = {

    def nonEmptyString(name: String): String => Validated[NonEmptyList[(String, String)], String] = (s: String) =>
      if (s.isEmpty) Invalid(name -> "must not be empty").toValidatedNel
      else Valid(s).toValidatedNel

    def areEqual(password: Password.Clear, confirmPassword: Password.Clear): Validated[NonEmptyList[(String, String)], Password.Clear] =
      if (password != confirmPassword) Invalid("confirmPassword" -> s"must be equal to password").toValidatedNel
      else Valid(password).toValidatedNel

    (nonEmptyString("name")(name),
      nonEmptyString("email")(email.value),
      nonEmptyString("password")(password.value) *>
        areEqual(password, confirmPassword)
    )
        .mapN { case (name, email, password) => Recipe(name, User.Email(email), password) }
  }
}

