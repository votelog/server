package votelog.syntax

import scala.concurrent.Future

trait FutureInstances {
  extension (fa: Future.type) {
    def fromEither[A, B](f: A => Exception)(either: Either[A, B]): Future[B] = {
      Future.fromTry(either.left.map(f).toTry)
    }
  }
}
