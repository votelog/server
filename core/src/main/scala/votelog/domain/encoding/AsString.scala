package votelog.domain.encoding

import io.circe.KeyEncoder

/** render canonical string representation of an entity. Not for display purposes, use [[cats.Show]] instead */
trait AsString[A] {
  def asString(a: A): String
}

object AsString {
  def apply[A: AsString]: AsString[A] = implicitly

  implicit def keyEncoderAsString[A](implicit keyEncoder: KeyEncoder[A]): AsString[A] = new AsString[A] {
    def asString(a: A): String = keyEncoder.apply(a)
  }
}
