package votelog.domain.organisation

import votelog.domain.authentication
import votelog.domain.organisation.OrganisationService.Recipe
import votelog.domain.politics.Ngo
import votelog.persistence.UserStore.Password

import java.time.LocalDateTime

trait OrganisationService[F[_]] {
  def createOrganisation(
    recipe: Recipe
  ): F[Ngo.Id]
}

object OrganisationService {

  case class Recipe(user: Recipe.User, organisation: Recipe.Organisation)

  object Recipe {
    case class User(
      name: String,
      email: authentication.User.Email,
      password: Password.Clear,
    )

    case class Organisation(
      name: String,
      slug: String,
      picture: Option[String],
      mail: String,
      about: String,
    )
  }
}
