package votelog.domain.politics

import votelog.domain.encoding.AsString

import java.util.UUID

case class Bill(
  title: String,
  businessId: Business.Id,
  billType: Bill.Type,
  submissionDate: java.time.LocalDateTime
)

object Bill {

  case class Partial(
    title: Option[String],
    businessId: Option[Business.Id],
    billType: Option[Bill.Type],
    submissionDate: Option[java.time.LocalDateTime],
  )

  object Partial {
    val empty: Partial = Bill.Partial(None, None, None, None)
  }

  sealed trait Type extends Product with Serializable
  object Type {
    case object FederalLaw extends Type
    case object Ordinance extends Type
    case object FederalDecision extends Type
    case object SimpleFederalDecision extends Type
  }

  case class Id(value: UUID)
  object Id {
    given AsString[Id] = _.value.toString
  }

  trait Field extends Product with Serializable
  object Field {
    case object Title extends Field
    case object BusinessId extends Field
    case object BillType extends Field
    case object SubmissionDate extends Field

    lazy val values: List[Field] = List(Title, BusinessId, BillType, SubmissionDate)
    lazy val unsafeFromString: (String => Field) =
      (values zip values).map { case (key, value) => (key.toString, value)}.toMap.apply
  }
}
