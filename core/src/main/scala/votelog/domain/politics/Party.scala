package votelog.domain.politics

import java.time.LocalDateTime

case class Party(id: Party.Id, name: Party.Name, abbreviation: Party.Abbreviation)

object Party {
  case class Id(value: Int)
  case class Name(value: String)
  case class Abbreviation(value: String)

  case class Partial(
    id: Option[Party.Id],
    name: Option[Party.Name],
    startDate: Option[LocalDateTime],
  )

}
