package votelog.domain.politics

import java.time.LocalDate

sealed trait Council extends Product with Serializable

object Council {
  case object CouncilOfStates extends Council
  case object NationalCouncil extends Council
  case object FederalCouncil extends Council

  sealed trait Function extends Product with Serializable
  object Function {
    case object Member extends Function
    case object President extends Function
  }

  case class Membership(council: Council, joining: LocalDate, leaving: Option[LocalDate])
}
