package votelog.domain.politics

import votelog.domain.politics.DecisionEvaluation.Score

import scala.util.Try

trait DecisionEvaluation {
  def scoreFor(preference: Option[Decision])(actual: Decision): Score
}

object DecisionEvaluation {
  case class Score(value: Double) {
    def +(other: Score): Score = Score(value + other.value)
  }

  object Score {
    val empty: Score = Score(0.0)

    implicit val scoreNumeric: Numeric[Score] =
      new Numeric[Score] {
        override def plus(x: Score, y: Score): Score = x + y
        override def minus(x: Score, y: Score): Score = Score(x.value - y.value)
        override def times(x: Score, y: Score ): Score = Score(x.value * y.value)
        override def negate(x: Score): Score = Score(-x.value)
        override def fromInt(x: Int): Score = Score(x.toDouble)
        override def parseString(str: String): Option[Score] = Try(str.toDouble).map(Score.apply).toOption
        override def toInt(x: Score): Int = x.value.toInt
        override def toLong(x: Score): Long = x.value.toLong
        override def toFloat(x: Score): Float = x.value.toFloat
        override def toDouble(x: Score): Double = x.value
        override def compare(x: Score, y: Score): Int = x.value.compare(y.value)
      }
  }

  object NaiveEvaluation extends DecisionEvaluation {
    override def scoreFor(preference: Option[Decision])(actual: Decision): Score =
      preference.collect {
        case `actual` => Score(1.0)
      }.getOrElse(Score(0.0))
  }
}