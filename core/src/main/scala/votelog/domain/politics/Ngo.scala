package votelog.domain.politics

import java.time.LocalDateTime
import java.util.UUID
import scala.util.Try

case class Ngo(
  name: String,
  slug: String,
  picture: Option[String],
  mail: String,
  about: String,
  createdAt: LocalDateTime,
)

object Ngo {

  case class Partial(
    name: Option[String],
    slug: Option[String],
    picture: Option[String],
    mail: Option[String],
    about: Option[String],
    createdAt: Option[LocalDateTime],
  )

  case class Topics(value: String)

  val empty: Partial = Partial(None, None, None, None, None, None)
  case class Id(value: UUID)

  object Id {
    def unapply(str: String): Option[Id] = Try(Id(UUID.fromString(str))).toOption
  }

  sealed trait Field
  object Field {
    case object Name extends Field
    case object Picture extends Field
    case object Mail extends Field
    case object About extends Field
    case object CreatedAt extends Field
    case object Slug extends Field

    val values: Seq[Field] = Seq(Name, Slug, Picture, Mail, About, CreatedAt)

    lazy val unsafeFromString: (String => Field) =
      (values zip values).map({ case (key, value) => (key.toString, value) }).toMap.apply
  }
}
