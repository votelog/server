package votelog.domain.politics

import votelog.domain.politics.{Context, Decision, Parliamentarian, Vote}
import votelog.persistence.PersonStore

trait ParliamentarianService[F[_]] extends PersonStore[F] {
  def decisionsByPerson(context: Context)(person: Parliamentarian.Id): F[List[(Vote.Id, Decision)]]
}