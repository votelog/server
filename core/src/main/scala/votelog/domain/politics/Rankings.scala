package votelog.domain.politics

import votelog.domain.politics.DecisionEvaluation.Score
import votelog.domain.politics.Rankings.{OrganisationRankings, ParliamentarianRankings}

trait Rankings[F[_]] {
  def personRankingByNgo(ngo: Ngo.Id, lp: LegislativePeriod.Id): F[OrganisationRankings]
  def personRankings(parliamentarian: Parliamentarian.Id, lp: LegislativePeriod.Id): F[ParliamentarianRankings]
}

object Rankings {
  case class OrganisationRankingResult(score: Score, participated: Int)
  case class ParliamentarianRankingResult(
    score: Score,
    preferencesCount: Int,
    participationCount: Int
  )

  case class OrganisationRankings(
    ranking: Map[Parliamentarian.Id, OrganisationRankingResult],
    totalPreferences: Int,
  )

  case class ParliamentarianRankings(
    ranking: Map[Ngo.Id, ParliamentarianRankingResult],
    participated: Int,
  )
}

