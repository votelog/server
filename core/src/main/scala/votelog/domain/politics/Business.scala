package votelog.domain.politics

import votelog.domain.encoding.AsString

import java.time.LocalDate
import scala.util.Try

case class Business(
  title: String,
  shortNumber: String,
  description: Option[String],
  submittedBy: Option[String],
  submissionDate: LocalDate
)

object Business {
  case class Partial(
    title: Option[String],
    shortNumber: Option[String],
    description: Option[String],
    submittedBy: Option[String],
    submissionDate: Option[LocalDate],
  )

  val empty: Partial = Partial(None, None, None, None, None)

  case class Id(value: Int)
  object Id {
    def unapply(str: String): Option[Business.Id] =
      Try(str.toInt).map(Business.Id.apply).toOption

    given AsString[Id] = _.value.toString
  }

  sealed trait Type extends Product with Serializable
  object Type {
    case object CouncilAffair extends Type // Geschäft des Bundesrates
    case object ParliamentaryAffair extends Type // Geschäft des Parlaments
    case object ParliamentaryInitiative extends Type /// Parlamentarische Initiative
    case object Motion extends Type
    case object Postulate extends Type
    case object Petition extends Type
  }

  sealed trait Field extends Product with Serializable
  object Field {
    case object Id extends Field
    case object Title extends Field
    case object ShortNumber extends Field
    case object Description extends Field
    case object SubmittedBy extends Field
    case object SubmissionDate extends Field

    val values: List[Field] = List(Title, ShortNumber, Description,  SubmittedBy, SubmissionDate)
    lazy val unsafeFromString: (String => Field) =
      (values zip values).map { case (key, value) => (key.toString, value)}.toMap.apply
  }
}
