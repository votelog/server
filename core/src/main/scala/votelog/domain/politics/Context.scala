package votelog.domain.politics

case class Context(legislativePeriod: LegislativePeriod.Id, language: Language)


object Context {
  val Default = Context(LegislativePeriod.Id.Default, Language.Default)
}
