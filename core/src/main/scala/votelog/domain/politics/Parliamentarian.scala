package votelog.domain.politics

import votelog.domain.encoding.AsString
import votelog.domain.politics.Parliamentarian.Image

case class Parliamentarian(
  id: Parliamentarian.Id,
  firstName: Parliamentarian.Name,
  lastName: Parliamentarian.Name,
  canton: Canton,
  gender: Parliamentarian.Gender,
  party: Party.Id,
  dateOfElection: Option[java.time.LocalDate],
  dateOfBirth: Option[java.time.LocalDate],
  image: Image,
  faction: Option[Faction.Id]
)

case class PersonPartial(
  id: Option[Parliamentarian.Id],
  firstName: Option[Parliamentarian.Name],
  lastName: Option[Parliamentarian.Name],
  canton: Option[Canton],
  gender: Option[Parliamentarian.Gender],
  party: Option[Party.Id],
  dateOfElection: Option[java.time.LocalDate],
  dateOfBirth: Option[java.time.LocalDate],
  image: Option[Image],
  faction: Option[Faction.Id],
)

object Parliamentarian {

  sealed trait Communication extends Product with Serializable
  object Communication {
    case class Email(value: String) extends Communication
    case class Phone(value: String) extends Communication
    case class Homepage(value: String) extends Communication
  }

  case class Id(value: Int) { override def toString: String = value.toString}
  object Id {
    given AsString[Id] = _.value.toString
  }

  case class Name(value: String) { override def toString: String = value }
  case class Image(id: Int)

  object Name  {
    implicit val ordering: Ordering[Name] = (x: Name, y: Name) => x.value.compare(y.value)
  }

  sealed trait Gender extends Product with Serializable
  object Gender {
    case object Female extends Gender
    case object Male extends Gender
  }

  sealed trait Field extends Product with Serializable
  object Field {
    case object Id extends Field
    case object FirstName extends Field
    case object LastName extends Field
    case object Canton extends Field
    case object Gender extends Field
    case object Party extends Field
    case object DateOfElection extends Field
    case object DateOfBirth extends Field
    case object Image extends Field
    case object Faction extends Field

    // ugly and brittle and fragile and broken: sequence must be equal to construction of select query ....
    // fixme: create better partial mechanism
    val values: List[Field] = List(Id, FirstName, LastName, Canton, Gender, Party, DateOfElection, DateOfBirth, Image, Faction)
    lazy val unsafeFromString: String => Field =
      (values zip values).map { case (key, value) => (key.toString, value)}.toMap.apply
  }
}
