package votelog.domain.politics

import votelog.domain.encoding.AsString
import votelog.domain.politics.Vote.{Meaning, Registration}

import java.time.LocalDateTime
import scala.util.Try

case class Vote(
  registration: Registration,
  bill: Option[Bill.Id],
  business: Business.Id,
  subject: Option[String],
  meaning: Meaning,
  end: LocalDateTime
)

object Vote {
  case class Id(value: Int)
  object Id {
    def unapply(value: String): Option[Id] =
      Try(Id(value.toInt)).toOption

    given AsString[Id] = _.value.toString
  }

  case class Registration(number: Int)

  case class Partial(
    bill: Option[Bill.Id],
    business: Option[Business.Id],
    subject: Option[String],
    meaning: Option[Meaning],
    end: Option[LocalDateTime],
  )

  object Registration {
    def unapply(value: String): Option[Registration] =
      Try(value.toInt).toOption.map(Registration.apply)
  }

  case class Meaning(yes: Option[String], no: Option[String])
}
