package votelog.domain.politics

trait DecisionsStore[F[_]] {

  // we probably need some kind of paging as well
  def decisionsForVoteRegistration(context: Context)(vote: Vote.Id): F[List[(Parliamentarian.Id, Decision)]]
  def decisionsByPerson(context: Context)(person: Parliamentarian.Id): F[List[(Vote.Id, Decision)]]
  def decisionsByContext(context: Context): fs2.Stream[F, (Vote.Id, Parliamentarian.Id, Decision)]
}
