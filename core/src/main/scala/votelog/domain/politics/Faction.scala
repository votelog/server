package votelog.domain.politics

case class Faction(name: String)

object Faction {
  case class Id(value: Int)
}



