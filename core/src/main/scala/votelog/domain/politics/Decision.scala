package votelog.domain.politics

sealed trait Decision extends Product with Serializable

object Decision {
  case object Yes extends Decision
  case object No extends Decision
  case object Abstain extends Decision
  case object Absent extends Decision
  case object Excused extends Decision
  case object PresidentialAbstain extends Decision

  val fromInt: Int => Option[Decision] = {
    case 1 => Some(Yes)
    case 2 => Some(No)
    case 3 => Some(Abstain)
    case 5 => Some(Absent)
    case 6 => Some(Excused)
    case 7 => Some(PresidentialAbstain)
    case _ => None
  }

  val fromString: String => Option[Decision] = {
    case "Yes" => Some(Yes)
    case "No" => Some(No)
    case "Abstain" => Some(Abstain)
    case "Absent" => Some(Absent)
    case "Excused" => Some(Excused)
    case _ => None
  }

  val asString: Decision => String = {
    case Yes => "Yes"
    case No => "No"
    case Abstain => "Abstain"
    case Absent => "Absent"
    case PresidentialAbstain => "Abstain"
    case Excused => "Excused"
  }
}
