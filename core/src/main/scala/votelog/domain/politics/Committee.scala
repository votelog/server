package votelog.domain.politics

import java.util.UUID

case class Committee(name: String)

object Committee {
  case class Id(id: UUID)
}
