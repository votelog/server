package votelog.domain.data

import cats.Show
import votelog.domain.data.Paging.{Offset, PageSize}
import votelog.domain.encoding.AsString

case class Paging(offset: Offset, pageSize: PageSize)

object Paging {

  implicit val asString: AsString[Paging] = paging => s"${paging.pageSize}-${paging.offset}"

  val Default = Paging(Offset(0), PageSize(10))

  case class PageSize(value: Int) { override def toString: String = value.toString }
  object PageSize {
    implicit val pageSizeOrdering: Ordering[PageSize] =
      (lhs: PageSize, rhs: PageSize) => lhs.value.compareTo(rhs.value)
    implicit val pageSizeShow: Show[PageSize] = _.value.toString
  }

  case class Offset private (value: Long) { override def toString: String = value.toString }
  object Offset {
    def apply(value: Long): Offset = {
      //assert(value <= 0, "offset must be equal to or larger than 0")
      new Offset(value)
    }
  }
}
