package votelog.domain.crudi

// TODO: try to make Identity and Recipe inner types
trait Store[F[_], Entity, Identity, Template, Partial, ReadParameter, IndexParameter]
  extends ReadOnlyStore[F, Entity, Identity, Partial, ReadParameter, IndexParameter] {

  def create(r: Template): F[Identity]
  def delete(id: Identity): F[Unit]
  def update(id: Identity, r: Template): F[Entity]
}

