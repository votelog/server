package votelog.domain.crudi

import votelog.domain.crudi.ReadOnlyStore.Index
import votelog.domain.data.Paging
import votelog.domain.data.Paging.{Offset, PageSize}
import votelog.domain.data.Sorting.Direction

trait ReadOnlyStore[F[_], T, Identity, Partial, ReadParameter, IndexParameter] {

  def index(queryParameters: IndexParameter): F[Index[Identity, Partial]]
  def read(queryParameters: ReadParameter)(id: Identity): F[T]
}

object ReadOnlyStore {

  case class IndexQueryParameters[T, Ordering, Field](paging: Paging, indexContext: T, orderings: List[(Ordering, Direction)], fields: Set[Field])
  case class PagedIndexQueryParameters[Context](paging: Paging, context: Context)

  case class Index[Id, Partial](totalEntities: Int, entities: List[(Id, Partial)])

  sealed trait Error extends Throwable
  object Error {
    case class UnknownId(id: String, cause: Throwable) extends Error {
      override def getMessage: String = s"Id '$id' not known"
    }
  }
}
