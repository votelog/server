package votelog.domain.authorization

import votelog.domain.authentication.User
import votelog.domain.authorization.Role.Permission

// todo: use 'token' instead of user
trait Authorization[F[_]] {
  def hasCapability(user: User, capability: Capability, component: Component): F[Boolean]
  def hasPermission(user: User, permission: Permission): F[Boolean] =
    hasCapability(user, permission.capability, permission.component)
}
