package votelog.domain.authorization

import votelog.domain.authorization.Component.Separator
import votelog.domain.encoding.AsString

/**
 * TODO: rename 'Path'
 *
  * Components form a n-tree where each component
  * is either a root or a descendant of another component
  * if a component a is a descendant of another component,
  * it is said to be contained in that component
  *
  * the encoding of the tree-structure currently happens
  * as part of the location.
  * The name consists of of the last element of the full
  * component path.
  *
  * elements are separated by slashes (`/`).
  */
trait Component {
  def location: String
  def contains(other: Component): Boolean
  def containsOrSelf(other: Component): Boolean = contains(other) || other == this
  def child(name: String): Component
  def child[A: AsString](a: A): Component
  def builder[A: AsString]: Component.Builder[A]
  def name: String
  def parent: Component
}

object Component {
  trait Builder[T] extends Component {
    def build(t: T): Component
  }

  object Builder {
    case class Impl[T](component: Component)(using ev: AsString[T]) extends Builder[T] {
      export component.*

      override def toString: String = component.toString
      override def build(entity: T): Component = component.child(AsString[T].asString(entity))
    }
  }

  case class Impl(location: String) extends Component { self =>
    override def builder[B](using ev: AsString[B]): Component.Builder[B] = Builder.Impl(self)(using ev)
    lazy val _parent = Impl(location.split(Separator).toVector.reverse.drop(1).reverse.mkString(Separator.toString))

    def parent: Component = _parent

    def contains(other: Component): Boolean = {
      val top =
        location.split(Separator)
          .zip(other.location.split(Separator))

      val compared = top.map { case (a, b) => a == b }

      this != other && compared.forall(identity)
    }

    override def containsOrSelf(other: Component): Boolean = contains(other) || other.location == this.location

    override def equals(obj: Any): Boolean =
      obj.isInstanceOf[Component] && obj.asInstanceOf[Component].location == this.location

    override def hashCode(): Int = this.location.hashCode
    def child(name: String): Component = Component(s"$location$Separator$name")
    def child[A: AsString](a: A): Component = child(AsString[A].asString(a))
    def name: String = location.split(Separator).lastOption.getOrElse(s"")
    override def toString: String = location
  }


  def apply(location: String): Component = Impl(location)

  val Root: Component = Component("")
  private val Separator: Char = '/'
}
