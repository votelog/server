package votelog.domain.authorization

trait Auth[F[_], T] {
  type Aux[A] = T => F[A]
}
