package votelog.domain.authorization


import votelog.domain.authorization.Role.Permission
import votelog.domain.encoding.AsString

import java.util.UUID

case class Role(name: String, component: Component, permissions: Set[Permission]) {
  def permits(capability: Capability, component: Component): Boolean =
    permissions
      .filter(_.component.containsOrSelf(component))
      .map(_.capability)
      .contains(capability)
}

object Role {

  case class Id(value: UUID)

  object Id {
    implicit val roleAsString: AsString[Role.Id] = _.value.toString
  }

  case class Permission(capability: Capability, component: Component) {
    def permits(cap: Capability, com: Component): Boolean = this == Permission(cap, com)
  }

  case class Template(name: String, component: Component)
}
