package votelog.domain.authorization

@FunctionalInterface
trait ComponentBuilder[T] {
  def buildComponent(entity: T): Component
}
