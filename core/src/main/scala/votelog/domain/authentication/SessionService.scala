package votelog.domain.authentication

import votelog.domain.authentication.Authentication.Credentials
import votelog.domain.authentication.SessionService.Session

trait SessionService[F[_]] {
  def login(cred: Credentials): F[Either[SessionService.Error, Session]]
  def get: F[Either[SessionService.Error, Session]]
  def logout(): F[Unit]
}

object SessionService {
  case class Session(user: User, token: Token)

  sealed trait Error extends Exception
  object Error {
    case class DecodingError(source: Throwable) extends Error {
      override def getMessage: String = source.getMessage
    }
    case class ServiceError(source: Throwable) extends Error {
      override def getMessage: String = source.getMessage
    }
    case object AuthenticationFailed extends Error
  }

}

object Authentication {

  sealed trait Credentials
  object Credentials {
    case class UserPassword(username: String, password: String) extends Credentials
  }
}
