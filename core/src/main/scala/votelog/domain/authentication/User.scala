package votelog.domain.authentication

import votelog.domain.authorization.Role

import java.util.UUID

case class User(name: String, email: User.Email, passwordHash: String, permissions: Set[Role.Permission], roles: Set[Role])

object User {

  case class Partial(
    name: Option[String],
    email: Option[User.Email],
  )

  val empty: Partial = Partial(None, None)

  case class Id(value: UUID)
  case class Email(value: String)

  sealed trait Field extends Product with Serializable
  object Field {
    case object Name extends Field
    case object Email extends Field

    val values: List[Field] = List(Name, Email)
    val unsafeFromString: (String => Field) =
      (values zip values).map( { case (key, value) => (key.toString, value) }).toMap.apply
  }
}
