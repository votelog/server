package votelog.domain.authentication

case class Token(value: String)
