package votelog.domain.search

import votelog.domain.search.Search.CuriaVistaDocument


trait AsCuriaVistaDocument[Id, Entity] {
  def transform(id: Id, entity: Entity): CuriaVistaDocument
}
