package votelog.domain.search

import cats.effect.Fiber
import votelog.domain.crudi.ReadOnlyStore
import votelog.domain.data.Paging
import votelog.domain.data.Paging.PageSize
import votelog.domain.politics
import votelog.domain.search.Search.CuriaVistaDocument

import java.time.{LocalDate, ZonedDateTime}
import votelog.domain.search.Search.Entity
import votelog.domain.authorization.Component
import votelog.domain.politics.{Context, Language, LegislativePeriod}
import votelog.domain.search.VoteLogIndexer.Event

trait Search[F[_]] {
  def fulltext(language: Option[Language], paging: Paging, query: String): F[ReadOnlyStore.Index[String, CuriaVistaDocument]]
  def retrieve(component: Component, language: Language): F[Seq[CuriaVistaDocument]]
}

trait VoteLogIndexer[F[_]] {
  def indexCuriaVistaEntities(
    context: Context,
    pageSize: PageSize,
    entityType: Set[Entity.Type],
  ): F[String]

  def indexCuriaVistaEntitiesProcess(
    context: Context,
    pageSize: PageSize,
    entityTypes: Set[Entity.Type],
  ): F[Fiber[F, Throwable, String]]

  def buildIndexingEvents(
    context: Context,
    pageSize: PageSize,
    entityTypes: Set[Search.Entity.Type],
  ): F[fs2.Stream[F, Event]]

  def indexEntity(entity: Search.Entity, context: Context): F[CuriaVistaDocument]
}

object VoteLogIndexer {
  case class Indexable(component: Component, document: CuriaVistaDocument)

  enum Event {
    case IndexRead(component: Component, totalEntites: Int)
    case DocumentFound(indexable: Indexable)
    case IndexCompleted(component: Component, completed: ZonedDateTime)
  }
}


trait Catalog[F[_]] {
  val search: Search[F]
  val indexer: VoteLogIndexer[F]
}

object Search {

  sealed trait Entity
  object Entity {
    
    case class Business(entity: politics.Business.Id) extends Entity
    case class Parliamentarian(entity: politics.Parliamentarian.Id) extends Entity
    case class Bill(entity: politics.Bill.Id) extends Entity
    case class Vote(entity: politics.Vote.Id) extends Entity

    enum Type {
      case Business
      case Parliamentarian
      case Bill
      case Vote
    }
  }

  case class CuriaVistaDocument(
    title: String,
    description: String,
    keywords: Set[String],
  )
}


