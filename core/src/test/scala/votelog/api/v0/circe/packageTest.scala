package votelog.api.v0.circe

import io.circe.{Codec, Json}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class packageTest extends AnyWordSpec with Matchers {

  case class WrappedInt(int: Int)

  "deriveUnwrapped" should {
    "do stuff" in {

      implicit val wrappedCodec: Codec[WrappedInt] = deriveUnwrappedCodec

      import io.circe.syntax.*

      WrappedInt(1).asJson.toString shouldBe "1"
      Json.fromInt(1).as[WrappedInt] shouldBe WrappedInt(1)
    }


  }
}
