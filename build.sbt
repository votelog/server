import sbt.Keys.{libraryDependencies, parallelExecution}
import sbtcrossproject.CrossPlugin.autoImport.crossProject
import sbtcrossproject.CrossType
import sbt.{CrossVersion, IntegrationTest}

val httpsVersion = "0.23.12"
val circeVersion = "0.14.3"
val doobieVersion = "1.0.0-RC1"
val catsVersion = "2.6.1"
val scalatestVersion = "3.2.15"
val scalacheckVersion = "1.17.0"
val  tapirVersion = "1.2.10"


jsEnv := new org.scalajs.jsenv.jsdomnodejs.JSDOMNodeJSEnv()

lazy val pureConfig =
  libraryDependencies ++= Seq(
    "com.github.pureconfig" %% "pureconfig-core" % "0.17.2",
  )

lazy val circe =
  libraryDependencies ++= Seq(
    "io.circe" %% "circe-core" % circeVersion,
    "io.circe" %% "circe-generic" % circeVersion,
    "io.circe" %% "circe-parser" % circeVersion,
  )

lazy val logging =
  libraryDependencies ++= Seq(
    "org.log4s" %% "log4s" % "1.10.0",
    "ch.qos.logback" % "logback-classic" % "1.4.1",
    "org.slf4j" % "jul-to-slf4j" % "1.7.32",
  )

lazy val crypto =
  libraryDependencies ++= Seq(
    "org.reactormonk" %% "cryptobits" % "1.3.1" cross CrossVersion.for3Use2_13,
  )


lazy val doobie =
  libraryDependencies ++= Seq(
    "org.tpolecat" %% "doobie-core" % doobieVersion,
    "org.tpolecat" %% "doobie-h2" % doobieVersion,
    "org.tpolecat" %% "doobie-postgres" % doobieVersion,
    "org.tpolecat" %% "doobie-scalatest" % doobieVersion % Test
  )

lazy val decline =
  libraryDependencies += "com.monovore" %% "decline" % "2.4.1"

lazy val flyway =
  libraryDependencies += "org.flywaydb" % "flyway-core"% "9.4.0"

lazy val scaffeine =
  libraryDependencies += "com.github.blemale" %% "scaffeine" % "5.2.1"

lazy val scopt =
  libraryDependencies += "com.github.scopt" %% "scopt" % "4.1.0"

lazy val postgres =
  libraryDependencies += "org.postgresql" % "postgresql" % "42.5.4"

lazy val lucene =
  libraryDependencies += "com.outr" %% "lucene4s" % "1.11.1"

lazy val cats =
  libraryDependencies ++= Seq(
    "org.typelevel" %%% "cats-core" % "2.9.0" withSources(),
    "org.typelevel" %%% "cats-effect" % "3.5.0" withSources(),
  )

val http4s =
  libraryDependencies ++= Seq(
    "org.http4s" %% "http4s-blaze-server" % httpsVersion,
    "org.http4s" %% "http4s-circe" % httpsVersion,
    "org.http4s" %% "http4s-dsl" % httpsVersion,
  )

val tapir =
  libraryDependencies ++= Seq(
     "com.softwaremill.sttp.tapir" %% "tapir-core" % tapirVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-http4s-server" % tapirVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-openapi-docs" % tapirVersion,
    "com.softwaremill.sttp.apispec" %% "openapi-circe-yaml" % "0.3.2",
    "com.softwaremill.sttp.tapir" %% "tapir-json-circe" % tapirVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-cats" % tapirVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-swagger-ui" % tapirVersion,
    "com.softwaremill.sttp.tapir" %% "tapir-redoc" % tapirVersion,
  )

val tapirClient =
  libraryDependencies ++= Seq(
    "com.softwaremill.sttp.tapir" %%% "tapir-core" % tapirVersion,
    "com.softwaremill.sttp.tapir" %%% "tapir-json-circe" % tapirVersion,
    "com.softwaremill.sttp.tapir" %%% "tapir-cats" % tapirVersion,
  )

val sttp =
  libraryDependencies ++= Seq(
    "com.softwaremill.sttp.tapir" %%% "tapir-sttp-client" % "1.7.3",
    "com.softwaremill.sttp.client3" %%% "fs2" % "3.9.0"
  )

val test =
  libraryDependencies ++= Seq(
    "org.scalatest" %% "scalatest" % scalatestVersion % "test, it",
    "org.scalacheck" %% "scalacheck" % scalacheckVersion % "test, it",
    "org.scalatestplus" %% "scalacheck-1-17" % "3.2.15.0" % "test, it",
  )


inThisBuild(List(
  scalacOptions ++= Seq(
    "-deprecation",
    "-feature",
    "-unchecked",
    "-language:implicitConversions",
    "-language:higherKinds",
    "-language:postfixOps",
    "-Xmax-inlines:64",
    "-source:future",
  ),
    scalaVersion := "3.3.0",
    organization := "org.esarbe.votelog",
    version := "0.0.1-SNAPSHOT",
))


val core =
  crossProject(JSPlatform, JVMPlatform)
    .crossType(CrossType.Pure)
    .settings(
      libraryDependencies ++=
        Seq(
          "org.typelevel" %%% "cats-core" % catsVersion withSources(),
          "io.circe" %%% "circe-core" % circeVersion withSources(),
          "io.circe" %%% "circe-generic" % circeVersion withSources(),
          "io.circe" %%% "circe-parser" % circeVersion withSources(),
          "org.scalatest" %%% "scalatest" % scalatestVersion % Test,
          "org.scalacheck" %%% "scalacheck" % "1.15.3" % Test,
          "co.fs2" %%% "fs2-core" % "3.3.0",
        )
    )
    .jsSettings(
      libraryDependencies +=  "io.github.cquiroz" %%% "scala-java-time" % "2.4.0"
    )

lazy val endpoints =
  crossProject(JSPlatform, JVMPlatform)
    .crossType(CrossType.Pure)
    .settings(
      cats,
      tapirClient,
      sttp,
    )
    .dependsOn(core)


lazy val webclient =
  (project in file("webclient"))
    .settings(
      scalaJSUseMainModuleInitializer := true,
      Compile / mainClass := Some("votelog.client.web.Application"),
      libraryDependencies ++=
        Seq(
          "org.typelevel" %%% "cats-core" % catsVersion,
          "org.scala-js" %%% "scalajs-dom" % "2.0.0",
          "in.nvilla" %%% "monadic-html" % "0.5.0-RC1",
          "in.nvilla" %%% "monadic-rx" % "0.5.0-RC1",
          "in.nvilla" %%% "monadic-rx-cats" % "0.5.0-RC1",
          "org.scalatest" %% "scalatest" % "3.2.9" % Test,
          "org.scalacheck" %% "scalacheck" % scalacheckVersion % Test,
        )
    )
    .dependsOn(core.js, endpoints.js)
    .enablePlugins(ScalaJSPlugin)

val webserver =
  (project in file("webserver"))
    .configs(IntegrationTest)
    .settings(
      name := "webserver",
      Compile / mainClass := Some("votelog.app.Webserver"),
      IntegrationTest / parallelExecution := false,
      Defaults.itSettings,
      cats,
      pureConfig,
      circe,
      crypto,
      decline,
      doobie,
      flyway,
      logging,
      lucene,
      tapir,
      http4s,
      test,
      scaffeine,
      scopt,
    )
    .enablePlugins(JavaServerAppPackaging, RpmPlugin)
    .dependsOn(core.jvm, endpoints.jvm)
    .settings(
      IntegrationTest / fork := true,
      IntegrationTest / envVars := Map(
        "SECURITY_PASSWORD_SALT" -> "password-salt",
        "SECURITY_SECRET" -> "secret",
      )
    )


val root =
  (project in file("."))
    .aggregate(webserver, webclient)
    .configs(IntegrationTest)
