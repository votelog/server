package votelog.client.web

import mhtml.{Rx, Var}
import org.scalajs.dom
import votelog.client.web.view.html.DynamicList

import scala.scalajs.js

object TestApp {
  def main(args: Array[String]): Unit = {
    val section = dom.document.createElement("section")
    val node = dom.document.appendChild(section)

    val list = Var(List(1,2,3,4))
    val render = (i: Int) => Rx { <div> {i.toString} </div> }

    DynamicList.mountOn(list, render)(section)

    js.timers.setTimeout(2000) {
      println(s"${node.childNodes.length}")
    }
  }
}
