package votelog.client.web

import mhtml.{Rx, Var}
import org.scalajs.dom
import org.scalajs.dom.HashChangeEvent
import sttp.model.Uri
import votelog.client.Configuration
import votelog.client.implementation.{SttpSessionService, SttpUserStore}
import votelog.client.web.view.Authentication.State.{Authenticated, Unauthenticated}
import votelog.client.web.view.business.BusinessComponent
import votelog.client.web.view.{CrudIndexView, Paging, Router, UserComponent}
import votelog.client.web.view.ngo.NgoComponent
import votelog.domain.authentication.User
import votelog.domain.authorization.Component
import votelog.domain.politics
import votelog.domain.politics.{Business, Context, DecisionsStore, LegislativePeriod, Ngo, Parliamentarian}

import java.util.UUID
import scala.concurrent.Future
import scala.xml.{Group, Node}

object Application {

  val backendUrl = dom.window.document.body.getAttribute("backend-url")
  val baseUri = Uri.unsafeParse(backendUrl)

  val href: Rx[String] = view.Href.value
  val defaultContext: Context = Context(LegislativePeriod.Default.id, politics.Language.German)
  val languageComponent = new view.Language(defaultContext.language)

  val sessionService = new SttpSessionService(baseUri)
  val authComponent = new view.Authentication(sessionService)
  val userComponent: UserComponent = {
    val userStore = new SttpUserStore(baseUri, sessionService)

    val configuration = UserComponent.Configuration(
      Paging.Configuration.default.defaultPageSize,
      Paging.Configuration.default.pageSizes)

    new view.UserComponent(
      Component.Root.child("user"),
      configuration,
      userStore)
  }

  val location: Rx[String] = href.map(_.dropWhile(_ != '#').drop(1)).dropRepeats
  val router = Router.build(baseUri, defaultContext, userComponent, authComponent, languageComponent)

  val appView: Rx[Node] = location.map(router.apply).dropRepeats

  def main(args: Array[String]): Unit = {
    val content =
      <application>
        <header>
          <branding>
            <logo />
            <name>VoteLog</name>
            <slogan>siehe selbst.</slogan>
          </branding>
          <navigation> <!--
            <a href="#/person">Persons</a>
            <a href="#/business">Businesses</a>
            <a href="#/ngo">NGOs</a>
            <a href="#/user">Users</a>
            //-->
          </navigation>
          <user>
            { authComponent.model.map {
                case Authenticated(user) => <a href="#/session">Logout {user.name} </a>
                case Unauthenticated => <span> <a href="#/session">Login</a> or <a href="#/signup">Sign up</a> </span>
              }
            }
          </user>

          <settings>
            { languageComponent.view }
          </settings>
        </header>

        { appView }

        <footer>
          Contact | Blag  gitlab
        </footer>
      </application>

    mhtml.mount(dom.document.body, content)
  }

}
