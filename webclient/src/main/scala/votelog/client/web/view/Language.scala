package votelog.client.web.view
import mhtml.Rx
import votelog.domain.politics

class Language(default: politics.Language)
  extends html.DynamicSelect[politics.Language]("Language", Rx(politics.Language.values), default)