package votelog.client.web.view

import mhtml.{Rx, Var}
import org.scalajs.dom
import org.scalajs.dom.HashChangeEvent

object Href {
  val value: Rx[String] = {
    val rx = Var(dom.window.location.href)
    val listener = (e: HashChangeEvent) => rx := e.newURL
    dom.window.addEventListener("hashchange", listener)
    rx
  }
}
