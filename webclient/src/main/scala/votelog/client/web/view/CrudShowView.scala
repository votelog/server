package votelog.client.web.view

import votelog.domain.crudi.ReadOnlyStore

import scala.concurrent.Future

trait CrudShowView[T, Identity, Partial, Order, Fields] {
  val store: ReadOnlyStore[Future, T, Identity, Partial, Order, Fields]
}
