package votelog.client.web.view

import mhtml.{Cancelable, Rx, Var}
import votelog.client.web.view.Paging.Configuration
import votelog.client.web.view.html.StaticSelect
import votelog.client.web.view.html.tools.{setAs, update}
import votelog.domain.authorization.Component
import votelog.domain.data.Paging.{Offset, PageSize}

import scala.scalajs.js
import scala.xml.Elem

object Paging {
  case class Configuration(
    defaultPageSize: PageSize,
    pageSizes: Seq[PageSize],
    defaultOffset: Offset,
    totalEntities: Int,
  )

  object Configuration {
    val default: Configuration =
      Configuration(
        defaultPageSize = PageSize(20),
        pageSizes = Seq(PageSize(20), PageSize(50), PageSize(100), PageSize(200)),
        defaultOffset = Offset(0),
        totalEntities = 0,
      )
  }
}

class Paging(component: Component, configuration: Configuration) {
  val initialPage: Int = 1

  val pageSize: Var[PageSize] = pageSizeSelect.model
  val page: Var[Int] = Var(initialPage)
  val validPage: Rx[Int] = page.foldp(initialPage){ case (acc, curr) => if (curr >= initialPage) curr else acc}

  lazy val pageSizeSelect =
    new StaticSelect[PageSize](
      legend = "page size",
      options = configuration.pageSizes,
      default = configuration.defaultPageSize,
      clazz = "pageSize",
      id = id("pageSize")(component)
    )

  val offset: Rx[Offset] =
    for {
      page <- validPage
      pageSize <- pageSizeSelect.model
    } yield Offset((page - 1) * pageSize.value )


  def incIfGtZero(by: Int)(i: Int): Int = (i + by) max 1

  val view: Elem = {
    <fieldset class="control paging">
      <legend>Paging</legend>
      <dl class="page">
        <dt><label for={ id("page")(component) } >Page</label></dt>
        <dd><button onclick={ update(page)(incIfGtZero(-1)) }>{"<"}</button></dd>
        <dd>
          <input
            id={ id("page")(component) }
            type="number"
            min="1"
            value={validPage.map(_.toString)}
            onchange={ (e: js.Dynamic) => setAs(page)(_.asInstanceOf[String].toInt)(e)
            }
          > </input>
        </dd>
        <dd><button onclick={update(page)((incIfGtZero(1)))}>{">"}</button></dd>
      </dl>
      { pageSizeSelect.view }
    </fieldset>
  }
}
