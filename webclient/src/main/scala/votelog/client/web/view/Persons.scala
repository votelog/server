package votelog.client.web.view

import cats.implicits.*
import mhtml.future.syntax.*
import mhtml.{Cancelable, Rx, Var}
import votelog.client.web.view.html.{DynamicList, StaticSelect}
import votelog.domain.authorization.Component
import votelog.domain.crudi.ReadOnlyStore.IndexQueryParameters
import votelog.domain.data
import votelog.domain.data.Paging.PageSize
import votelog.domain.data.Sorting.Direction.{Ascending, Descending}
import votelog.domain.politics.*
import votelog.persistence.{BusinessStore, PersonStore}

import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import scala.concurrent.Future
import scala.util.{Failure, Success}
import scala.xml.{Group, Node}
import scala.collection.mutable

class Persons(
  component: Component,
  persons: PersonStore[Future],
  businesses: BusinessStore[Future],
  voting: DecisionsStore[Future],
  language: Rx[votelog.domain.politics.Language],
) {

  val pagingConfiguration: Paging.Configuration = Paging.Configuration.default
  val paging: Paging = new Paging(component.child("paging"), pagingConfiguration)
  var cache: mutable.Map[Parliamentarian.Id,Rx[Parliamentarian]] = collection.mutable.Map.empty[Parliamentarian.Id, Rx[Parliamentarian]]
  var viewCancelable: Option[Cancelable] = None

  val legislativePeriod =
    new StaticSelect(
      legend = "legislative period",
      options = LegislativePeriod.ids,
      default = LegislativePeriod.Default.id,
      clazz = "legislativePeriod",
      id = "legislativePeriod"
    )

  val previewFields: Set[Parliamentarian.Field] =
    Set(Parliamentarian.Field.FirstName, Parliamentarian.Field.LastName, Parliamentarian.Field.Party)

  val queryParameters: Rx[Context] =
    for {
      language <- language
      legislativePeriod <- legislativePeriod.model
    } yield Context(legislativePeriod, language)

  // will run like: None -> Some(List) -> None -> Some(List)
  val unstableIds: Rx[Option[Either[Throwable, List[(Parliamentarian.Id, PersonPartial)]]]] =
    for {
      offset <- paging.offset
      pageSize <- paging.pageSize
      queryParameters <- queryParameters.dropRepeats
      orderings = List(Parliamentarian.Field.LastName -> Ascending, Parliamentarian.Field.FirstName -> Ascending)
      indexQueryParams: IndexQueryParameters[Context, Parliamentarian.Field, Parliamentarian.Field] =
        IndexQueryParameters(data.Paging(offset, pageSize), queryParameters, orderings, previewFields)
      ids <- persons.index(indexQueryParams).map(_.entities).toRx
    }  yield ids match {
      case Some(Success(ids)) => Some(Right(ids))
      case Some(Failure(error)) => Some(Left(error))
      case None => None
    }

  // keeps last list, None doesn't appear
  val ids: Rx[List[(Parliamentarian.Id, PersonPartial)]] = unstableIds.foldp(List.empty[(Parliamentarian.Id, PersonPartial)]){
    case (_, Some(Right(ids))) => ids
    case (acc, _) => acc
  }

  val indexError: Rx[Iterable[Throwable]] = unstableIds.map(_.toList.flatMap(_.swap.toList)).dropRepeats

  val maybeSelected: Var[Option[Parliamentarian.Id]] = Var(None)

  val selectedEntity: Rx[Option[Either[Throwable, Parliamentarian]]] =
    for {
      selectedId <- maybeSelected.dropRepeats
      language <- language.dropRepeats
      entity = selectedId.map(id => persons.read(language)(id).toRx)
      maybeResult <- entity match {
        case Some(rx) => rx
        case None => Rx(None)
      }
    } yield maybeResult.map(_.toEither)

  val maybeEntity: Rx[Option[Parliamentarian]] =
    selectedEntity.collect {
      case Some(Right(person)) => Option(person)
    }(None)

  val maybeIdOrEntity: Rx[Option[Either[Parliamentarian.Id, Parliamentarian]]] =
    maybeSelected.map(_.map(Left.apply))
        .merge(maybeEntity.map(_.map(Right.apply)))

  val entityError: Rx[Iterable[Throwable]] =
    selectedEntity.collect {
      case Some(Left(failure)) => List(failure)
    }(Nil)

  val model: Rx[List[Parliamentarian]] = {
    for {
      offset <- paging.offset
      pageSize <- paging.pageSize
      queryParameters <- queryParameters
      orderings = List(Parliamentarian.Field.LastName -> Descending, Parliamentarian.Field.FirstName -> Descending)
      indexQueryParams: IndexQueryParameters[Context, Parliamentarian.Field, Parliamentarian.Field]
        = IndexQueryParameters(data.Paging(offset, pageSize), queryParameters, orderings, previewFields)
      ids = persons.index(indexQueryParams)
      persons <-
        ids
          .flatMap { index => Future.sequence(index.entities.map { case (id, _) => persons.read(queryParameters.language)(id)}) }
          .toRx
          .collect { case Some(Success(persons)) => persons }(Nil)
    } yield persons
  }

  val errors: Rx[Iterable[Throwable]] = indexError.merge(entityError)

  def previewPerson: Either[Parliamentarian.Id, Parliamentarian] => Node = {
    case Right(p) =>
      <a href={s"#${id(p.id.value.toString)(component)}"}>
        <dl class="entity person" data-id={p.id.value.toString}>
          <dt>Name</dt><dd>{p.firstName.value} foo {p.lastName.value}</dd>
        </dl>
      </a>

    case Left(id) =>
      <dl class="entity person loading" data-id={id.value.toString}></dl>
  }

  val votes: Rx[Either[Throwable, List[(Vote.Id, Decision)]]] =
    for {
      maybeSelected <- maybeSelected
      context <- queryParameters
      votes <- maybeSelected match {
        case Some(id) =>
          voting
            .decisionsByPerson(context)(id)
            .toRx.collect {
            case Some(Success(votes)) => Right(votes)
            case Some(Failure(error)) => Left(error)
          }(Right(Nil))
        case None => Rx(Right(Nil))
      }
    } yield votes

  def renderVotes: Either[Throwable, List[(Vote.Id, Decision)]] => Node = {
    case Right(votes) =>
        Group(votes.map {
          case (registration, votum) =>
            <dl class="entity list vote">
              <dt> {registration.value.toString} </dt>
              <dd> {Decision.asString(votum)} </dd>
            </dl>
          })
    case Left(error) =>
      <p>
        Error occured: {error.getMessage}
      </p>
  }

  def renderEntity: Option[Either[Parliamentarian.Id , Parliamentarian]] => Node = {
    case Some(Right(entity)) =>
      <dl class="entity person" data-id={entity.id.value.toString}>
        <dt class="name">Name</dt>
        <dd>{entity.firstName.value} {entity.lastName.value} </dd>
        <dt class="canton">Canton</dt>
        <dd>{entity.canton.value}</dd>
        <dt class="party">Party</dt>
        <dd>{entity.party.value}</dd>
      </dl>
    case Some(Left(id)) => <dl class="loading entity person" data-id={id.value.toString}></dl>
    case None => <dl class="empty entity person"></dl>
  }

  def renderPartial (ref: Parliamentarian.Id, partial: PersonPartial): Rx[Node] = {
    Rx({
      <a href={s"#${id(ref.value.toString)(component)}"}>
        <span class="entity person" data-id={ref.value.toString}>
          {
            for {
              firstName <- partial.firstName
              lastName <- partial.lastName
            } yield s"$firstName $lastName"
          }
        </span>
      </a>
    })
  }

  val render: Parliamentarian.Id => Rx[Node] = { (id: Parliamentarian.Id) =>
    val person: Rx[Either[Throwable, Either[Parliamentarian.Id, Parliamentarian]]] =
      for {
        language <- language
        person <- persons.read(language)(id).toRx
      } yield person match {
        case Some(Success(person)) => Right(Right(person))
        case Some(Failure(exception)) => Left(exception)
        case None => Right(Left(id))
      }

    person.map {
      case Right(person) => previewPerson(person)
      case Left(exception) => <div> { exception.getMessage } </div>
    }
  }

  def mountView(e: org.scalajs.dom.Node): Unit = {

    viewCancelable = Some(DynamicList.mountOn(ids.dropRepeats, renderPartial.tupled)(e))
  }

  def umountView(e: org.scalajs.dom.Node): Unit = {
    viewCancelable.foreach(_.cancel)
  }

  val view: Group = Group {
    <controls>
      { paging.view }
    </controls>

    <article class="person">
      <ul class="index"
        mhtml-onmount={ (node: org.scalajs.dom.Node) => mountView(node) }
        mhtml-onunmount={ (node: org.scalajs.dom.Node) => umountView(node) }
      />

      { maybeIdOrEntity.map(renderEntity) }
      { votes.map(renderVotes) }

    </article>

    <messages>
      { errors.map { _.toList.map { error => <error> { error.getMessage } </error> } } }
    </messages>
  }

}
