package votelog.client.web.view

import sttp.model.Uri
import votelog.client.web.view
import votelog.client.web.view.business.BusinessComponent
import votelog.client.web.view.ngo.NgoComponent
import votelog.domain.authentication.User
import votelog.domain.authorization.Component
import votelog.domain.politics
import votelog.domain.politics.{Business, Context, DecisionsStore, LegislativePeriod, Ngo, Parliamentarian}
import votelog.client.implementation.*

import java.util.UUID
import scala.concurrent.Future
import scala.xml.Node

class Router(
  userComponent: UserComponent,
  ngoComponent: NgoComponent,
  authComponent: view.Authentication,
  businessComponent: BusinessComponent,
  personsComponent: view.Persons,
) {
  // #foo/bar/qux
  def apply(location: String): Node = {
    location.drop(1).split('/').toList match {
      case "user" :: Nil =>
        userComponent.view
      case "user" :: id :: Nil =>
        userComponent.selectedId := Some(User.Id(UUID.fromString(id)))
        userComponent.view

      case "ngo" :: Nil =>
        ngoComponent.view
      case "ngo" :: id :: Nil =>
        ngoComponent.selectedId := Some(Ngo.Id(UUID.fromString(id)))
        ngoComponent.view

      case "business" :: Nil =>
        businessComponent.view

      case "business" :: id :: Nil =>
        businessComponent.selectedId := Some(Business.Id(id.toInt))
        businessComponent.view

      case "session" :: Nil => authComponent.view
      case "signup" :: Nil => userComponent.create.form("Sign Up")
      case "person" :: Nil => personsComponent.view
      case "person" :: id :: Nil =>
        personsComponent.maybeSelected := Some(Parliamentarian.Id(id.toInt))
        personsComponent.view
      case a => <message>Not found</message>
    }
  }
}

object Router {
  def build(
    baseUri: Uri,
    defaultContext: Context,
    userComponent: UserComponent,
    authComponent: view.Authentication,
    languageComponent: view.Language,
  ): Router = {

    val paging: Paging.Configuration = Paging.Configuration.default

    val root = Component.Root

    val personsStore = new SttpParliamentarianStore(baseUri)

    val ngoStore = new SttpOrganisationStore(baseUri)
    val businessStore = new SttpBusinessStore(baseUri)
    val votesStore = new SttpVoteStore(baseUri)
    val decisionsStore: DecisionsStore[Future] = new SttpDecisionsStore(baseUri)

    val personsComponent =
      new view.Persons(
        root.child("person"),
        personsStore,
        businessStore,
        decisionsStore,
        languageComponent.model)

    val ngoComponent: NgoComponent = {
      val configuration =
        NgoComponent.Configuration(
          defaultContext,
          paging.defaultPageSize,
          paging.pageSizes,
        )
      new NgoComponent(
        root.child("ngo"),
        configuration,
        ngoStore)
    }


    val businessComponent: BusinessComponent = {
      val configuration = BusinessComponent.Configuration(
        defaultContext,
        paging.defaultPageSize,
        paging.pageSizes)
      new view.business.BusinessComponent(
        root.child("business"),
        configuration,
        businessStore,
        languageComponent.model)
    }

    trait RoutedComponent {
      def apply(path: String): Option[Node]
    }

    new Router(
      userComponent,
      ngoComponent,
      authComponent,
      businessComponent,
      personsComponent,
    )
  }

}
