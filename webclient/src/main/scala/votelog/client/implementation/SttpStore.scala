package votelog.client.implementation

import sttp.model.Uri
import votelog.api.v1.StoreEndpoint
import votelog.domain.crudi.Store
import votelog.syntax.implicits.*

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

open class SttpStore[Entity, Identity, Recipe, Partial, ReadParameter, IndexParameter](
  baseUri: Uri,
  endpoint: StoreEndpoint[Entity, Identity, Recipe, Partial, ReadParameter, IndexParameter],
) extends
  SttpReadOnlyStore[Entity, Identity, Partial, ReadParameter, IndexParameter](baseUri, endpoint),
  Store[Future, Entity, Identity, Recipe, Partial, ReadParameter, IndexParameter] {

  def create(r: Recipe): Future[Identity] = {
    interpreter
      .toSecureClientThrowDecodeFailures(endpoint.create, Some(baseUri), backend)
      .apply(None)
      .apply(r)
      .flatMap(Future.fromEither(new Exception(_)))
  }

  def delete(id: Identity): Future[Unit] = {
    interpreter
      .toSecureClientThrowDecodeFailures(endpoint.delete, Some(baseUri), backend)
      .apply(None)
      .apply(id)
      .flatMap(Future.fromEither(new Exception(_)))
  }

  def update(id: Identity, r: Recipe): Future[Entity] = {
    interpreter
      .toSecureClientThrowDecodeFailures(endpoint.update, Some(baseUri), backend)
      .apply(None)
      .apply((id, r))
      .flatMap(Future.fromEither(new Exception(_)))
  }
}
