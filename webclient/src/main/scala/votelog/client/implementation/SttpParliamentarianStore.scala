package votelog.client.implementation

import sttp.model.Uri
import votelog.api.v1.{ParliamentariansEndpoint, ReadOnlyEndpoint, StoreEndpoint}
import votelog.domain.crudi.ReadOnlyStore.IndexQueryParameters
import votelog.domain.politics.*
import votelog.persistence.PersonStore

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import votelog.syntax.implicits.fromEither

class SttpParliamentarianStore(
  baseUri: Uri,
) extends
  SttpReadOnlyStore(baseUri, ParliamentariansEndpoint),
  ParliamentarianService[Future] {

  def decisionsByPerson(context: Context)(person: Parliamentarian.Id): Future[List[(Vote.Id, Decision)]] = {
    interpreter
      .toSecureClientThrowDecodeFailures(ParliamentariansEndpoint.readDecisions, Some(baseUri), backend)
      .apply(None)
      .apply((context, person))
      .flatMap(Future.fromEither(new Exception(_)))
  }

}
