package votelog.client.implementation

import sttp.capabilities.WebSockets
import sttp.client3.{FetchBackend, SttpBackend}
import sttp.model.Uri
import sttp.tapir.DecodeResult
import sttp.tapir.client.sttp.SttpClientInterpreter
import votelog.api.v1.OrganisationsEndpoint
import votelog.client.Configuration
import votelog.domain.organisation.OrganisationService
import votelog.domain.organisation.OrganisationService.Recipe
import votelog.domain.politics.Ngo

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class SttpOrganisationService(uri: Uri) extends OrganisationService[Future] {

  val backend: SttpBackend[Future, WebSockets] = FetchBackend()

  def createOrganisation(recipe: Recipe): Future[Ngo.Id] = {
    SttpClientInterpreter()
      .toSecureClientThrowDecodeFailures(OrganisationsEndpoint.createOrganisation, Some(uri), backend)
      .apply(None)
      .apply(recipe)
      .flatMap {
        case Right(id) => Future.successful(id)
        case Left(error) => Future.failed(new Exception(error))
      }

  }

}
