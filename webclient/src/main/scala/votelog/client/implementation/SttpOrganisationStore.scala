package votelog.client.implementation

import sttp.model.Uri
import votelog.api.v1.OrganisationsEndpoint as endpoint
import votelog.domain.crudi.ReadOnlyStore.{Index, IndexQueryParameters}
import votelog.domain.politics.Ngo.*
import votelog.domain.politics.{Decision, LegislativePeriod, Ngo, Vote}
import votelog.persistence.OrganisationStore
import votelog.persistence.OrganisationStore.Recipe
import votelog.util.Slug

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import votelog.syntax.implicits.fromEither

class SttpOrganisationStore(
  baseUri: Uri,
) extends
  SttpStore[Ngo, Ngo.Id, Recipe, Ngo.Partial, Unit, IndexQueryParameters[Unit, Ngo.Field, Ngo.Field]](baseUri, endpoint),
  OrganisationStore[Future] {

  override def bySlug(slug: Slug): Future[(Id, votelog.domain.politics.Ngo)] = {
    interpreter
      .toSecureClientThrowDecodeFailures(endpoint.bySlug, Some(baseUri), backend)
      .apply(None)
      .apply(slug)
      .flatMap(Future.fromEither(new Exception(_)))
  }

  override def deletePreference(ngo: Id, vote: Vote.Id): Future[Unit] = {
    interpreter
      .toSecureClientThrowDecodeFailures(endpoint.deletePreference, Some(baseUri), backend)
      .apply(None)
      .apply((ngo, vote))
      .flatMap(Future.fromEither(new Exception(_)))
  }

  override def preferences(
    lp: LegislativePeriod.Id,
    ngo: Id,
  ): Future[List[(Vote.Id, OrganisationStore.DecisionPreference)]] = {
    interpreter
      .toSecureClientThrowDecodeFailures(endpoint.indexPreferences, Some(baseUri), backend)
      .apply(None)
      .apply((lp, ngo))
      .flatMap(Future.fromEither(new Exception(_)))
  }

  override def readPreference(ngo: Id, vote: Vote.Id): Future[OrganisationStore.DecisionPreference] = {
    interpreter
      .toSecureClientThrowDecodeFailures(endpoint.readPreference, Some(baseUri), backend)
      .apply(None)
      .apply((ngo, vote))
      .flatMap(Future.fromEither(new Exception(_)))
  }

  override def updatePreference(ngo: Id, vote: Vote.Id, preference: Decision): Future[Unit] = {
    interpreter
      .toSecureClientThrowDecodeFailures(endpoint.updatePreference, Some(baseUri), backend)
      .apply(None)
      .apply((ngo, vote, preference))
      .flatMap(Future.fromEither(new Exception(_)))
  }
}
