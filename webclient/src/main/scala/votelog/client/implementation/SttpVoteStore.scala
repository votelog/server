package votelog.client.implementation

import sttp.model.Uri
import votelog.api.v1.VotesEndpoint
import votelog.domain.politics.Vote
import votelog.persistence.VoteStore
import votelog.syntax.implicits.*

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class SttpVoteStore(
  baseUri: Uri,
) extends
  SttpReadOnlyStore(baseUri, VotesEndpoint),
  VoteStore[Future] {

  override def fetchByIds(
    language: ReadParameters,
    ids: Set[Vote.Id]
  ): Future[List[(Vote.Id, Vote)]] = {
    interpreter
      .toSecureClientThrowDecodeFailures(VotesEndpoint.fetchByIds, Some(baseUri), backend)
      .apply(None)
      .apply((language, ids))
      .flatMap(Future.fromEither(new Exception(_)))
  }

}
