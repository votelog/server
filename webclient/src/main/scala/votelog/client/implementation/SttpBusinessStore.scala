package votelog.client.implementation

import sttp.model.Uri
import votelog.api.v1.{BusinessesEndpoint, ReadOnlyEndpoint}
import votelog.domain.crudi.ReadOnlyStore.IndexQueryParameters
import votelog.domain.politics.{Bill, Business, Context, Language}
import votelog.persistence.BusinessStore

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import votelog.syntax.implicits.fromEither

class SttpBusinessStore(
  baseUri: Uri,
) extends
  SttpReadOnlyStore(baseUri, BusinessesEndpoint),
  BusinessStore[Future] {

  def billsForBusiness(context: Context, business: Business.Id ): Future[List[(Bill.Id, Bill.Partial)]] = {
    interpreter
      .toSecureClientThrowDecodeFailures(BusinessesEndpoint.billsForBusiness, Some(baseUri), backend)
      .apply(None)
      .apply((context, business))
      .flatMap(Future.fromEither(new Exception(_)))
  }

  def fetchByIds(language: Language, ids: Set[Business.Id] ): Future[List[(Business.Id, Business)]] = {
    interpreter
      .toSecureClientThrowDecodeFailures(BusinessesEndpoint.fetchByIds, Some(baseUri), backend)
      .apply(None)
      .apply((language, ids))
      .flatMap(Future.fromEither(new Exception(_)))
  }

}
