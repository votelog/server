package votelog.client.implementation

import sttp.model.Uri
import votelog.domain.politics.{Context, Decision, DecisionsStore, Parliamentarian, Vote}

import scala.concurrent.Future
import scala.util.Try

class SttpDecisionsStore(
  baseUri: Uri,
) extends DecisionsStore[Future] {
  override def decisionsForVoteRegistration(context: Context)
    (vote: Vote.Id): Future[List[(Parliamentarian.Id, Decision)]] = Future.fromTry(Try(???))

  override def decisionsByPerson(context: Context)
    (person: Parliamentarian.Id): Future[List[(Vote.Id, Decision)]] = Future.fromTry(Try(???))

  override def decisionsByContext(context: Context): fs2.Stream[Future, (Vote.Id, Parliamentarian.Id, Decision)] =
    fs2.Stream.empty
}
