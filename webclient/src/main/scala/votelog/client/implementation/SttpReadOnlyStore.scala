package votelog.client.implementation

import sttp.capabilities.WebSockets
import sttp.client3.{FetchBackend, SttpBackend}
import sttp.model.Uri
import sttp.tapir.client.sttp.SttpClientInterpreter
import votelog.api.v1.{ReadOnlyEndpoint, StoreEndpoint}
import votelog.domain.crudi.ReadOnlyStore
import votelog.domain.crudi.ReadOnlyStore.Index

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import votelog.syntax.implicits.fromEither

open class SttpReadOnlyStore[Entity, Identity, Partial, ReadParameter, IndexParameter](
  baseUri: Uri,
  endpoint: ReadOnlyEndpoint[Entity, Identity, Partial, ReadParameter, IndexParameter]
) extends ReadOnlyStore[Future, Entity, Identity, Partial,  ReadParameter, IndexParameter]{

  protected val interpreter: SttpClientInterpreter = SttpClientInterpreter()
  protected val backend: SttpBackend[Future, WebSockets] = FetchBackend()

  def index (queryParameters: IndexParameter): Future[Index[Identity, Partial]] = {
    interpreter.toSecureClientThrowDecodeFailures(endpoint.index, Some(baseUri), backend)
      .apply(None)
      .apply(queryParameters)
      .flatMap(Future.fromEither(new Exception(_)))
  }

  def read (queryParameters: ReadParameter)(id: Identity): Future[Entity] = {
    interpreter
      .toSecureClientThrowDecodeFailures(endpoint.read, Some(baseUri), backend)
      .apply(None)
      .apply((queryParameters, id))
      .flatMap(Future.fromEither(new Exception(_)))
  }
}
