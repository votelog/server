package votelog.client.implementation

import sttp.model.Uri
import votelog.api.v1.UsersEndpoint
import votelog.domain.authentication.{SessionService, User}
import votelog.domain.authorization.Role
import votelog.persistence.UserStore
import votelog.syntax.implicits.*

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class SttpUserStore(
  baseUri: Uri,
  sessionService: SessionService[Future],
) extends
  SttpStore(baseUri, UsersEndpoint),
  UserStore[Future] {

  def findByName(name: String): Future[Option[(User.Id, User)]] = {
    sessionService.get.flatMap { session =>
      interpreter
        .toSecureClientThrowDecodeFailures(UsersEndpoint.findByName, Some(baseUri), backend)
        .apply(session.toOption.map(_.token))
        .apply(name)
        .flatMap(Future.fromEither(new Exception(_)))
    }
  }

  def grantRole(user: User.Id, role: Role.Id): Future[Unit] = {
    sessionService.get.flatMap { session =>
      interpreter
        .toSecureClientThrowDecodeFailures(UsersEndpoint.grantRole, Some(baseUri), backend)
        .apply(session.toOption.map(_.token))
        .apply((user, role))
        .flatMap(Future.fromEither(new Exception(_)))
    }
  }

  def revokeRole(user: User.Id, role: Role.Id): Future[Unit] = {
    sessionService.get.flatMap { session =>
      interpreter
        .toSecureClientThrowDecodeFailures(UsersEndpoint.revokeRole, Some(baseUri), backend)
        .apply(session.toOption.map(_.token))
        .apply((user, role))
        .flatMap(Future.fromEither(new Exception(_)))
    }
  }
}