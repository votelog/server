package votelog.client.implementation

import cats.implicits.*
import sttp.capabilities.WebSockets
import sttp.client3.{FetchBackend, SttpBackend}
import sttp.model.Uri
import sttp.tapir.client.sttp.SttpClientInterpreter
import sttp.tapir.model.UsernamePassword
import votelog.api.v1.SessionEndpoint
import votelog.domain.authentication.Authentication.Credentials
import votelog.domain.authentication.Authentication.Credentials.UserPassword
import votelog.domain.authentication.SessionService.Error.ServiceError
import votelog.domain.authentication.SessionService.Session
import votelog.domain.authentication.{SessionService, Token, User}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class SttpSessionService(
  baseUri: Uri,
) extends SessionService[Future] {

  val endpoint = SessionEndpoint

  protected val interpreter: SttpClientInterpreter = SttpClientInterpreter()
  protected val backend: SttpBackend[Future, WebSockets] = FetchBackend()

  def get: Future[Either[SessionService.Error, Session]] = {
    interpreter
      .toSecureClientThrowDecodeFailures(endpoint.read, Some(baseUri), backend)
      .apply(None)
      .apply(())
      .map {
        _.map(Session.apply).left.map(msg => ServiceError(new Throwable(msg)))
      }
  }

  def login(cred: Credentials): Future[ Either[SessionService.Error, Session]] = {
    val usernamePassword = cred match {
      case UserPassword(username, password) => UsernamePassword(username, Some(password))
    }

    interpreter
      .toClientThrowDecodeFailures(endpoint.createSession, Some(baseUri), backend)
      .apply(usernamePassword)
      .map {
        _.map(Session.apply).left.map(msg => ServiceError(new Throwable(msg)))
      }
  }

  def logout(): Future[Unit] = {
    interpreter
      .toSecureClientThrowDecodeFailures(endpoint.delete, Some(baseUri), backend)
      .apply(None)
      .apply(())
      .void
  }

}
