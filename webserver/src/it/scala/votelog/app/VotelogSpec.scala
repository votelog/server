package votelog.app

import cats.effect.IO
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import pureconfig.{ConfigReader, ConfigSource}
import pureconfig.generic.derivation.default.*
import votelog.app
import votelog.domain.data.Paging.{Offset, PageSize}
import votelog.domain.politics.{Context, Language, LegislativePeriod}
import votelog.app.implementation.Log4SLogger
import cats.effect.unsafe.implicits.global
import votelog.domain.crudi.ReadOnlyStore.IndexQueryParameters
import votelog.domain.data.Paging

class VotelogSpec extends AnyFlatSpec with Matchers {

  implicit val configReader: ConfigReader[app.Configuration] = ConfigReader.derived[app.Configuration]

  "VoteLog" should "able to list persons in various orders" in {
    lazy val loadConfiguration =
      IO(ConfigSource.default.at("votelog").loadOrThrow[app.Configuration])

    implicit val log: Log4SLogger[IO] = new Log4SLogger[IO](org.log4s.getLogger)

    val result =
      for {
        configuration <- loadConfiguration
        voteLog = VoteLog[IO](configuration)
        persons <- voteLog.use { voteLog =>
          voteLog.politicians.index(
            IndexQueryParameters(
              Paging(
                Offset(0),
                PageSize(10),
              ),
              Context(LegislativePeriod.Id(51), Language.German),
              List.empty,
              Set.empty
            ))
        }

      } yield persons.entities.length shouldBe 10

    result.unsafeRunSync()
  }

}
