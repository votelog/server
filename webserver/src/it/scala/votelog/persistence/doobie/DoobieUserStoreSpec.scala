package votelog.persistence.doobie

import cats.effect.IO
import cats.effect.unsafe.implicits.global
import pureconfig.generic.derivation.default
import org.scalatest.Inside
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import pureconfig.{ConfigReader, ConfigSource}
import pureconfig.generic.derivation.default.*
import votelog.app
import votelog.app.Database
import votelog.app.implementation.{Log4SLogger, UserCapabilityAuthorization}
import votelog.crypto.PasswordHasher
import votelog.domain.authentication.User
import votelog.domain.authorization.Role.Permission
import votelog.domain.authorization.{Capability, Component, Role}
import votelog.domain.crudi.ReadOnlyStore.IndexQueryParameters
import votelog.domain.data.Paging
import votelog.domain.data.Paging.{Offset, PageSize}
import votelog.domain.data.Sorting.Direction.Descending
import votelog.persistence.UserStore.{Password, Recipe}
import votelog.persistence.{StoreSpec, UserStore}

import scala.util.Random

class DoobieUserStoreSpec
  extends AnyFlatSpec
    with StoreSpec
    with ScalaFutures
    with Matchers
    with Inside {

  implicit val log: Log4SLogger[IO] = new Log4SLogger[IO](org.log4s.getLogger)

  lazy val configuration = ConfigSource.default.at("votelog.database").loadOrThrow[Database.Configuration]
  val transactor = Database.buildTransactor[IO](configuration)

  val hasher = new PasswordHasher[IO] {
    override def hashPassword(password: String): IO[String] = IO.pure(s"hashed$password")
  }

  val creationRecipe: Recipe = UserStore.Recipe("name", User.Email("email"), Password.Clear("password"))
  val createdEntity: User.Id => User = _ => User("name", User.Email("email"), "hashedpassword", Set.empty, Set.empty)
  val updatedRecipe: Recipe = Recipe("new name", User.Email("new email"), Password.Clear("new password"))
  val updatedEntity: User.Id => User = _ => User("new name", User.Email("new email"), "hashednew password", Set.empty, Set.empty)
  val indexQueryParameters: IndexQueryParameters[Unit, User.Field, User.Field] =
    IndexQueryParameters(Paging(Offset(0), PageSize(10)), (), List(User.Field.Name -> Descending), User.Field.values.toSet )
  val partialEntity = User.Partial(Some("name"), Some(User.Email("email")))

  val userStore =
    transactor.use { transactor =>

      val roleStore = new DoobieRoleStore(transactor)
      val store = new DoobieUserStore(transactor, roleStore, hasher)

      aStore(store, creationRecipe, createdEntity, updatedRecipe, updatedEntity, (a: User.Id) => partialEntity)((), indexQueryParameters)
  }

  it should behave like userStore.unsafeRunSync()

  it should "create a user and a role and grant permissions to the role and grant the role to the user" in
    transactor.use { transactor =>

      val id = Random.nextInt()
      val component = Component(s"$id/foo/bar/qux")

      val roleStore: DoobieRoleStore[IO] = new DoobieRoleStore(transactor)
      val users = new DoobieUserStore(transactor, roleStore, hasher)
      val roles = new DoobieRoleStore(transactor)
      val authorization = new UserCapabilityAuthorization[IO]()

      for {
        userId <- users.create(UserStore.Recipe(s"test-$id", User.Email(s"$id@."), Password.Clear("pwd")))
        roleId <- roles.create(Role.Template("Owner", component))
        _ <- roles.grantPermission(roleId, Permission(Capability.Create, component))
        _ <- roles.grantPermission(roleId, Permission(Capability.Delete, component))

        user <- users.read(())(userId)
        hasCreate <- authorization.hasCapability(user, Capability.Create, component)
        hasDelete <- authorization.hasCapability(user, Capability.Delete, component)
        hasRead   <- authorization.hasCapability(user, Capability.Read, component)
        hasUpdate <- authorization.hasCapability(user, Capability.Update, component)
        _ = hasCreate shouldBe false
        _ = hasDelete shouldBe false
        _ = hasRead shouldBe false
        _ = hasUpdate shouldBe false

        _ <- users.grantRole(userId, roleId)

        user <- users.read(())(userId)
        hasCreate <- authorization.hasCapability(user, Capability.Create, component)
        hasDelete <- authorization.hasCapability(user, Capability.Delete, component)
        hasRead   <- authorization.hasCapability(user, Capability.Read, component)
        hasUpdate <- authorization.hasCapability(user, Capability.Update, component)
        _ = hasCreate shouldBe true
        _ = hasDelete shouldBe true
        _ = hasRead shouldBe false
        _ = hasUpdate shouldBe false

        _ <- roles.revokePermission(roleId, Permission(Capability.Create, component))
        _ <- roles.revokePermission(roleId, Permission(Capability.Delete, component))
        _ <- roles.grantPermission(roleId, Permission(Capability.Read, component))
        _ <- roles.grantPermission(roleId, Permission(Capability.Update, component))

        user <- users.read(())(userId)
        hasCreate <- authorization.hasCapability(user, Capability.Create, component)
        hasDelete <- authorization.hasCapability(user, Capability.Delete, component)
        hasRead   <- authorization.hasCapability(user, Capability.Read, component)
        hasUpdate <- authorization.hasCapability(user, Capability.Update, component)
        _ = hasCreate shouldBe false
        _ = hasDelete shouldBe false
        _ = hasRead shouldBe true
        _ = hasUpdate shouldBe true

        _ <- users.revokeRole(userId, roleId)
        user <- users.read(())(userId)
        hasCreate <- authorization.hasCapability(user, Capability.Create, component)
        hasDelete <- authorization.hasCapability(user, Capability.Delete, component)
        hasRead   <- authorization.hasCapability(user, Capability.Read, component)
        hasUpdate <- authorization.hasCapability(user, Capability.Update, component)
        _ = hasCreate shouldBe false
        _ = hasDelete shouldBe false
        _ = hasRead shouldBe false
        _ = hasUpdate shouldBe false

      } yield ()
    }.unsafeRunSync()
}
