package votelog.persistence.doobie

import cats.effect.IO
import cats.effect.kernel.Resource
import cats.effect.unsafe.implicits.global
import doobie.util.transactor
import doobie.util.transactor.Transactor
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import pureconfig.{ConfigReader, ConfigSource}
import pureconfig.generic.derivation.default.*
import votelog.app
import votelog.app.Database
import votelog.app.Database.Configuration.MigrationMode
import votelog.app.Database.{Configuration, buildTransactor}
import votelog.app.implementation.Log4SLogger
import votelog.domain.crudi.ReadOnlyStore.{Index, IndexQueryParameters}
import votelog.domain.data.Paging.{Offset, PageSize}
import votelog.domain.data.{Paging, Sorting}
import votelog.domain.data.Sorting.Direction.Ascending
import votelog.domain.politics.{Context, Language, LegislativePeriod, Parliamentarian}
import votelog.orphans.doobie.domain.politics.PersonInstances

class DoobieParliamentarianStoreSpec extends AnyFlatSpec with Matchers with PersonInstances {

  lazy val configuration = ConfigSource.default.at("votelog.curia-vista").loadOrThrow[Database.Configuration]
  implicit val log: Log4SLogger[IO] = new Log4SLogger[IO](org.log4s.getLogger)
  implicit val transactor: Resource[IO, Transactor[IO]] = buildTransactor[IO](configuration)
  val store = transactor.map(new DoobiePersonStore[IO](_))

  "DoobiePersonStore" should "retrieve index " in {

    val retrieveIndex = {
      (store: DoobiePersonStore[IO]) =>

        val qp: store.IndexParameters = IndexQueryParameters(
          Paging(
            Offset(0),
            PageSize(10),
          ),
          Context(LegislativePeriod.Id(51), Language.German),
          List[(Parliamentarian.Field, Sorting.Direction)](Parliamentarian.Field.LastName -> Ascending, Parliamentarian.Field.FirstName -> Ascending),
          Set(Parliamentarian.Field.Id, Parliamentarian.Field.FirstName, Parliamentarian.Field.LastName, Parliamentarian.Field.DateOfBirth),
        )

        for {
          Index(noEntities, partialsById) <- store.index(qp)
        } yield {
          val (ids, partials) = partialsById.unzip
          partialsById.size shouldBe qp.paging.pageSize.value

          partials.foreach { person =>
            person.lastName shouldNot be(None)
            person.firstName shouldNot be(None)
          }
        }
    }

    store.use(retrieveIndex).unsafeRunSync()
  }
}
