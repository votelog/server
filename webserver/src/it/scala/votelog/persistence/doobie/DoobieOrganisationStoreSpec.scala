package votelog.persistence.doobie

import cats.effect.IO
import cats.effect.unsafe.implicits.global
import pureconfig.generic.derivation.default.*
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import pureconfig.{ConfigReader, ConfigSource}
import votelog.app.Database
import votelog.app.implementation.Log4SLogger
import votelog.domain.crudi.ReadOnlyStore
import votelog.domain.crudi.ReadOnlyStore.{Index, IndexQueryParameters}
import votelog.domain.data.Paging
import votelog.domain.data.Paging.{Offset, PageSize}
import votelog.domain.data.Sorting.Direction.Descending
import votelog.domain.politics.Vote.{Meaning, Registration}
import votelog.domain.politics.{Bill, Business, Context, Decision, LegislativePeriod, Ngo, Vote}
import votelog.persistence.OrganisationStore.{DecisionPreference, Recipe}
import votelog.persistence.{OrganisationStore, StoreSpec, VoteStore}

import java.time.LocalDateTime

class DoobieOrganisationStoreSpec
  extends AnyFlatSpec
    with StoreSpec
    with ScalaFutures
    with Matchers {

  implicit val log: Log4SLogger[IO] = new Log4SLogger[IO](org.log4s.getLogger)
  lazy val configuration = ConfigSource.default.at("votelog.database").loadOrThrow[Database.Configuration]
  val transactor = Database.buildTransactor[IO](configuration)
  val vote = Vote(Registration(1), None, Business.Id(1), Some("Test"), Meaning(None, None), LocalDateTime.now())

  val votes = new VoteStore[IO] {
    override def index(
      queryParameters: ReadOnlyStore.PagedIndexQueryParameters[Context]
    ): IO[Index[Vote.Id, Vote.Partial]] = IO.pure(Index(0, Nil))

    override def read(queryParameters: ReadParameters)(id: Vote.Id): IO[Vote] =
      IO.pure(vote)

    override def fetchByIds(
      language: ReadParameters,
      ids: Set[Vote.Id]
    ): IO[List[(Vote.Id, Vote)]] = IO.pure {
      List(
        Vote.Id(1) -> vote
      )
    }
  }

  val store =
    transactor
      .map(new DoobieOrganisationStore[IO](_, votes))

  val ngoRecipe = OrganisationStore.Recipe("Earthicans", "earthicans", None, "dick@earthicans.com", "", LocalDateTime.of(2020, 1,1, 0, 0))
  val ngo = Ngo("Earthicans", "earthicans", None, "dick@earthicans.com", "", LocalDateTime.of(2020, 1,1, 0, 0))

  val creationRecipe: Recipe = ngoRecipe
  val createdEntity: Ngo.Id => Ngo = _ => ngo
  val updatedRecipe: Recipe = ngoRecipe.copy(mail = "gerry@earthicans.com")
  val updatedEntity: Ngo.Id => Ngo = _ => ngo.copy(mail = "gerry@earthicans.com")
  val indexQueryParameters: IndexQueryParameters[Unit, Ngo.Field, Ngo.Field] =
    IndexQueryParameters(
      Paging(
        offset = Offset(0),
        pageSize = PageSize(10),
      ),
      indexContext = (),
      orderings = List(Ngo.Field.Name -> Descending),
      fields = Ngo.Field.values.toSet
    )

  val partial: Ngo.Partial = Ngo.Partial(Some("Earthicans"), Some("earthicans"), None, Some("dick@earthicans.com"), Some(""), Some(LocalDateTime.of(2020, 1,1, 0, 0)))

  val ngoStore =
    store.use { store =>
      aStore(store, creationRecipe, createdEntity, updatedRecipe, updatedEntity, (_: Any) => partial)((), indexQueryParameters)
    }

  it should behave like ngoStore.unsafeRunSync()

  it should "update an existing record" in {
    val lp = LegislativePeriod.Id(0)

    val check = (store: OrganisationStore[IO]) =>
      for {
        nid <- store.create(OrganisationStore.Recipe("Earthicans", "earthicans", None, "dick@earthicans.com", "", LocalDateTime.of(2020, 1,1, 0, 0)))
        before <- store.preferences(lp, nid)
        registration = Vote.Id(1)
        _ <- store.updatePreference(nid, registration, Decision.Yes)
        afterScoring <- store.preferences(lp, nid)
        _ <- store.updatePreference(nid, registration, Decision.No)
        afterUpdate <- store.preferences(lp, nid)
      } yield {
        before shouldBe Nil
        afterScoring shouldBe List(registration -> DecisionPreference(vote.business, vote.bill, Decision.Yes))
        afterUpdate shouldBe List(registration -> DecisionPreference(vote.business, vote.bill, Decision.No))
      }

    store.use(check).unsafeRunSync()
  }


  it should "remove an existing record" in {

    val lp = LegislativePeriod.Id(0)

    val check = (store: OrganisationStore[IO]) =>
      for {
        nid <- store.create(ngoRecipe)
        before <- store.preferences(lp, nid)
        mid = Vote.Id(1)
        _ <- store.updatePreference(nid, mid, Decision.Yes)
        afterScoring <- store.preferences(lp, nid)
        _ <- store.deletePreference(nid, mid)
        afterUpdate <- store.preferences(lp, nid)
      } yield {
        before shouldBe Nil
        afterScoring shouldBe List(mid -> DecisionPreference(vote.business, vote.bill, Decision.Yes))
        afterUpdate shouldBe Nil
      }

    store.use(check).unsafeRunSync()
  }
}
