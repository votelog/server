package votelog.persistence.doobie

import java.util.UUID
import cats.effect.{Async, Resource}
import doobie.util.transactor.Transactor
import votelog.app.Database
import votelog.app.Database.Configuration.MigrationMode
import votelog.infrastructure.logging.Logger

object TransactorBuilder {

  def buildDatabaseConfig(name: String, migrationMode: MigrationMode) =
    Database.Configuration(
      driver = "org.h2.Driver",
      url = s"jdbc:h2:mem:${name};MODE=PostgreSQL;DB_CLOSE_DELAY=1",
      user = "sa",
      password = "",
      migrationMode = migrationMode
    )

  def buildTransactor[F[_]: Async: Logger](
    name: String,
    migrationMode: MigrationMode = MigrationMode.SkipMigrations
  ): Resource[F, Transactor[F]] = {
    Database.buildTransactor(buildDatabaseConfig(name, migrationMode))
  }

  def buildTransactor[F[_]: Async: Logger]: Resource[F, Transactor[F]] = {
    Database.buildTransactor(buildDatabaseConfig(UUID.randomUUID.toString, MigrationMode.RunMigrations))
  }
}
