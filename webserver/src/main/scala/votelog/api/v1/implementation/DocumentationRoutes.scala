package votelog.api.v1.implementation

import cats.Applicative
import cats.implicits.{catsSyntaxApplicativeId, toFunctorOps}
import io.circe.Printer
import io.circe.syntax.*
import sttp.apispec.openapi.OpenAPI
import sttp.apispec.openapi.circe.*
import sttp.apispec.openapi.circe.yaml.*
import sttp.tapir.docs.openapi.OpenAPIDocsInterpreter
import sttp.tapir.redoc.{Redoc, RedocUIOptions}
import sttp.tapir.server.ServerEndpoint
import votelog.api.v1.DocumentationEndpoint

class DocumentationRoutes[F[_]: Applicative](
  endpoints: Iterable[ServerEndpoint[?, F]]
) {

  val documentation: OpenAPI =
    OpenAPIDocsInterpreter()
      .serverEndpointsToOpenAPI(
        ses = endpoints,
        title = "VoteLog Server API",
        version = "v1"
      )

  val redocServers: List[ServerEndpoint[Any, F]] =
    Redoc(
      "VoteLog Server API",
      documentation.toYaml,
      RedocUIOptions.default.copy(
        pathPrefix = List("api", "v1", "doc"),
      )
    )

  val renderJson: Unit => F[Either[Unit, String]] = {
    _ =>
      Right(Printer.spaces2.print(documentation.asJson)).pure[F].widen
  }

  val renderYaml: Unit => F[Either[Unit, String]] = {
    _ =>
      Right(documentation.toYaml).pure[F].widen
  }

  val servers: List[ServerEndpoint[Any, F]] =
    List(
      DocumentationEndpoint.jsonDocumentation.serverLogic(renderJson),
      DocumentationEndpoint.yamlDocumentation.serverLogic(renderYaml),
    ) ++ redocServers
}
