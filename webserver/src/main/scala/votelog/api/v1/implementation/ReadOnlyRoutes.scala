package votelog.api.v1.implementation

import cats.effect.Async
import sttp.tapir.server.ServerEndpoint
import sttp.tapir.server.ServerEndpoint.Full
import votelog.app.implementation.AuthorizationHandler
import votelog.api.v1.ReadOnlyEndpoint
import votelog.domain.authentication.{Token, User}
import votelog.domain.authorization.{Capability, Component}
import votelog.domain.crudi.ReadOnlyStore

class ReadOnlyRoutes[F[_]: Async, Entity, Identity, Partial, ReadParameter, IndexParameter](
  component: Component,
  store: ReadOnlyStore[F, Entity, Identity, Partial, ReadParameter, IndexParameter],
  endpoint: ReadOnlyEndpoint[Entity, Identity, Partial, ReadParameter, IndexParameter],
  authorization: AuthorizationHandler[F],
) {

  import authorization.*

  def componentPath(in: (ReadParameter, Identity)): Component = {
    val (_, id) = in
    component.child(endpoint.identityTextCodec.encode(id))
  }

  val readServer: Full[Option[Token], User, (ReadParameter, Identity), String, Entity, Any, F] =
    endpoint.read
      .serverSecurityLogic(byUserWithFallback)
      .serverLogic(
        authorizedComponent(
          Capability.Read,
          componentPath,
          righted { case (a, b) => store.read(a)(b)} ),
      )

  val indexServer: Full[Option[Token], User, IndexParameter, String, ReadOnlyStore.Index[Identity, Partial], Any, F] =
    endpoint.index
      .serverSecurityLogic(byUserWithFallback)
      .serverLogic(authorized(Capability.Read, component)(righted(store.index)))

  val servers: List[ServerEndpoint[Any, F]] =
    List(indexServer, readServer)
}
