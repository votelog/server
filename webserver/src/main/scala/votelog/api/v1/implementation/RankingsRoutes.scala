package votelog.api.v1.implementation

import cats.effect.kernel.Sync
import sttp.tapir.server.ServerEndpoint
import votelog.app.implementation.AuthorizationHandler
import votelog.api.v1.RankingsEndpoint
import votelog.domain.authorization.{Capability, Component}
import votelog.domain.politics.Rankings.{OrganisationRankings, ParliamentarianRankings}
import votelog.domain.politics.{LegislativePeriod, Ngo, Parliamentarian, Rankings}

class RankingsRoutes[F[_]: Sync](
  component: Component,
  rankings: Rankings[F],
  authorization: AuthorizationHandler[F],
) {

  import authorization.*

  def componentNgo(in: (Ngo.Id, LegislativePeriod.Id)): Component = {
    val (id, _) = in
    component.child(id.value.toString)
  }

  def componentParliamentarian(in: (Parliamentarian.Id, LegislativePeriod.Id)): Component = {
    val (id, _) = in
    component.child(id.value.toString)
  }

  def personRankingByNgo: ((Ngo.Id, LegislativePeriod.Id)) => F[Either[String, OrganisationRankings]] =
    righted(rankings.personRankingByNgo.tupled)

  def personRankings: ((Parliamentarian.Id, LegislativePeriod.Id)) => F[Either[String, ParliamentarianRankings]] =
    righted(rankings.personRankings.tupled)

  val rankingsByNgoServer: ServerEndpoint[Any, F] =
    RankingsEndpoint
      .rankings
      .serverSecurityLogic(byUserWithFallback)
      .serverLogic(
        authorizedComponent(
          Capability.Read,
          componentNgo,
          personRankingByNgo,
        ))

  val rankingsForParliamentarianServer: ServerEndpoint[Any, F] =
    RankingsEndpoint
      .rankingsForParliamentarian
      .serverSecurityLogic(byUserWithFallback)
      .serverLogic(
        authorizedComponent(
          Capability.Read,
          componentParliamentarian,
          personRankings,
        ))

  val servers: List[ServerEndpoint[Any, F]] =
    List(
      rankingsByNgoServer,
      rankingsForParliamentarianServer,
    )
}
