package votelog.api.v1.implementation

import cats.MonadThrow
import sttp.tapir.server.ServerEndpoint
import votelog.app.implementation.AuthorizationHandler
import votelog.api.v1.VotesEndpoint
import votelog.domain.authentication.{Token, User}
import votelog.domain.authorization.{Capability, Component}
import votelog.domain.politics.{Language, Vote}
import votelog.persistence.VoteStore

class VotesRoutes[F[_]: MonadThrow](
  component: Component,
  store: VoteStore[F],
  authorizationHandler: AuthorizationHandler[F]
) {

  import authorizationHandler.{authorized, byUserWithFallback}

  val fetchByIds: ServerEndpoint.Full[Option[Token],User,(Language, Set[Vote.Id]),String,List[(Vote.Id, Vote)],Any,F] =
    VotesEndpoint
      .fetchByIds
      .serverSecurityLogic(byUserWithFallback)
      .serverLogic(authorized(Capability.Read, component.child("-").child("byId"))(righted(store.fetchByIds.tupled)))

  val servers: List[ServerEndpoint.Full[Option[Token],User,(Language, Set[Vote.Id]),String,List[(Vote.Id, Vote)],Any,F]] =
    List(fetchByIds)
}
