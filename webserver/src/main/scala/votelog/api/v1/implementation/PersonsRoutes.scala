package votelog.api.v1.implementation

import cats.effect.kernel.Sync
import sttp.tapir.server.ServerEndpoint
import votelog.app.implementation.AuthorizationHandler
import votelog.api.v1.ParliamentariansEndpoint
import votelog.domain.authentication.{Token, User}
import votelog.domain.authorization.{Capability, Component}
import votelog.domain.politics.*

class PersonsRoutes[F[_]: Sync](
  component: Component,
  decisionsStore: DecisionsStore[F],
  authorization: AuthorizationHandler[F],
) {

  import authorization.*

  def decisions(context: Context, person: Parliamentarian.Id): F[List[(Vote.Id, Decision)]] =
    decisionsStore.decisionsByPerson(context)(person)

  def decisionsForPerson(in: (Context, Parliamentarian.Id)): Component = {
    val (_, id) = in
    component.child(id.value.toString).child("decisions")
  }

  val readDecisions: ServerEndpoint.Full[Option[Token],User,(Context, Parliamentarian.Id),String,List[(Vote.Id, Decision)],Any,F] =
    ParliamentariansEndpoint
      .readDecisions
      .serverSecurityLogic(byUserWithFallback)
      .serverLogic(
        authorizedComponent(
          Capability.Read,
          decisionsForPerson,
          righted(decisions.tupled)),
      )

  val servers: List[ServerEndpoint[Any, F]] =
    List(
      readDecisions
    )
}
