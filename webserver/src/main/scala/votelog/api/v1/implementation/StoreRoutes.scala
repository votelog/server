package votelog.api.v1.implementation

import cats.effect.kernel.Async
import sttp.tapir.server.ServerEndpoint
import votelog.app.implementation.AuthorizationHandler
import votelog.api.v1.StoreEndpoint
import votelog.domain.authorization.{Capability, Component}
import votelog.domain.crudi.Store

class StoreRoutes[F[_]: Async, Entity, Identity, Recipe, Partial, ReadParameter, IndexParameter](
  component: Component,
  store: Store[F, Entity, Identity, Recipe, Partial, ReadParameter, IndexParameter],
  endpoint: StoreEndpoint[Entity, Identity, Recipe, Partial, ReadParameter, IndexParameter],
  authorization: AuthorizationHandler[F]
) {
  import authorization.*

  def componentPath(a: Identity): Component =
    component.child(endpoint.identityTextCodec.encode(a))

  def componentForUpdate(in: (Identity, Recipe)): Component = {
    val (id, _) = in
    component.child(endpoint.identityTextCodec.encode(id))
  }

  val servers: List[ServerEndpoint[Any, F]] =
    List(
      endpoint.create
        .serverSecurityLogic(byUserWithFallback)
        .serverLogic(authorized(Capability.Create, component)(righted(store.create))),

      endpoint.update
        .serverSecurityLogic(byUserWithFallback)
        .serverLogic(
          authorizedComponent(
            Capability.Update,
            componentForUpdate,
            righted(store.update.tupled)
          )
        ),

      endpoint.delete
        .serverSecurityLogic(byUserWithFallback)
        .serverLogic(
          authorizedComponent(
            Capability.Delete,
            componentPath,
            righted(store.delete)
          )
        ),
    )
}
