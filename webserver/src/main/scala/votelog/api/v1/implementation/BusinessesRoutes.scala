package votelog.api.v1.implementation

import cats.effect.kernel.Async
import sttp.tapir.server.ServerEndpoint
import votelog.app.implementation.AuthorizationHandler
import votelog.api.v1.BusinessesEndpoint
import votelog.domain.authentication.{Token, User}
import votelog.domain.authorization.{Capability, Component}
import votelog.domain.politics.{Bill, Business, Context, Language}
import votelog.persistence.BusinessStore

class BusinessesRoutes[F[_]: Async](
  component: Component,
  store: BusinessStore[F],
  authorization: AuthorizationHandler[F],
) {

  import authorization.*

  def billsForBusinessComponent(in: (Context, Business.Id)): Component = {
    val (_, id) = in
    component.child(BusinessesEndpoint.identityTextCodec.encode(id)).child("bills")
  }

  val billsForBusiness: ServerEndpoint.Full[Option[Token],User,(Context, Business.Id),String,List[(Bill.Id, Bill.Partial)],Any,F] =
    BusinessesEndpoint
      .billsForBusiness
      .serverSecurityLogic(byUserWithFallback)
      .serverLogic(
        authorizedComponent(
          Capability.Read,
          billsForBusinessComponent,
          righted(store.billsForBusiness.tupled),
        ))


  val fetchByIds: ServerEndpoint.Full[Option[Token],User,(Language, Set[Business.Id]),String,List[(Business.Id, Business)],Any,F] =
    BusinessesEndpoint
      .fetchByIds
      .serverSecurityLogic(byUserWithFallback)
      .serverLogic(authorized(Capability.Read, component.child("-").child("byId"))(righted(store.fetchByIds.tupled)))

  val servers: List[ServerEndpoint[Any, F]] =
    List(
      fetchByIds,
      billsForBusiness,
    )
}
