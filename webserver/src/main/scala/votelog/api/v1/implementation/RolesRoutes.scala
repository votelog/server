package votelog.api.v1.implementation

import cats.effect.Async
import votelog.app.implementation.AuthorizationHandler
import votelog.api.v1.RolesEndpoint
import votelog.domain.authorization.Role.Permission
import votelog.domain.authorization.{Capability, Component, Role}
import votelog.domain.data.Paging
import votelog.persistence.RoleStore

class RolesRoutes[F[_]: Async](
  component: Component,
  store: RoleStore[F],
  authorization: AuthorizationHandler[F],
) {
  import authorization.*

  def indexComponent(paging: Paging): Component = component.child(paging)

  val indexServer =
    RolesEndpoint
      .index
      .serverSecurityLogic(byUserWithFallback)
      .serverLogic(
        authorizedComponent(
          Capability.Read,
          indexComponent,
          righted(store.index)
        )
      )

  def updateRoleAndHavePermission(i: (Role.Id, Permission)): Set[Permission] = {
    val (role, permission) = i

    Set(
      Permission(Capability.Update, component.child(role)),
      permission
    )
  }

  implicit def singletonSeq[A](a: A): Seq[A] = Seq(a)

  val revokePermissionServer =
    RolesEndpoint
      .revokePermission
      .serverSecurityLogic(byUserWithFallback)
      .serverLogic(
        withPermissions(
          updateRoleAndHavePermission,
          righted(store.revokePermission.tupled)
        )
      )

  val grantPermissionServer =
    RolesEndpoint
      .grantPermission
      .serverSecurityLogic(byUserWithFallback)
      .serverLogic(
        withPermissions(
          updateRoleAndHavePermission,
          righted(store.grantPermission.tupled)
        )
      )

  val servers =
    List(
      indexServer,
      revokePermissionServer,
      grantPermissionServer,
    )
}
