package votelog.api.v1

import cats.*
import cats.implicits.*
import sttp.model.StatusCode
import sttp.tapir.server.interceptor.exception.{ExceptionContext, ExceptionHandler}
import sttp.tapir.server.model.ValuedEndpointOutput
import sttp.tapir.{statusCode, stringBody}
import votelog.app.implementation.AuthorizationHandler.Error.Unauthorized
import votelog.domain.crudi.ReadOnlyStore.Error.UnknownId

package object implementation {

  def exceptionHandler[F[_]]: ExceptionHandler[F] = ExceptionHandler.pure[F] { (ctx: ExceptionContext) =>
    ctx.e match {
      case err: UnknownId => response(
        StatusCode.NotFound,
        err.getMessage)
      case err: Unauthorized => response(
        StatusCode.Unauthorized,
        err.getMessage)
      case err => response(
        StatusCode.InternalServerError,
        err.getMessage)
    }
  }

  private def response(sc: StatusCode, message: String): Option[ValuedEndpointOutput[(StatusCode, String)]] =
    Some(ValuedEndpointOutput(statusCode.and(stringBody), (sc, message)))

  def righted[F[_] : ApplicativeThrow, T, S](f: T => F[S]): T => F[Either[String, S]] =
    t => f(t).attempt.map(_.left.map(_.toString))
}
