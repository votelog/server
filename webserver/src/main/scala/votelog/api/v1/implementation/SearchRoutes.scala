package votelog.api.v1.implementation

import cats.effect.IO
import sttp.capabilities.fs2.Fs2Streams
import sttp.tapir.server.ServerEndpoint
import votelog.api.v1.SearchEndpoint
import votelog.app.implementation.AuthorizationHandler
import votelog.domain.authentication.{Token, User}
import votelog.domain.authorization.{Capability, Component}
import votelog.domain.crudi.ReadOnlyStore
import votelog.domain.data.Paging
import votelog.domain.data.Paging.PageSize
import votelog.domain.politics.{Context, Language}
import votelog.domain.search.{Catalog, Search}

class SearchRoutes(
  component: Component,
  index: Catalog[IO],
  authorization: AuthorizationHandler[IO]
) {

  import authorization.{authorizedComponent, byUserWithFallback}

  def searchComponent(in: (Option[Language], Paging, String)) = component
  def searchEntityComponent(entity: Search.Entity, context: Context) = component
  def indexComponent(in: (Context, Set[Search.Entity.Type])) = component

  val search: ServerEndpoint.Full[Option[Token],User,(Option[Language], Paging, String),String,ReadOnlyStore.Index[String,Search.CuriaVistaDocument],Any,IO] =
    SearchEndpoint
      .search
      .serverSecurityLogic(byUserWithFallback)
      .serverLogic(
        authorizedComponent(
          Capability.Read, searchComponent,
          righted(index.search.fulltext.tupled),
        )
      )


  val indexEntities: ServerEndpoint[Any, IO] =
    SearchEndpoint
      .indexEntities
      .serverSecurityLogic(byUserWithFallback)
      .serverLogic(
        authorizedComponent(
          Capability.Update,
          indexComponent,
          righted(index.indexer.indexCuriaVistaEntities(_, PageSize(100), _))
        )
      )

  val indexEntity =
    SearchEndpoint
      .indexBusinessEntity
      .serverSecurityLogic(byUserWithFallback)
      .serverLogic(
        authorizedComponent(
          Capability.Update,
          searchEntityComponent.tupled,
          righted(index.indexer.indexEntity.tupled)
        )
      )


  val servers: List[ServerEndpoint[Any, IO]] = List(search, indexEntity, indexEntities)
}
