package votelog.api.v1.implementation

import cats.effect.kernel.Sync
import cats.implicits.*
import sttp.tapir.server.ServerEndpoint
import votelog.app.implementation.AuthorizationHandler
import votelog.api.v1.OrganisationsEndpoint
import votelog.domain.authentication.{Token, User}
import votelog.domain.authorization.{Capability, Component}
import votelog.domain.crudi.ReadOnlyStore.IndexQueryParameters
import votelog.domain.data.Paging
import votelog.domain.organisation.OrganisationService
import votelog.domain.organisation.OrganisationService.Recipe
import votelog.domain.politics.Vote.Registration
import votelog.domain.politics.{Decision, LegislativePeriod, Ngo, Vote}
import votelog.persistence.OrganisationStore
import votelog.util.Slug

class OrganisationsRoutes[F[_]: Sync](
  component: Component,
  store: OrganisationStore[F],
  service: OrganisationService[F],
  authorizationHandler: AuthorizationHandler[F],
) {

  import authorizationHandler.*

  val preferenceComponent: Component = component.child("preferences")

  def preferenceReadComponent(in: (Ngo.Id, Vote.Id)): Component = {
    val (ngo, vote) = in
    preferenceComponent.child(ngo.value.toString).child(vote.value.toString)
  }

  def preferenceUpdateComponent(in: (Ngo.Id, Vote.Id, Decision)): Component = {
    val (ngo, vote, _) = in
    preferenceComponent.child(ngo.value.toString).child(vote.value.toString)
  }

  val bySlug: ServerEndpoint.Full[Option[Token],User,Slug,String,(Ngo.Id, Ngo),Any,F] =
    OrganisationsEndpoint
      .bySlug
      .serverSecurityLogic(byUserWithFallback)
      .serverLogic { user => slug =>
        store.bySlug(slug)
          .flatTap { case (id, _) => checkAuthorization(user, Capability.Read, component.child(id.value.toString))}
          .attempt
          .map(_.left.map(_.getMessage))
      }

  private val defaultIndexParameter: IndexQueryParameters[Unit, Ngo.Field, Ngo.Field] =
    IndexQueryParameters(Paging.Default, (), List.empty, Set.empty)

  val initialIndex =
    OrganisationsEndpoint
      .initialIndex
      .serverSecurityLogic(byUserWithFallback)
      .serverLogic(authorized(Capability.Read, component)(a => righted(store.index)(defaultIndexParameter)))

  val indexPreferences: ServerEndpoint.Full[Option[Token],User,(LegislativePeriod.Id, Ngo.Id),String,List[(Vote.Id, OrganisationStore.DecisionPreference)],Any,F] =
    OrganisationsEndpoint
      .indexPreferences
      .serverSecurityLogic(byUserWithFallback)
      .serverLogic(authorized(Capability.Read, preferenceComponent)(righted(store.preferences.tupled)))

  val readPreference: ServerEndpoint.Full[Option[Token],User,(Ngo.Id, Vote.Id),String,OrganisationStore.DecisionPreference,Any,F] =
    OrganisationsEndpoint
      .readPreference
      .serverSecurityLogic(byUserWithFallback)
      .serverLogic(
        authorizedComponent(
          Capability.Read,
          preferenceReadComponent,
          righted(store.readPreference.tupled)),
      )

  val updatePreference: ServerEndpoint.Full[Option[Token],User,(Ngo.Id, Vote.Id, Decision),String,Unit,Any,F] =
    OrganisationsEndpoint
      .updatePreference
      .serverSecurityLogic(byUserWithFallback)
      .serverLogic(
        authorizedComponent(
          Capability.Update,
          preferenceUpdateComponent,
          righted(store.updatePreference.tupled)),
      )

  val deletePreference: ServerEndpoint.Full[Option[Token],User,(Ngo.Id, Vote.Id),String,Unit,Any,F] =
    OrganisationsEndpoint
      .deletePreference
      .serverSecurityLogic(byUserWithFallback)
      .serverLogic(
        authorizedComponent(
          Capability.Delete,
          preferenceReadComponent,
          righted(store.deletePreference.tupled)),
      )

  val createOrganisation = {
    import cats.implicits.*

    OrganisationsEndpoint
      .createOrganisation
      .serverSecurityLogic(byUserWithFallback)
      .serverLogic {
        authorizedComponent(
          Capability.Create,
          { in => component },
          { in => service.createOrganisation(in).map(Right.apply) }
        )
      }
  }

  lazy val servers: List[ServerEndpoint[Any, F]] =
    List(
      bySlug,
      indexPreferences,
      readPreference,
      updatePreference,
      deletePreference,
      createOrganisation,
      initialIndex,
    )
}
