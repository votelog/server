package votelog.api.v1.implementation

import cats.effect.kernel.Async
import sttp.tapir.server.ServerEndpoint
import votelog.app.implementation.AuthorizationHandler
import votelog.api.v1.BillsEndpoint
import votelog.domain.authentication.{Token, User}
import votelog.domain.authorization.{Capability, Component}
import votelog.domain.politics.{Bill, Language, Vote}
import votelog.persistence.BillStore

class BillsRoutes[F[_]: Async](
  component: Component,
  store: BillStore[F],
  authorization: AuthorizationHandler[F],
) {

  import authorization.*

  val votesForBill: ServerEndpoint.Full[Option[Token],User,(Language, Bill.Id),String,Map[Vote.Id,Vote.Partial],Any,F] =
    BillsEndpoint
      .votesForBill
      .serverSecurityLogic(byUserWithFallback)
      .serverLogic(authorized(Capability.Read, component.child("votes"))(righted(store.votesForBill.tupled)))

  val fetchByIds: ServerEndpoint.Full[Option[Token], User,(Language, Set[Bill.Id]),String,List[(Bill.Id, Bill)],Any,F] =
    BillsEndpoint
      .fetchByIds
      .serverSecurityLogic(byUserWithFallback)
      .serverLogic(authorized(Capability.Read, component.child("-").child("byId"))(righted(store.fetchByIds.tupled)))

  val servers: List[ServerEndpoint[Any, F]] =
    List(
      fetchByIds,
      votesForBill,
    )
}

