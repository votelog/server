package votelog.api.v1.implementation

import cats.effect.Async
import votelog.app.implementation.AuthorizationHandler
import votelog.api.v1.UsersEndpoint
import votelog.domain.authentication.User
import votelog.domain.authorization.Role.Permission
import votelog.domain.authorization.{Capability, Component, ComponentBuilder, Role}
import votelog.persistence.UserStore

class UserRoutes[F[_]: Async](
  roleComponentBuilder: ComponentBuilder[Role.Id],
  userComponentBuilder: ComponentBuilder[User.Id],
  store: UserStore[F],
  authorizationHandler: AuthorizationHandler[F],
) {

  import authorizationHandler.*

  val ownRole: ((User.Id, Role.Id)) => Set[Role.Permission] = {
    case (user, role) =>
      Set(Permission(Capability.Update, roleComponentBuilder.buildComponent(role)))
  }

  val canRead: Either[String, Option[(User.Id, User)]] => Set[Permission] = {
    _.flatMap {
      case Some((id, user)) => Right(Set(Permission(Capability.Read, userComponentBuilder.buildComponent(id))))
      case None => Right(Set.empty)
    }.getOrElse(Set.empty)
  }

  val grantRoleServer =
    UsersEndpoint
      .grantRole
      .serverSecurityLogic(byUserWithFallback)
      .serverLogic(
        withPermissionsFromParameters(
          ownRole,
          righted(store.grantRole.tupled)
        )
      )

  val revokeRoleServer =
    UsersEndpoint
      .grantRole
      .serverSecurityLogic(byUserWithFallback)
      .serverLogic(
        withPermissionsFromParameters(
          ownRole,
          righted(store.revokeRole.tupled)
        )
      )

  val findByName =
    UsersEndpoint
      .findByName
      .serverSecurityLogic(byUserWithFallback)
      .serverLogic(
        withPermissionsFromResult(
          canRead,
          righted(store.findByName)
        )
      )

  val servers =
    List(
      grantRoleServer,
      revokeRoleServer,
    )
}
