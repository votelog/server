package votelog.api.v1.implementation

import cats.effect.kernel.{Async, Clock}
import cats.implicits.*
import org.reactormonk.CryptoBits
import sttp.model.headers.Cookie.SameSite
import sttp.model.headers.CookieValueWithMeta
import sttp.tapir.model.UsernamePassword
import sttp.tapir.server.ServerEndpoint
import votelog.app.implementation.AuthorizationHandler
import votelog.api.v1.SessionEndpoint
import votelog.app.Webserver.Configuration.Http
import votelog.crypto.PasswordHasher
import votelog.domain.authentication.{Token, User}
import votelog.domain.authorization.Authorization
import votelog.persistence.UserStore

class SessionRoutes[F[_]: Async](
  domain: String,
  users: UserStore[F],
  passwordHasher: PasswordHasher[F],
  cryptoBits: CryptoBits,
  clock: Clock[F],
  authorization: AuthorizationHandler[F]
)  {

  import authorization.*

  def createSession(user: User): F[Either[String, (User, Token)]] = {
    clock
      .realTime
      .map { time =>
        val token = cryptoBits.signToken(user.name, time.toMillis.toString)

        Right((user, Token(token)))
      }
  }

  def authenticateUser(usernamePassword: UsernamePassword): F[Either[String, User]] =
    usernamePassword match {
      case UsernamePassword(username, Some(password)) =>
        for {
          maybeUser <- users.findByName(username).map(_.toRight("user not found or password mismatch"))
          hashedPassword <- passwordHasher.hashPassword(password)
        } yield
          maybeUser
            .map(_._2)
            .filterOrElse(_.passwordHash == hashedPassword, "user not found or password mismatch")

      case UsernamePassword(_, None) =>
        Async[F].raiseError(new Error("password required"))
    }

  val servers: List[ServerEndpoint[Any, F]] =
    List(
      SessionEndpoint
        .createSession
        .serverLogic { usernamePassword =>
          val maybeUser: F[Either[String, User]] = authenticateUser(usernamePassword)

          maybeUser.flatMap {
            case Left(error) => Async[F].pure(Left(error))
            case Right(user) => createSession(user)
          }
        },

      SessionEndpoint
        .delete
        .serverSecurityLogic(byUser)
        .serverLogic(_ => _ => Right(()).pure[F].widen),

      SessionEndpoint
        .read
        .serverSecurityLogic(fetchSession)
        .serverLogic(user => _ => Right(user).pure[F].widen),
    )

  private def fetchSession(maybeToken: Option[Token]): F[Either[String, (User, Token)]] = {
    for {
      maybeUser <- byUser(maybeToken)
    } yield for {
      token <- maybeToken.toRight("TokenMissing")
      user <- maybeUser
    } yield (user, token)
  }
}
