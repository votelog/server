package votelog.orphans.doobie.model

import doobie.postgres.implicits.*
import doobie.{Get, Meta, Read}
import votelog.domain.authorization.{Capability, Component, Role}

import java.util.UUID

trait RoleInstances {

  implicit val roleIdMeta: Meta[Role.Id] = Meta[UUID].imap(Role.Id.apply)(_.value)

  implicit val metaCapability: Meta[Capability] =
    Meta[String]
      .imap {
        case "Read" => Capability.Read
        case "Create" => Capability.Create
        case "Update" => Capability.Update
        case "Delete" => Capability.Delete
      }{
        case Capability.Read => "Read"
        case Capability.Create => "Create"
        case Capability.Update => "Update"
        case Capability.Delete => "Delete"
      }
}
