package votelog.orphans.doobie.domain.politics

import doobie.util.meta.Meta
import votelog.domain.politics.Party

trait PartyInstances {
  implicit val partyIdMeta: Meta[Party.Id] = Meta[Int].imap(Party.Id.apply)(_.value)
}
