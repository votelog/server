package votelog.orphans.doobie.domain.politics

import doobie.{Meta, Put, Read}
import votelog.domain.politics.Bill
import doobie.postgres.implicits.*

import java.util.UUID

trait BillInstances {
  implicit val billTypeMeta: Meta[Bill.Type] =
    Meta[Int]
      .imap {
        case 6 => Bill.Type.FederalLaw
        case 7 => Bill.Type.Ordinance
        case 8 => Bill.Type.FederalDecision
        case 9 => Bill.Type.SimpleFederalDecision
      } {
        case Bill.Type.FederalLaw => 6
        case Bill.Type.Ordinance => 7
        case Bill.Type.FederalDecision => 8
        case Bill.Type.SimpleFederalDecision => 9
      }

  implicit val billIdPut: Put[Bill.Id] = Put[UUID].contramap(bill => bill.value)
  implicit val billIdRead: Read[Bill.Id] = Read[UUID].map(v => Bill.Id(v))
}
