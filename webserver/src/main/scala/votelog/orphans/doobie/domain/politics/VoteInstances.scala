package votelog.orphans.doobie.domain.politics

import doobie.{Meta, Put}
import votelog.domain.politics.Vote
import votelog.domain.politics.Vote.Registration

trait VoteInstances {
  implicit lazy val registrationMeta: Meta[Registration] =
    Meta[Int].imap(Registration.apply)(_.number)
}
