package votelog.orphans.doobie

import java.util.UUID
import doobie.util.{Get, Put, Read, Write}
import votelog.domain.authentication.User
import votelog.domain.politics.{Decision, Language, LegislativePeriod, Ngo}
import doobie.postgres.implicits.*
import doobie.util.meta.Meta
import votelog.domain.authorization.Component
import votelog.domain.politics.Decision.{Absent, Abstain, No, PresidentialAbstain, Yes}
import votelog.orphans.doobie.domain.politics.{BillInstances, PartyInstances, PersonInstances, VoteInstances}

object implicits
  extends PersonInstances
     with PartyInstances
     with BillInstances
     with VoteInstances
     with model.RoleInstances {

  implicit val languageMeta: Meta[Language] =
    Meta[String]
      .imap(s => Language.fromIso639_1Unsafe(s.toLowerCase))(_.iso639_1.toUpperCase)

  implicit val languagePut: Put[Language] = Put[String].contramap(_.iso639_1.toUpperCase)
  implicit val languageWrite: Write[Language] = Write[String].contramap(_.iso639_1.toUpperCase)

  implicit val legislativePeriod: Meta[LegislativePeriod.Id] =
    Meta[Int].imap(LegislativePeriod.Id.apply)(_.value)

  implicit val UserIdPut: Put[User.Id] = Put[UUID].contramap(_.value)
  implicit val UserIdRead: Read[User.Id] = Read[UUID].map(v => User.Id(v))

  implicit val ngoIdPut: Put[Ngo.Id] = Put[UUID].contramap(uid => uid.value)
  implicit val ngoIdRead: Read[Ngo.Id] = Read[UUID].map(Ngo.Id.apply)

  implicit val componentWrite: Write[Component] = Write[String].contramap(_.location)
  implicit val componentMeta: Meta[Component] = Meta[String].imap(Component.Impl.apply)(_.location)
}
