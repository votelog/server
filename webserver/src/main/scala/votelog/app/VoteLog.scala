package votelog.app

import cats.*
import cats.effect.*
import cats.effect.kernel.Resource
import cats.implicits.*
import doobie.util.transactor.Transactor
import org.flywaydb.core.Flyway
import org.flywaydb.core.api.output.MigrateResult
import votelog.app.Database.buildTransactor
import votelog.crypto.PasswordHasherJavaxCrypto.Salt
import votelog.crypto.{PasswordHasher, PasswordHasherJavaxCrypto}
import votelog.domain.authorization.Component.Root
import votelog.domain.authorization.{Authorization, Capability}
import votelog.domain.politics.{DecisionEvaluation, DecisionsStore, Rankings}
import votelog.app.implementation.{ConfigurableRankings, DefaultAuthorization, FallbackAuthorization, OrganisationServiceImpl, UserCapabilityAuthorization}
import votelog.domain.organisation.OrganisationService
import votelog.infrastructure.logging.Logger
import votelog.persistence.*
import votelog.persistence.doobie.*

import scala.jdk.CollectionConverters.*
import votelog.domain.authorization.Component

abstract class VoteLog[F[_]] {

  object component {
    val api: Component = Root.child("api")

    object v1 {
      val v1: Component = api.child("v1")
      val auth: Component = v1.child("auth")
      val bill: Component = v1.child("bill")
      val business: Component = v1.child("business")
      val faction: Component = v1.child("faction")
      val organisation: Component = v1.child("organisation")
      val party: Component = v1.child("party")
      val parliamentarian: Component = v1.child("parliamentarian")
      val user: Component = v1.child("user")
      val role: Component = v1.child("role")
      val vote: Component = v1.child("vote")
      val ranking: Component = v1.child("ranking")
      val search: Component = v1.child("search")
    }
  }

  val decisions: DecisionsStore[F]
  val politicians: PersonStore[F]
  val businesses: BusinessStore[F]
  val users: UserStore[F]
  val ngos: OrganisationStore[F]
  val ngoService: OrganisationService[F]
  val bills: BillStore[F]
  val faction: FactionStore[F]
  val parties: PartyStore[F]
  val votes: VoteStore[F]
  val rankings: Rankings[F]
  val roles: RoleStore[F]
  val authorization: Authorization[F]
  val passwordHasher: PasswordHasher[F]
}

object VoteLog {

  def apply[F[_]: Async: NonEmptyParallel: Logger](configuration: Configuration): Resource[F, VoteLog[F]] = {
    val hasher = new PasswordHasherJavaxCrypto[F](Salt(configuration.security.passwordSalt))

    val votelog =
      buildTransactor(configuration.database).use { db =>
        buildTransactor(configuration.curiaVista).use { cv =>
          Async[F].delay(buildVotelog(hasher, db, cv))
        }
      }

    Resource.eval(votelog)
  }


  def buildVotelog[F[_]: Applicative: NonEmptyParallel: Async: Logger](
    hasher: PasswordHasher[F],
    votelogDatabase: Transactor[F],
    curiaVistaDatabase: Transactor[F],
  ): VoteLog[F] =
    new VoteLog[F] {

      val v1defaultPermissions =
        List(
          (component.v1.bill, Capability.Read),
          (component.v1.business, Capability.Read),
          (component.v1.faction, Capability.Read),
          (component.v1.organisation, Capability.Read),
          (component.v1.party, Capability.Read),
          (component.v1.parliamentarian, Capability.Read),
          (component.v1.vote, Capability.Read),
          (component.v1.ranking, Capability.Read),
          (component.v1.search, Capability.Read),
        )

      val politicians = new DoobiePersonStore(curiaVistaDatabase)
      val votes = new DoobieVoteStore(curiaVistaDatabase)
      val decisions = new DoobieDecisionsStore(curiaVistaDatabase)
      val businesses = new DoobieBusinessStore(curiaVistaDatabase)
      val bills = new DoobieBillStore(curiaVistaDatabase)
      val faction = new DoobieFactionStore(curiaVistaDatabase)
      val parties = new DoobiePartyStore(curiaVistaDatabase)

      val roles: DoobieRoleStore[F] = new DoobieRoleStore(votelogDatabase)
      val users = new DoobieUserStore(votelogDatabase, roles, hasher)
      val ngos = new DoobieOrganisationStore(votelogDatabase, votes)

      val clock = java.time.Clock.systemDefaultZone()
      val ngoService: OrganisationService[F] =
        new OrganisationServiceImpl[F](component.v1.organisation, ngos, users, roles, clock)

      val rankings = new ConfigurableRankings(DecisionEvaluation.NaiveEvaluation, ngos, decisions)

      val passwordHasher = hasher
      val defaultAuthorization = new DefaultAuthorization[F](v1defaultPermissions)
      val userAuthorization = new UserCapabilityAuthorization[F]
      val authorization = new FallbackAuthorization(defaultAuthorization, userAuthorization)
    }
}
