package votelog.app

import cats.effect.{ExitCode, IO, IOApp}
import fs2.io.file.Files
import fs2.{Pipe, text}
import pureconfig.{ConfigReader, ConfigSource}
import votelog.domain.politics.{Decision, Ngo, Vote}
import votelog.domain.politics.Vote.Registration
import cats.*
import cats.implicits.*
import votelog.app.implementation.Log4SLogger
import votelog.infrastructure.logging.Logger
import pureconfig.generic.derivation.default.*

import java.nio.file.Path
import java.util.UUID
import scala.util.Try

object InsertPreferences extends IOApp {

  implicit val log: Log4SLogger[IO] = new Log4SLogger[IO](org.log4s.getLogger)

  case class PreferenceLine(id: Ngo.Id, registration: Registration, preference: Decision)

  implicit val fg: ConfigReader[Console.Configuration] =
    ConfigReader.derived[Console.Configuration]

  override def run(args: List[String]): IO[ExitCode] =
    for {
      configuration <- IO(ConfigSource.default.at("console").loadOrThrow[Console.Configuration])
      path =
        args.headOption.map(location => fs2.io.file.Path.fromNioPath(Path.of(location)))
          .getOrElse(sys.error("expecting path"))
      result <- VoteLog[IO](configuration.votelog).use(voteLog => runInserts(path, voteLog))
    } yield result


  def csvParser[F[_]]: Pipe[F, Byte, List[String]] =
    _.through(text.utf8.decode)
      .through(text.lines)
      .drop(1) // remove headers
      .map(_.split(',').toList) // separate by comma

  val parseLine: List[String] => Either[Throwable, Option[PreferenceLine]] = {
    case line @ org :: id :: voting_id :: voting_datetime :: business_nr :: business_nr_short :: registrationNr :: preferenceRaw :: Nil =>
      val ngoId: Either[Throwable, Ngo.Id] = Right(Ngo.Id(UUID.fromString(id)))
      val registration = Try(registrationNr.toInt).map(Registration.apply).toEither
      val preference: Either[Throwable, Decision] =
        Right(preferenceRaw).flatMap {
          case "Ja" => Right(Decision.Yes)
          case "Nein" => Right(Decision.No)
          case other => Left(new Error(s"unknown decision $other"))
        }

      (ngoId, registration, preference).mapN {
        case (ngo, vote, preference) => Some(PreferenceLine(ngo, vote, preference))
      }
        .left.map {
          error => new Error(s"could not read line: $line:  ${error.getMessage}")
      }
    case _ => Right(None)
  }

  def insertPreference(votelog: VoteLog[IO], line: Either[Throwable, Option[PreferenceLine]]): IO[Unit] = line match {
    case Right(Some(line)) =>
      votelog.ngos.updatePreference(line.id, ???, line.preference)

    case Right(None) => IO.unit
    case Left(error) => Logger[IO].warn(s"Could not read line: $error")
  }

  def runInserts(path: fs2.io.file.Path, voteLog: VoteLog[IO]): IO[ExitCode] = {
    Files[IO]
      .readAll(path)
      .through(csvParser)
      .map(parseLine) // parse each line into a valid sample
      .evalMap(insertPreference(voteLog, _))
      .compile
      .drain
      .as(ExitCode.Success)
  }
}
