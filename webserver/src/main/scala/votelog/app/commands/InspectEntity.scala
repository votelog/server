package votelog.app.commands

import votelog.domain.politics.Context
import votelog.domain.search.{Catalog, Search}
import votelog.domain.data.Paging
import votelog.domain.data.Paging.Offset
import votelog.domain.data.Paging.PageSize
import votelog.domain.politics.Language
import votelog.app.VoteLog
import votelog.domain.search.Search.Entity
import votelog.domain.politics
import cats.effect.kernel.Async
import cats.implicits.*
import votelog.domain.authorization.Component
import votelog.infrastructure.logging.Logger

class InspectEntity[F[_]: Async: Logger](catalog: Catalog[F], votelog: VoteLog[F]) {

  private def buildComponent(entity: Search.Entity) = entity match {
    case Entity.Business(politics.Business.Id(id)) => votelog.component.v1.business.child(id.toString)
    case Entity.Bill(politics.Bill.Id(id)) => votelog.component.v1.bill.child(id.toString)
    case Entity.Vote(politics.Vote.Id(id)) => votelog.component.v1.vote.child(id.toString)
    case Entity.Parliamentarian(politics.Parliamentarian.Id(id)) => votelog.component.v1.parliamentarian.child(id.toString)
  }

  def run(component: Component, language: Language): F[Unit] = {
    for {
      results <- catalog.search.retrieve(component, language)
      _ <- Logger[F].info(s"Results for '$results':")
      _ <- Logger[F].info(results.toString)
    } yield ()
  }

  def run(entity: Search.Entity, language: Language): F[Unit] =
    run(buildComponent(entity), language)
}
