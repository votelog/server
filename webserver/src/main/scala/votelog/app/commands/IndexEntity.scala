package votelog.app.commands

import cats.*
import cats.implicits.*
import votelog.app.VoteLog
import votelog.domain.politics.{Business, Context}
import votelog.domain.search.{Catalog, Search}

class IndexEntity[F[_]: Applicative](catalog: Catalog[F]) {

  def run(entity: Search.Entity, context: Context): F[Unit] = {
    catalog.indexer.indexEntity(entity, context)
      .void
  }

}
