package votelog.app.commands

import cats.Applicative
import cats.effect.kernel.Outcome.{Canceled, Succeeded, Errored}
import cats.implicits.*
import cats.effect.kernel.Sync
import scopt.{OParser, OParserBuilder, Read}
import votelog.app.Console.{Action, ConsoleAction}
import votelog.domain.data.Paging.PageSize
import votelog.domain.politics.{Context, Language, LegislativePeriod}
import votelog.domain.search.{Catalog, Search, VoteLogIndexer}
import votelog.infrastructure.logging.Logger
import votelog.lang.nonEmptyOr

class IndexEntities[F[_]: Sync: Logger](catalog: Catalog[F]) {

  def run(entityTypes: Set[Search.Entity.Type], context: Context): F[Unit] = {
    val selectedTypes = entityTypes.nonEmptyOr(Search.Entity.Type.values.toSet)
    catalog.indexer
      .indexCuriaVistaEntitiesProcess(context, PageSize(100), selectedTypes)
      .flatMap(_.join)
      .flatTap {
        case Canceled() => Logger[F].warn("Indexing canceled")
        case Errored(error) => Logger[F].error(error)(s"Failure while indexing: ${error.getMessage}")
        case Succeeded(result) => Logger[F].info("Indexing completed.")
      }
      .void
  }

}

object IndexEntities {

  given Read[Search.Entity.Type] =
    Read.reads(Search.Entity.Type.valueOf)

  given Read[Language] =
    Read.reads(Language.fromIso639_1Unsafe)

  given Read[LegislativePeriod.Id] =
    Read.intRead.map(LegislativePeriod.Id.apply)

  def command(builder: OParserBuilder[ConsoleAction]): OParser[Unit, ConsoleAction] = {
    import builder.*

    cmd("index-entities")
      .text("Index CuriaVista entities by entity types")
      .action((_, c) => c.copy(action = Some(Action.IndexEntities())))
      .children(
        opt[LegislativePeriod.Id]("lp")
          .valueName("51")
          .text("legislative period")
          .action { (lp, c) =>
            c.action match {
              case Some(a@Action.IndexEntities(context, entityTypes)) =>
                ConsoleAction(Some(Action.IndexEntities(context.copy(legislativePeriod = lp), entityTypes)))
              case a => c
            }
          },
        opt[Language]("lang")
          .action { (lang, c) =>
            c.action match {
              case Some(a@Action.IndexEntities(context, entityTypes)) =>
                ConsoleAction(Some(Action.IndexEntities( context.copy(language = lang), entityTypes)))
              case a => c
            }
          },
        opt[Seq[Search.Entity.Type]]('t',"types")
          .valueName("bill,business,parliamentarian,vote")
          .optional()
          .text("The entities which should be indexed")
          .action { (ts, c) =>
            c.action match {
              case Some(a@Action.IndexEntities(context, entityTypes)) =>
                ConsoleAction(Some(Action.IndexEntities(context, ts.toSet)))
              case a => c
            }
          }
      )
  }
}