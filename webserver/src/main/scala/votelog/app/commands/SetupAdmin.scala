package votelog.app.commands

import cats.Monad
import votelog.app.VoteLog
import votelog.domain.authentication.User
import votelog.persistence.UserStore
import votelog.persistence.UserStore.Password
import cats.implicits.*
import scopt.{OParser, OParserBuilder}
import votelog.app.Console.{Action, ConsoleAction}
import votelog.domain.authorization.Role.Permission
import votelog.domain.authorization.{Capability, Component, Role}
import votelog.infrastructure.logging.Logger

class SetupAdmin[F[_]: Monad: Logger](votelog: VoteLog[F]) {
  def run(password: Password.Clear): F[Unit] = {
    val userRecipe = UserStore.Recipe("admin", User.Email("admin@votelog.ch"), password)
    val roleTemplate = Role.Template("admin", Component.Root)

    for {
      admin <- votelog.users.create(userRecipe)
      role <- votelog.roles.create(roleTemplate)
      _ <- votelog.users.grantRole(admin, role)
      _ <- votelog.roles.grantPermission(role, Permission(Capability.Read, Component.Root))
      _ <- votelog.roles.grantPermission(role, Permission(Capability.Create, Component.Root))
      _ <- votelog.roles.grantPermission(role, Permission(Capability.Update, Component.Root))
      _ <- votelog.roles.grantPermission(role, Permission(Capability.Delete, Component.Root))
      _ <- Logger[F].info("admin created with password: <hideden>")
    } yield ()

  }
}

object SetupAdmin {
  def command(builder: OParserBuilder[ConsoleAction]): OParser[Unit, ConsoleAction] = {
    import builder.*

    cmd("setup-admin")
      .children(
        arg[String]("password")
          .action((password, action) => action.copy(action = Some(Action.SetupAdmin(Password.Clear(password)))))
          .text("create new administrative user")
      )
  }
}
