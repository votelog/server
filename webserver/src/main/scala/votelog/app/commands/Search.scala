package votelog.app.commands

import cats.Monad
import cats.effect.*
import cats.implicits.*
import cats.effect.implicits.*
import scopt.{OParser, OParserBuilder}
import votelog.app.Console.{Action, ConsoleAction}
import votelog.domain.data.Paging
import votelog.domain.data.Paging.{Offset, PageSize}
import votelog.domain.politics.Context
import votelog.domain.search.{Catalog, Search}
import votelog.infrastructure.logging.Logger

class Search[F[_]: Monad: Logger](catalog: Catalog[F]) {

  def run(query: String): F[Unit] = {

    for {
      results <- catalog.search.fulltext(None, Paging(Offset(0), PageSize(10)), query)
      _ <- Logger[F].info(s"Results for '$query':")
      _ <- Logger[F].info(results.toString)
    } yield ()

  }
}

object Search {
  def command(builder: OParserBuilder[ConsoleAction]): OParser[Unit, ConsoleAction] = {
    import builder.*

    cmd("search")
      .children(
        arg[String]("<query>")
          .action((query, action) => action.copy(action = Some(Action.Search(query))))
          .text("Search for CuriavVista entities within the index")
      )
  }
}