package votelog.app

import cats.Applicative
import cats.effect.{Async, Resource, Sync}
import doobie.util.transactor.Transactor
import org.flywaydb.core.Flyway
import org.flywaydb.core.api.output.MigrateResult
import votelog.infrastructure.logging.Logger
import cats.implicits.*
import pureconfig.ConfigReader
import pureconfig.generic.derivation.EnumConfigReaderDerivation.Default.EnumConfigReader
import pureconfig.generic.derivation.default.*
import votelog.app.Database.Configuration.MigrationMode
import votelog.app.Database.Configuration.MigrationMode.{RunMigrations, SkipMigrations}

import scala.jdk.CollectionConverters.*

object Database {

  case class Configuration(
    driver: String,
    url: String,
    user: String,
    password: String,
    migrationMode: MigrationMode,
  ) derives ConfigReader

  object Configuration {
    sealed trait MigrationMode extends Product with Serializable
    object MigrationMode {
      case object RunMigrations extends MigrationMode
      case object SkipMigrations extends MigrationMode

      implicit val modeReader: ConfigReader[Configuration.MigrationMode] =
        EnumConfigReader.derived[Database.Configuration.MigrationMode]
    }
  }

  def buildTransactor[F[_]: Async: Logger](
    config: Configuration
  ): Resource[F, Transactor[F]] =
    Resource.pure(
      Transactor.fromDriverManager[F](
        driver = config.driver,
        url = config.url,
        user = config.user,
        pass = config.password,
      )
    ).preAllocate {
      if (config.migrationMode == RunMigrations) runMigrations(config)
      else Applicative[F].unit
    }


  def runMigrations[F[_]: Logger: Sync](db: Database.Configuration): F[Unit] =
    Sync[F].delay {
      Flyway.configure()
        .dataSource(db.url, db.user, db.password)
        .locations("db/migration")
        .load()
        .migrate()
    }
      .flatMap { result =>
        if (result.success) Logger[F].info("")
        else Logger[F].info(s"migration failed: ${result}") *>
          Logger[F].debug(renderMigrationResult(result))
      }

  def renderMigrationResult(result: MigrateResult): String = {
    s"""Migration: ${if (result.success) "success" else "failure" }
       |  executed: ${result.migrations.size}
       |    ${result.migrations.asScala.map(o => s"${o.version} \t ${o.description} - ${o.`type`} ")}
       |""".stripMargin
  }

}
