package votelog
package app

import cats.effect.implicits.*
import cats.effect.kernel.{Ref, Resource}
import cats.effect.std.Supervisor
import cats.effect.{ExitCode, IO, IOApp}
import cats.implicits.*
import pureconfig.generic.derivation.default.*
import pureconfig.generic.derivation.default.*
import pureconfig.{ConfigReader, ConfigSource}
import pureconfig.generic.derivation.default.*
import scopt.{OParserBuilder, Read}
import votelog.app.Console.Action.IndexEntities
import votelog.app.LuceneVoteLogIndex.IndexLog
import votelog.app.commands
import votelog.app.implementation.Log4SLogger
import votelog.domain.authentication.User
import votelog.domain.authorization.Role.Permission
import votelog.domain.authorization.{Capability, Component, Role}
import votelog.domain.crudi.ReadOnlyStore
import votelog.domain.data.Paging
import votelog.domain.crudi.ReadOnlyStore.IndexQueryParameters
import votelog.domain.data.Paging.{Offset, PageSize}
import votelog.domain.organisation.OrganisationService
import votelog.domain.politics.*
import votelog.domain.search.{Catalog, Search}
import votelog.domain.search.Search.CuriaVistaDocument
import votelog.infrastructure.logging.Logger
import votelog.persistence
import votelog.persistence.UserStore.Password
import votelog.persistence.{OrganisationStore, RoleStore, UserStore}

import java.time.LocalDateTime

object Console extends IOApp {
  val defaultLegislativePeriodId = 51
  val defaultContext: Context = Context(LegislativePeriod.Id(defaultLegislativePeriodId), Language.German)
  val defaultPasswordLength = 8
  val defaultPageSize = 10

  implicit val log: Log4SLogger[IO] = new Log4SLogger[IO](org.log4s.getLogger)

  case class Configuration(votelog: app.Configuration) derives ConfigReader

  val loadConfiguration: IO[Configuration] =  IO(ConfigSource.default.at("console").loadOrThrow[Configuration])

  case class ConsoleAction(action: Option[Action])
  object ConsoleAction {
    def run(
      action: Option[ConsoleAction],
      configuration: Configuration,
      voteLog: VoteLog[IO],
      supervisor: Supervisor[IO],
      logger: Logger[IO],
    ): IO[Unit] = {
      action.flatMap(_.action) match {
        case Some(Action.IndexEntities(context, entityTypes)) =>
          for {
            indexLog <- IndexLog.init[IO]
            index = SearchIndex.build(configuration.votelog.search, voteLog, supervisor, indexLog)
            run <- new commands.IndexEntities[IO](index).run(entityTypes, context)
          } yield run
        case Some(Action.Search(query)) =>
          for {
            indexLog <- IndexLog.init[IO]
            index = SearchIndex.build(configuration.votelog.search, voteLog, supervisor,indexLog)
            run <- new commands.Search(index).run(query)
          } yield run

        case Some(Action.SetupAdmin(password)) =>
          for {
            _ <- new commands.SetupAdmin(voteLog).run(password)
          } yield ()

        case _ => IO(logger.warn("Not implemented"))
      }
    }
  }

  enum Action {
    val defaultContext = Context(LegislativePeriod.Id(51), Language.German)

    case CreateOrganisation(organisation: OrganisationStore.Recipe, user: UserStore.Recipe)

    case IndexEntities(
      context: Context = defaultContext,
      entityTypes: Set[votelog.domain.search.Search.Entity.Type] = Set.empty,
    )

    case InspectEntity(context: Option[Context])
    case Search(query: String)
    case SetupAdmin(password: Password.Clear)
  }

  def run(args: List[String]): IO[ExitCode] = {

    import scopt.OParser
    val builder: OParserBuilder[ConsoleAction] = OParser.builder[ConsoleAction]
    import builder.*

    val parser = {
      OParser.sequence(
        programName("votelog-console"),
        commands.IndexEntities.command(builder),
        commands.Search.command(builder),
        commands.SetupAdmin.command(builder),
      )
    }

    val parseArguments = IO(OParser.parse(parser, args, ConsoleAction(None)))
    
    def buildSystem(configuration: Configuration): Resource[IO, (VoteLog[IO], Supervisor[IO])] =
      VoteLog[IO](configuration.votelog)
        .flatMap { voteLog =>
          Supervisor[IO](await = true).map { supervisor =>
            (voteLog, supervisor)
          }
      }

    for {
      configuration <- loadConfiguration
      _ <- log.info(s"configuration: $configuration")
      _ <- Logger[IO].info("Starting console")
      result <- buildSystem(configuration).use { case (voteLog, supervisor) =>
        for {
          action <- parseArguments
          consequences <-
            ConsoleAction
              .run(action, configuration, voteLog, supervisor, summon[Logger[IO]])
              .onError(error => IO(println(error.getMessage)) >> Logger[IO].error(s"Failed $action: ${error.getMessage}"))
        } yield consequences
      }
    } yield ExitCode.Success
  }


  def renderResults(query: String, index: ReadOnlyStore.Index[String, CuriaVistaDocument]): String = {
    s"""|${index.totalEntities} for $query:
        |
        |${index.entities.map { case (id, entity) => s"$id: $entity"}}
        |""".stripMargin
  }


  def createUser(
    voteLog: Resource[IO, VoteLog[IO]],
    recipe: UserStore.Recipe,
  ) = {
    for {
      (id, user) <- voteLog.use { voteLog =>
        voteLog.users.create(recipe)
          .flatMap(id => voteLog.users.read(())(id).map(user => (id, user)))
      }
    } yield id
  }


  def createNgo(
    voteLog: Resource[IO, VoteLog[IO]],
    recipe: OrganisationStore.Recipe,
  ) = {
    for {
      (id, ngo) <- voteLog.use { voteLog =>
        voteLog.ngos.create(recipe.copy(createAt = LocalDateTime.now()))
          .flatMap{ id => voteLog.ngos.read(())(id).map(ngo => (id, ngo))}
      }
    } yield id
  }

  def grantNgoPermissions(votelog: Resource[IO, VoteLog[IO]], user: User.Id, ngo: Ngo.Id) =
    votelog.use { votelog =>
      val ngoComponent = votelog.component.v1.organisation.child(ngo.value.toString)

      for {
        ngoOwnerRole <- votelog.roles.create(Role.Template("Owner", ngoComponent))
        _ <- votelog.roles.grantPermission(ngoOwnerRole, Permission(Capability.Create, ngoComponent))
        _ <- votelog.roles.grantPermission(ngoOwnerRole, Permission(Capability.Read, ngoComponent))
        _ <- votelog.roles.grantPermission(ngoOwnerRole, Permission(Capability.Update, ngoComponent))
        _ <- votelog.roles.grantPermission(ngoOwnerRole, Permission(Capability.Delete, ngoComponent))
        _ <- votelog.users.grantRole(user, ngoOwnerRole)
      } yield ()
    }

  def setupNgo(
    voteLog: Resource[IO, VoteLog[IO]],
    organisation: OrganisationStore.Recipe,
    user: UserStore.Recipe,
  ) =
    for {
      ngoId <- createNgo(voteLog, organisation)
      userId <- createUser(voteLog, user)
      _ <- grantNgoPermissions(voteLog, userId, ngoId)
      _ <- Logger[IO].info(s"Created ngo ${organisation.name} with administrative user ${user.name} and password ${user.password.value}")
    } yield ()

  def getRanking(ngoId: Ngo.Id): IO[ExitCode] =
    for {
      configuration <- loadConfiguration
      _ <- log.info(s"configuration: $configuration")
      voteLog = VoteLog[IO](configuration.votelog)

      context = Context(LegislativePeriod.Id(51), Language.German)
      fields: Set[Parliamentarian.Field] = Set(Parliamentarian.Field.LastName, Parliamentarian.Field.FirstName)
      ranking <- voteLog.use { votelog =>
        votelog.rankings.personRankingByNgo(ngoId, context.legislativePeriod)
      }
      persons <- voteLog.use { votelog =>
        votelog.politicians.index(IndexQueryParameters(Paging(Offset(0), PageSize(300)), context, List.empty, fields))
      }
    } yield {

      val parliamentarianById: Map[Parliamentarian.Id, PersonPartial] = persons.entities.toMap
      val scoredParliamentarians = ranking.ranking.map {
        case (personId, score) => (score, personId, parliamentarianById.get(personId))
      }

      println(s"# Scoring of parliamentarians for organisation $ngoId")

      scoredParliamentarians
        .toList
        .sortBy(-_._1.score.value)
        .zipWithIndex
        .collect {
          case ((score, id, Some(person)), rank) =>
            s"#${rank+1} ($score): ${person.firstName} ${person.lastName} ($id)"
          case ((score, id, None), rank) =>
            s"#${rank+1} ($score): ($id)"
        }
        .foreach(println)

      ExitCode.Success
    }

}
