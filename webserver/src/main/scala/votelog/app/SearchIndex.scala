package votelog.app


import cats.effect.IO
import cats.effect.kernel.{Fiber, Ref, Sync}
import cats.effect.std.Supervisor
import cats.syntax.all.*
import cats.*
import com.outr.lucene4s.*
import com.outr.lucene4s.field.{Field, FieldType}
import com.outr.lucene4s.field.value.FieldAndValue
import com.outr.lucene4s.field.value.support.{StringifyValueSupport, ValueSupport}
import com.outr.lucene4s.query.{ExactIntSearchTerm, SearchTerm}
import com.outr.lucene4s.{DirectLucene, Lucene, Stringify}
import fs2.*
import org.apache.lucene.document.{Document, IntPoint, NumericDocValuesField, StoredField}
import org.apache.lucene.index.IndexableField
import org.apache.lucene.search.SortField
import org.apache.lucene.search.SortField.Type
import votelog.app.LuceneVoteLogIndex.{IndexLog, Indexing}
import votelog.app.implementation.search.EntityIndexer.IndexableDocument
import votelog.app.implementation.search.{BillAsCuriaVistaDocument, BusinessAsCuriaVistaDocument, EntityIndexer, ParliamentarianAsCuriaVistaDocument, VoteAsCuriaVistaDocument}
import votelog.app.implementation.search.lucene.{LuceneDocumentIndexer, LuceneSearch}
import votelog.domain.authorization.Component
import votelog.domain.crudi.ReadOnlyStore
import votelog.domain.crudi.ReadOnlyStore.{IndexQueryParameters, PagedIndexQueryParameters}
import votelog.domain.data.Paging
import votelog.domain.data.Paging.{Offset, PageSize}
import votelog.domain.data.Sorting.Direction
import votelog.domain.data.Sorting.Direction.{Ascending, Descending}
import votelog.domain.politics.*
import votelog.domain.search.Search.CuriaVistaDocument
import votelog.domain.search.{AsCuriaVistaDocument, Catalog, Search, VoteLogIndexer}
import votelog.infrastructure.logging.Logger

import java.nio.file.Path
import votelog.domain.search.Search.Entity

import java.time.{ZoneOffset, ZonedDateTime}
import scala.Option.when
import cats.effect.unsafe.implicits.global
import votelog.domain.search.VoteLogIndexer.{Event, Indexable}

import scala.util.{Failure, Success}

object LuceneVoteLogIndex {
  case class Indexing(totalEntities: Int, indexedEntities: Int)

  trait IndexLog[F[_]] {
    def processing: F[Map[Component, Indexing]]
    def history: F[List[Event.IndexCompleted]]
    def aggregateEvent(event: Event): F[Unit]
  }

  object IndexLog {
    class Impl[F[_]: FlatMap](
      processingRef: Ref[F, Map[Component, Indexing]],
      historyRef: Ref[F, List[Event.IndexCompleted]],
    ) extends IndexLog[F] {
      def processing: F[Map[Component, Indexing]] = processingRef.get
      def history: F[List[Event.IndexCompleted]] = historyRef.get
      def aggregateEvent(event: Event): F[Unit] =
        for {
          _ <- processingRef.getAndUpdate(updateProcesses(event, _))
          _ <- historyRef.getAndUpdate(updateHistory(event, _))
        } yield ()
    }

    def init[F[_]: Sync]: F[IndexLog[F]] = {
      val processing: F[Ref[F, Map[Component, Indexing]]] = Ref[F].of(Map.empty)
      val history: F[Ref[F, List[Event.IndexCompleted]]] = Ref[F].of(List.empty)

      for {
        processing <- processing
        history <- history
      } yield Impl(processing, history)

    }

    def updateProcesses(event: Event, processes: Map[Component, Indexing]) =
      event match {
        case Event.IndexRead(component, totalEntities) =>
          val indexing =
            processes
              .get(component)
              .map(_.copy(totalEntities = totalEntities))
              .getOrElse(Indexing(totalEntities, 0))
          processes.updated(component, indexing)

        case Event.DocumentFound(Indexable(component, document)) =>
          val indexing =
            processes
              .get(component.parent)
              .map(i => i.copy(indexedEntities = i.indexedEntities + 1))
              .getOrElse(Indexing(0, indexedEntities = 1))
          processes.updated(component.parent, indexing)

        case Event.IndexCompleted(component, _) =>
          processes.removed(component)
      }

    def updateHistory(event: Event, history: List[Event.IndexCompleted]) = event match {
      case event: Event.IndexCompleted => event :: history
      case _ => history
    }

  }
}

class LuceneVoteLogIndex(
  votelog: VoteLog[IO],
  documentIndexer: LuceneDocumentIndexer[IO],
  val search: Search[IO],
  supervisor: Supervisor[IO],
  indexLog: IndexLog[IO],
) (implicit log: Logger[IO]) extends Catalog[IO] {

  val indexer: VoteLogIndexer[IO] = new VoteLogIndexer[IO] {

    override def indexCuriaVistaEntities(
      context: Context,
      pageSize: PageSize,
      entityTypes: Set[Entity.Type],
    ): IO[String] = indexCuriaVistaEntitiesProcess(context, pageSize, entityTypes)
      .flatMap(_.join)
      .flatMap(_.embed(IO("failed")))


    def indexCuriaVistaEntitiesProcess(
      context: Context,
      pageSize: PageSize,
      entityTypes: Set[Entity.Type],
    ): IO[Fiber[IO, Throwable, String]] = {

      val indexingProcessEvents = buildIndexingEvents(context, pageSize, entityTypes)

      val indexing =
        indexingProcessEvents.map { events =>
          events
            .evalTap(applyInserts(context, _))
            .evalTap(indexLog.aggregateEvent)
        }
      import scala.concurrent.duration.*

      val status = supervisor.supervise {
        indexing.flatMap { _.compile.drain } >> indexLog.processing.map(_.mkString("\n"))
      }

      IO(println("executed")) >>
        supervisor.supervise(IO(println("indexing started")) >> indexing.flatMap(_.compile.toList) >> IO("finished"))
    }

    def applyInserts(context: Context, event: Event): IO[Unit] = event match {
      case event@ Event.DocumentFound(indexable @ Indexable(component, document)) =>
        documentIndexer.indexCuriaVistaDocument(context, component, document)
      case otherwise =>
        IO.unit
    }

    override def indexEntity(entity: Search.Entity, context: Context): IO[CuriaVistaDocument] = {
      val fetchDocument =
        entity match {
          case Search.Entity.Business(id) => businessEntities(context).fetchDocument(id)
          case Search.Entity.Bill(id) => billsEntities(context).fetchDocument(id)
          case Search.Entity.Vote(id) => voteEntities(context).fetchDocument(id)
          case Search.Entity.Parliamentarian(id) => parliamentarianEntities(context).fetchDocument(id)
        }

      fetchDocument.flatMap { case Indexable(component, document) =>
        documentIndexer.indexCuriaVistaDocument(context, component, document)
          .flatTap( _ =>
            log.info(s"Indexed as $component the document $document with context $context")
          ).map(_ => document)
      }
    }


    override def buildIndexingEvents(
      context: Context,
      pageSize: PageSize,
      entityTypes: Set[Search.Entity.Type],
    ): IO[Stream[IO, Event]] = {
      val indexerByType: Map[Entity.Type, EntityIndexer[?, ?, ?, ?]] =
        Map(
          Entity.Type.Vote -> voteEntities(context),
          Entity.Type.Bill -> billsEntities(context),
          Entity.Type.Business -> businessEntities(context),
          Entity.Type.Parliamentarian -> parliamentarianEntities(context),
        )

      val componentsByType: Map[Entity.Type, Component] =
        indexerByType
          .view
          .mapValues(_.indexableDocument.component)
          .toMap

      val events =
        indexLog.processing.map { running =>
          val notRunning = componentsByType
            .filterNot { case (key, value) => running.keySet.contains(value) }.keySet

          indexerByType
            .view
            .filterKeys(entityTypes)
            .filterKeys(notRunning)
            .values
            .map(indexEvents(pageSize, _))
            .fold(Stream.empty)(_ ++ _)
        }

      events
    }
  }



  private def indexEvents[Identity](
    pageSize: PageSize,
    indexer: EntityIndexer[?, Identity, ?, ?],
  ): Stream[IO, Event] = {
    val component = indexer.indexableDocument.component
    val chunks = 10
    val emptyPage = Paging(Offset(0), PageSize(0))

    val readIndex: fs2.Stream[IO, ReadOnlyStore.Index[Identity, ?]] =
      fs2.Stream
        .eval(indexer.fetchPage(emptyPage))

    val indexMessage: Pipe[IO, ReadOnlyStore.Index[Identity, ?],  Event] = (src) =>
      src.map(index => Event.IndexRead(component, index.totalEntities))

    val documents: Pipe[IO, Int, Event] = (src) =>
      src
        .evalMap { offset =>
          indexer
            .fetchPage(Paging(Offset(offset), pageSize))
            .attempt
            .flatMap {
              case f @ Left(e) => Logger[IO].error(e)("Failure while fetching page") >> IO.fromEither(f)
              case s => Logger[IO].info("Index fetched ") >> IO.fromEither(s)
            }

        }
        .flatMap { page =>
          fs2.Stream.fromIterator[IO](page.entities.map(_._1).iterator , chunks)
        }
        .evalMap(id => indexer.fetchDocument(id).onError(error => Logger[IO].error(error)("reading document")))
        .map(indexable => Event.DocumentFound(indexable))

    readIndex
      .flatMap { index =>
        fs2.Stream.emit(Event.IndexRead(component, index.totalEntities)) ++
          fs2.Stream
            .fromIterator[IO](Range(0, index.totalEntities, pageSize.value).iterator, chunks)
            .through(documents) ++
          fs2.Stream.eval(IO.realTimeInstant).flatMap { instant =>
            fs2.Stream.emit(Event.IndexCompleted(component, ZonedDateTime.ofInstant(instant, ZoneOffset.UTC)))
          }
      }
  }


  private def indexCuriaVistaEntities[Identity, Entity, Read](
    context: Context,
    pageSize: PageSize,
    fetchPage: Paging => IO[ReadOnlyStore.Index[Identity, ?]],
    fetchDocument: Identity => IO[Indexable]
  ) = {
    val chunks = 10
    val emptyPage = Paging(Offset(0), PageSize(0))

    fs2.Stream
      .eval(fetchPage(emptyPage))
      .flatMap { index =>
        fs2.Stream.fromIterator[IO](Range(pageSize.value, index.totalEntities, pageSize.value).iterator, chunks)
      }
      .evalMap { offset =>
        fetchPage(Paging(Offset(offset), pageSize))
      }
      .flatMap { page =>
        fs2.Stream.fromIterator[IO](page.entities.unzip._1.iterator , chunks)
      }
      .evalMap(id => fetchDocument(id))
      .evalMap { case Indexable(component, document) =>
        documentIndexer
          .indexCuriaVistaDocument(context, component, document)
          .map(_ => s"Entity ${component} ($document) indexed\n")
      }
      .through(text.utf8.encode)
  }


  private def billsEntities(context: Context) =
    EntityIndexer(
      votelog.bills,
      context.language,
      (paging: Paging) =>
        IndexQueryParameters(paging, context, List((Bill.Field.BusinessId, Ascending), (Bill.Field.Title, Ascending)), Set(Bill.Field.BusinessId)),
      IndexableDocument(
        BillAsCuriaVistaDocument,
        votelog.component.v1.bill.builder
      )
    )

  private def businessEntities(context: Context) =
    EntityIndexer(
      votelog.businesses,
      context.language,
      (paging: Paging) =>
        IndexQueryParameters(paging, context, List.empty, Set.empty),
      IndexableDocument(
        BusinessAsCuriaVistaDocument,
        votelog.component.v1.business.builder
      )
    )

  def voteEntities(context: Context) =
    EntityIndexer(
      votelog.votes,
      context.language,
      (paging: Paging) => PagedIndexQueryParameters(paging, context),
      IndexableDocument(
        VoteAsCuriaVistaDocument,
        votelog.component.v1.vote.builder[Vote.Id]
      ),
    )

  def parliamentarianEntities(context: Context) =
    EntityIndexer(
      votelog.politicians,
      context.language,
      (paging: Paging) => IndexQueryParameters(paging, context, List((Parliamentarian.Field.Id, Ascending)), Set(Parliamentarian.Field.Id)),
      IndexableDocument(
        ParliamentarianAsCuriaVistaDocument,
        votelog.component.v1.parliamentarian.builder
      ),
    )
}


object SearchIndex {
  case class Configuration(indexDirectory: Option[Path])

  def build(
    configuration: Configuration,
    voteLog: VoteLog[IO],
    supervisor: Supervisor[IO],
    indexLog: IndexLog[IO],
  )(implicit logger: Logger[IO]): LuceneVoteLogIndex = {

    val lucene: Lucene = new DirectLucene(
      defaultFullTextSearchable = true,
      uniqueFields = List("component"),
      autoCommit = true,
      appendIfExists = true,
      directory = configuration.indexDirectory
    )

    val languageStringify: Stringify[Language] = new Stringify[Language] {
      override def toString(value: Language): String = value.iso639_1
      override def fromString(s: String): Language = Language.fromIso639_1Unsafe(s)
    }

    implicit val languageValueSupport: StringifyValueSupport[Language] = StringifyValueSupport(languageStringify)
    implicit val lpValueSupport: IntegerishValueSupport[LegislativePeriod.Id] = new IntegerishValueSupport[LegislativePeriod.Id](LegislativePeriod.Id.apply, _.value)

    val language = lucene.create.field[Language]("language", fullTextSearchable = true)
    val legislativePeriod = lucene.create.field[LegislativePeriod.Id]("legislativePeriod", fullTextSearchable = true)

    val component = lucene.create.field[String]("component", fieldType = FieldType.Untokenized, fullTextSearchable = true)
    val title = lucene.create.field[String]("title", fullTextSearchable = true)
    val description = lucene.create.field[String]("description", fullTextSearchable = true)

    val indexer = new LuceneDocumentIndexer[IO](lucene, component, title, description, language, legislativePeriod)
    val search = new LuceneSearch[IO](lucene, component, title, description, language)

    new LuceneVoteLogIndex(voteLog, indexer, search, supervisor, indexLog)
  }
}


class IntegerishValueSupport[T](fromInt: Int => T, toInt: T => Int) extends ValueSupport[T] {
  override def store(field: Field[T], value: T, document: Document): Unit = {
    val stored = new StoredField(field.storeName, toInt(value))
    document.add(stored)
  }

  override def filter(field: Field[T], value: T, document: Document): Unit = {
    val filtered = new IntPoint(field.filterName, toInt(value))
    document.add(filtered)
  }

  override def sorted(field: Field[T], value: T, document: Document): Unit = {
    val sorted = new NumericDocValuesField(field.sortName, toInt(value))
    document.add(sorted)
  }

  override def fromLucene(fields: List[IndexableField]): T = fromInt(fields.head.numericValue().intValue())

  override def sortFieldType: Type = SortField.Type.INT

  override def searchTerm(fv: FieldAndValue[T]): SearchTerm = new ExactIntSearchTerm(fv.field.asInstanceOf[Field[Int]], toInt(fv.value))
}
