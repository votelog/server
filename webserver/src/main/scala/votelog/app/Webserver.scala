package votelog.app

import cats.effect.*
import cats.effect.kernel.Ref
import cats.effect.std.Supervisor
import cats.effect.unsafe.implicits.*
import cats.implicits.*
import com.github.blemale.scaffeine.{AsyncLoadingCache, Scaffeine}
import org.http4s.{Header, Headers, HttpRoutes, Request, Uri}
import org.http4s.blaze.server.BlazeServerBuilder
import org.http4s.dsl.io.*
import org.http4s.headers.Origin
import org.http4s.implicits.*
import org.http4s.server.{AuthMiddleware, Router}
import org.http4s.server.middleware.{CORS, CORSConfig, CORSPolicy}
import org.reactormonk.{CryptoBits, PrivateKey}
import org.typelevel.ci.CIString
import pureconfig.{ConfigReader, ConfigSource}
import sttp.capabilities.fs2.Fs2Streams
import sttp.tapir.server.ServerEndpoint
import sttp.tapir.server.http4s.{Http4sServerInterpreter, Http4sServerOptions}
import votelog.app
import votelog.app.implementation.{CachedDecisionsStore, CachedRankings, ConfigurableRankings, Log4SLogger}
import votelog.crypto.PasswordHasher
import votelog.domain.authentication.User
import votelog.domain.authorization.{Authorization, Component, Role}
import votelog.domain.politics.Rankings.{OrganisationRankings, ParliamentarianRankings}
import votelog.domain.politics.{Context, Decision, DecisionEvaluation, DecisionsStore, Language, LegislativePeriod, Ngo, Parliamentarian, Rankings, Vote}
import votelog.app.implementation.AuthorizationHandler
import votelog.api.v1.implementation.*
import votelog.api.v1.*
import votelog.app.LuceneVoteLogIndex.IndexLog
import votelog.domain.crudi.ReadOnlyStore
import votelog.domain.organisation.OrganisationService
import votelog.domain.search.Catalog
import votelog.persistence.*
import votelog.util.Slug

import scala.concurrent.duration.DurationInt

object Webserver extends IOApp {

  implicit val log: Log4SLogger[IO] = new Log4SLogger[IO](org.log4s.getLogger)

  override def run(args: List[String]): IO[ExitCode] =
    for {
      configuration <- loadConfiguration
      indexLog <- IndexLog.init[IO]
      _ <- log.info(s"connecting to votelog db: ${configuration.votelog.database.url}")
      _ <- log.info(s"connecting to curiavista db: ${configuration.votelog.curiaVista.url}")
      voteLog = VoteLog[IO](configuration.votelog)
      runServer <- voteLog.use { voteLog =>
        Supervisor[IO].use { supervisor =>
          val routes = setupHttpRoutes(configuration, voteLog, supervisor, indexLog)

          runVotelogWebserver(configuration.http, applyCors(routes))
        }
      }
    } yield runServer

  private def runVotelogWebserver(
      config: Configuration.Http,
      routes: HttpRoutes[IO],
  ): IO[ExitCode] =
    log.info(s"attempting to bind to port ${config.port}") *>
      BlazeServerBuilder[IO]
        .bindHttp(config.port, config.interface)
        .withHttpApp(routes.orNotFound)
        .serve
        .compile
        .drain
        .as(ExitCode.Success)

  def setupHttpRoutes(
    configuration: Webserver.Configuration,
    votelog: VoteLog[IO],
    supervisor: Supervisor[IO],
    indexLog: IndexLog[IO]
  ): HttpRoutes[IO] = {

    val cachedVoteLog = buildCachedVoteLog(votelog)
    val index: Catalog[IO] = SearchIndex.build(configuration.votelog.search, votelog, supervisor, indexLog)

    val key = PrivateKey(configuration.votelog.security.secret.getBytes)
    val crypto: CryptoBits = CryptoBits(key)
    val clock = Clock[IO]

    val v1Routes = setupV1(configuration.http.domain ,cachedVoteLog, index, crypto, clock)

    v1Routes
  }

  def buildCachedVoteLog(votelog: VoteLog[IO]): VoteLog[IO] = new VoteLog[IO] {

    export votelog.votes
    export votelog.ngos
    export votelog.roles
    export votelog.users
    export votelog.bills
    export votelog.authorization
    export votelog.businesses
    export votelog.politicians
    export votelog.faction
    export votelog.parties
    export votelog.ngoService
    export votelog.passwordHasher

    private val decisionsCache: AsyncLoadingCache[(Context, Vote.Id), List[(Parliamentarian.Id, Decision)]] =
      Scaffeine()
        .maximumSize(500000)
        .buildAsyncFuture { case (context, id) =>
          votelog.decisions.decisionsForVoteRegistration(context)(id).unsafeToFuture()
        }

    private val cachedDecisions: DecisionsStore[IO] =
      new CachedDecisionsStore(votelog.decisions, decisionsCache)

    override val decisions = cachedDecisions

    override val rankings: ConfigurableRankings[IO] =
      new ConfigurableRankings(
        DecisionEvaluation.NaiveEvaluation, votelog.ngos, cachedDecisions)(using Sync[IO], Clock[IO], log)
  }


  def setupV1(
    domain: String,
    votelog: VoteLog[IO],
    index: Catalog[IO],
    crypto: CryptoBits,
    clock: Async[IO],
  ): HttpRoutes[IO] = {

    val root = votelog.component.v1
    
    val authorizationHandler = new AuthorizationHandler(crypto, votelog.users, votelog.authorization)

    val sessions = new SessionRoutes(domain, votelog.users, votelog.passwordHasher, crypto, clock, authorizationHandler)

    val parliamentarians =
      new ReadOnlyRoutes(root.parliamentarian, votelog.politicians, ParliamentariansEndpoint, authorizationHandler).servers ++
        new PersonsRoutes(root.parliamentarian, votelog.decisions, authorizationHandler).servers

    val businesses =
      new ReadOnlyRoutes(root.business, votelog.businesses, BusinessesEndpoint, authorizationHandler).servers ++
        new BusinessesRoutes(root.business, votelog.businesses, authorizationHandler).servers

    val bills = new ReadOnlyRoutes(root.bill, votelog.bills, BillsEndpoint, authorizationHandler).servers ++
      new BillsRoutes(root.bill, votelog.bills, authorizationHandler).servers

    val parties = new ReadOnlyRoutes(root.party, votelog.parties, PartiesEndpoint, authorizationHandler).servers
    val factions = new ReadOnlyRoutes(root.faction, votelog.faction, FactionsEndpoint, authorizationHandler).servers
    val votes =
      new VotesRoutes(root.vote, votelog.votes, authorizationHandler).servers ++
        new ReadOnlyRoutes(root.vote, votelog.votes, VotesEndpoint, authorizationHandler).servers

    def buildRoleComponent(id: Role.Id) = root.role.child(id.value)
    def buildUserComponent(id: User.Id): Component = root.organisation.child(id.value.toString)

    val users =
      new ReadOnlyRoutes(root.user, votelog.users, UsersEndpoint, authorizationHandler).servers ++
        new StoreRoutes(root.user, votelog.users, UsersEndpoint, authorizationHandler).servers ++
        new UserRoutes(buildRoleComponent, buildUserComponent, votelog.users, authorizationHandler).servers

    val roles =
      new ReadOnlyRoutes(root.role, votelog.roles, RolesEndpoint, authorizationHandler).servers ++
        new StoreRoutes(root.role, votelog.roles, RolesEndpoint, authorizationHandler).servers ++
        new RolesRoutes(root.role, votelog.roles, authorizationHandler).servers

    val organisations =
      new OrganisationsRoutes(root.organisation, votelog.ngos, votelog.ngoService, authorizationHandler).servers
         ++ new ReadOnlyRoutes(root.organisation, votelog.ngos, OrganisationsEndpoint, authorizationHandler).servers
         ++ new StoreRoutes(root.organisation, votelog.ngos, OrganisationsEndpoint, authorizationHandler).servers

    val rankings =
      new RankingsRoutes(root.ranking, votelog.rankings, authorizationHandler).servers

    val search = new SearchRoutes(root.search, index, authorizationHandler)

    val apiEndpoints: List[ServerEndpoint[Any & Fs2Streams[IO], IO]] = {
      sessions.servers ++
        factions ++
        businesses ++
        bills ++
        parties ++
        organisations ++
        parliamentarians ++
        rankings ++
        roles ++
        users ++
        votes ++
        search.servers
    }

    val documentationRoutes = new DocumentationRoutes(apiEndpoints)

    val serverOptions =
      Http4sServerOptions
        .customiseInterceptors[IO]
        .exceptionHandler(exceptionHandler[IO])
        .options

    val apiRoutes =
      Http4sServerInterpreter(serverOptions)
        .toRoutes(apiEndpoints ++ documentationRoutes.servers)

    apiRoutes
  }

  def applyCors(routes: HttpRoutes[IO]): HttpRoutes[IO] = {
    CORS.policy
      .withAllowOriginHeader(_ => true)
      .withAllowCredentials(true)
      .withMaxAge(10.minutes)
      .apply(routes)
  }

  import pureconfig.generic.derivation.default.*

  implicit val acr: ConfigReader[app.Configuration] = ConfigReader.derived[app.Configuration]
  implicit val chr: ConfigReader[Configuration.Http] = ConfigReader.derived[Configuration.Http]

  lazy val loadConfiguration: IO[Configuration] = {
    for {
      votelog <- IO(ConfigSource.default.at("votelog").loadOrThrow[app.Configuration])
      http <- IO(ConfigSource.default.at("webapp.http").loadOrThrow[Configuration.Http])
    } yield Configuration(votelog, http)
  }

  case class Configuration(votelog: app.Configuration, http: Configuration.Http)

  object Configuration {
    case class Http(port: Int, interface: String, domain: String)
  }
}
