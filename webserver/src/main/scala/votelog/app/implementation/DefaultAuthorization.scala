package votelog.app.implementation

import cats.Applicative
import votelog.domain.authentication.User
import votelog.domain.authorization.{Authorization, Capability, Component}

class DefaultAuthorization[F[_]: Applicative](
  defaults: List[(Component, Capability)]
) extends Authorization[F] {
  override def hasCapability(
    user: User,
    capability: Capability,
    component: Component
  ): F[Boolean] =
    Applicative[F].pure(
      defaults.exists { case (defaultComponent, capability) =>
        defaultComponent.containsOrSelf(component) && capability == capability
      }
    )
}
