package votelog.app.implementation

import cats.Applicative
import cats.effect.kernel.Sync
import votelog.infrastructure.logging.Logger

class Log4SLogger[F[_]](log: org.log4s.Logger)(implicit ev: Sync[F]) extends Logger[F] {
  override def warn(message: String): F[Unit] = ev.delay(log.warn(message))

  override def info(message: String): F[Unit] = ev.delay(log.info(message))

  override def error(t: Throwable)(message: String): F[Unit] = ev.delay(log.error(t)(message))

  override def error(message: String): F[Unit] = ev.delay(log.error(message))

  override def debug(message: String): F[Unit] = ev.delay(log.debug(message))

  override def debug(t: Throwable)(message: String): F[Unit] = ev.delay(log.debug(t)(message))

}
