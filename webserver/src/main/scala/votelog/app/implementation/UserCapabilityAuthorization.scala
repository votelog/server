package votelog.app.implementation

import cats.Applicative
import votelog.domain.authentication.User
import votelog.domain.authorization.{Authorization, Capability, Component}

class UserCapabilityAuthorization[F[_]: Applicative] extends Authorization[F] {
  def hasCapability(user: User, capability: Capability, component: Component): F[Boolean] =
    Applicative[F].pure(
      user
        .roles
        .exists(_.permits(capability, component))
    )
}
