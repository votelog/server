package votelog.app.implementation

import cats.*
import cats.implicits.*
import votelog.domain.authentication.User
import votelog.domain.authorization.{Authorization, Capability, Component}

class FallbackAuthorization[F[_]: Monad](
  primary: Authorization[F],
  secondary: Authorization[F]
) extends Authorization[F] {

    override def hasCapability(user: User, capability: Capability, component: Component
    ): F[Boolean] = primary.hasCapability(user, capability, component).flatMap {
      default =>
        if (!default) secondary.hasCapability(user, capability, component)
        else Applicative[F].pure(true)
    }
}
