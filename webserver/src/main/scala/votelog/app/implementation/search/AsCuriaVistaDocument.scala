package votelog.app.implementation.search

import votelog.domain.politics.{Bill, Business, Parliamentarian, Vote}
import votelog.domain.search.AsCuriaVistaDocument
import votelog.domain.search.Search.CuriaVistaDocument

import java.time.format.DateTimeFormatter


object ParliamentarianAsCuriaVistaDocument extends AsCuriaVistaDocument[Parliamentarian.Id, Parliamentarian] {

  def transform(id: Parliamentarian.Id, entity: Parliamentarian): CuriaVistaDocument = {
    val title = List(entity.firstName.value, entity.lastName.value).mkString(" ")
    val description =
      List(entity.canton.value).mkString(", ")
    CuriaVistaDocument(title, description, Set.empty)
  }
}

object BusinessAsCuriaVistaDocument
  extends AsCuriaVistaDocument[Business.Id, Business] {

  override def transform(id: Business.Id, entity: Business): CuriaVistaDocument = {
    val title = entity.title
    val description =
      List(
        Some(id.value),
        Some(entity.shortNumber),
        entity.description,
        entity.submittedBy,
        Some(entity.submissionDate.format(DateTimeFormatter.ISO_LOCAL_DATE)),
      ).flatten.mkString(", ")

    CuriaVistaDocument(title, description, Set.empty)
  }
}

object BillAsCuriaVistaDocument extends AsCuriaVistaDocument[Bill.Id, Bill] {

  override def transform(id: Bill.Id, entity: Bill): CuriaVistaDocument = {
    val title = entity.title
    val description =
      List(
        id.value,
        entity.submissionDate.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME),
        entity.businessId.value,
      ).mkString(", ")

    CuriaVistaDocument(title, description, Set.empty)
  }
}


object VoteAsCuriaVistaDocument extends AsCuriaVistaDocument[Vote.Id, Vote] {

  override def transform(id: Vote.Id, entity: Vote): CuriaVistaDocument = {
    val title = entity.subject.getOrElse("")

    val description =
      List(
        entity.bill.map(_.value),
        Some(entity.business.value),
        Some(entity.end.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)),
        entity.meaning.yes,
        entity.meaning.no,
      ).flatten.mkString(", ")

    CuriaVistaDocument(title, description, Set.empty)
  }
}