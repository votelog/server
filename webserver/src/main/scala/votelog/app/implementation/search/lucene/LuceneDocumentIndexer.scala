package votelog.app.implementation.search.lucene

import cats.effect.kernel.Sync
import com.outr.lucene4s.exact
import com.outr.lucene4s.Lucene
import com.outr.lucene4s.field.Field
import com.outr.lucene4s.field.value.FieldAndValue
import votelog.domain.authorization.Component
import votelog.domain.politics.{Context, Language, LegislativePeriod}
import votelog.domain.search.Search.CuriaVistaDocument

class LuceneDocumentIndexer[F[_]: Sync](
  lucene: Lucene,
  componentField: Field[String],
  titleField: Field[String],
  descriptionField: Field[String],
  languageField: Field[Language],
  legislativePeriodField: Field[LegislativePeriod.Id]
) {

  def indexCuriaVistaDocument(context: Context, component: Component, document: CuriaVistaDocument): F[Unit] = {
    Sync[F].delay {
      lucene
        .update(exact(componentField(component.location)))
        .fields(
          componentField(component.location),
          titleField(document.title),
          descriptionField(document.description),
          languageField(context.language),
          legislativePeriodField(context.legislativePeriod)
        )
        .index()
    }
  }
}
