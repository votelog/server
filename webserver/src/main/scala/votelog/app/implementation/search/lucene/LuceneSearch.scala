package votelog.app.implementation.search.lucene

import cats.effect.kernel.Sync
import com.outr.lucene4s.{exact, wildcard, fuzzy}
import com.outr.lucene4s.Lucene
import com.outr.lucene4s.field.Field
import com.outr.lucene4s.field.value.FieldAndValue
import com.outr.lucene4s.query.SearchResult
import votelog.domain.authorization.Component
import votelog.domain.crudi.ReadOnlyStore.Index
import votelog.domain.data.Paging
import votelog.domain.politics.Language
import votelog.domain.search.Search
import votelog.domain.search.Search.CuriaVistaDocument


class LuceneSearch[F[_]: Sync](
  lucene: Lucene,
  componentField: Field[String],
  title: Field[String],
  description: Field[String],
  languageField: Field[Language],
) extends Search[F] {

  private def extractDocument(result: SearchResult): (Component, CuriaVistaDocument) = {

    val titleValue = result.highlighting(title).headOption.map(_.fragment).getOrElse(result(title))
    val descriptionValue = result.highlighting(description).headOption.map(_.fragment).getOrElse(result(description))
    val languageValue = result(languageField)

    Component(result(componentField)) ->
      CuriaVistaDocument(
        titleValue,
        descriptionValue,
        Set.empty,
      )
  }

  override def retrieve(
    component: Component,
    language: Language
  ): F[Seq[CuriaVistaDocument]] = Sync[F].delay {

    lucene
      .query()
      .filter(exact(FieldAndValue(componentField, component.location)), exact(FieldAndValue(languageField, language)))
      .search()
      .entries
      .map(a => extractDocument(a)._2)
  }

  override def fulltext(language: Option[Language], paging: Paging, query: String): F[Index[String, CuriaVistaDocument]] =
    Sync[F].delay {

      val queries = query.split("\\s+").toList
      val textQueries = queries.filterNot(_.matches("[0-9]*"))
      val wildcardTerms = queries.map(query => wildcard(s"*$query*"))
      val fuzzyTerms = textQueries.map(query => fuzzy(query))
      val allTerms = wildcardTerms ++ fuzzyTerms

      val queryBuilder =
        lucene
          .query()
          .offset(paging.offset.value.toInt)
          .limit(paging.pageSize.value)
          .filter(allTerms*)

      val search = queryBuilder.highlight().search()
      val entities =
        search
          .results
          .map { result =>
            val titleValue = result.highlighting(title).headOption.map(_.fragment).getOrElse(result(title))
            val descriptionValue = result.highlighting(description).headOption.map(_.fragment).getOrElse(result(description))

            result(componentField) -> CuriaVistaDocument(titleValue, descriptionValue, Set.empty)
          }

      Index(search.total.toInt, entities.toList)
    }


}