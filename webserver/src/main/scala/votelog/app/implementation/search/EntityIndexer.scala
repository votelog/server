package votelog.app.implementation.search

import cats.effect.IO
import votelog.app.implementation.search.EntityIndexer.IndexableDocument
import votelog.domain.authorization.Component
import votelog.domain.crudi.ReadOnlyStore
import votelog.domain.crudi.ReadOnlyStore.IndexQueryParameters
import votelog.domain.data.Paging
import votelog.domain.data.Paging.PageSize
import votelog.domain.data.Sorting.Direction.Ascending
import votelog.domain.politics.{Bill, Context}
import votelog.domain.search.AsCuriaVistaDocument
import votelog.domain.search.Search.CuriaVistaDocument
import votelog.domain.search.VoteLogIndexer.Indexable

case class EntityIndexer[T, Identity, Read, IndexContext](
  store: ReadOnlyStore[IO, T, Identity, ?,  Read, IndexContext],
  read: Read,
  contextFor: (Paging) => IndexContext,
  indexableDocument: IndexableDocument[Identity, T],
) {
    def fetchDocument(
      id: Identity,
    ): IO[Indexable] = {
        store
          .read(read)(id)
          .map(indexableDocument.asIndexable(id, _))
    }

    def fetchPage(
      paging: Paging,
    ): IO[ReadOnlyStore.Index[Identity, ?]] = {
        store
          .index(contextFor(paging))
    }
}

object EntityIndexer {
  case class IndexableDocument[Identity, T](
    asDocument: AsCuriaVistaDocument[Identity, T],
    component: Component.Builder[Identity],
  ) {
    def asIndexable(id: Identity, entity: T): Indexable = {
      val document = asDocument.transform(id, entity)
      val entityComponent = component.build(id)
      Indexable(entityComponent, document)
    }
  }


}
