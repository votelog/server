package votelog.app.implementation

import cats.MonadThrow
import votelog.domain.organisation.OrganisationService
import votelog.domain.organisation.OrganisationService.Recipe
import votelog.domain.politics.Ngo
import votelog.persistence.{OrganisationStore, RoleStore, UserStore}
import cats.implicits.*
import votelog.domain.authorization.Role.Permission
import votelog.domain.authorization.{Capability, Component, Role}

import java.time.{Clock, LocalDateTime}

class OrganisationServiceImpl[F[_]: MonadThrow](
  organisationComponentRoot: Component,
  organisationStore: OrganisationStore[F],
  userStore: UserStore[F],
  roleStore: RoleStore[F],
  clock: Clock,
) extends OrganisationService[F] {

  override def createOrganisation(
    recipe: Recipe
  ): F[Ngo.Id] =
    for {
      userId <- userStore.create(UserStore.Recipe(recipe.user.name, recipe.user.email, recipe.user.password))
      id <- organisationStore.create(toStoreRecipe(recipe.organisation))
      // todo: find a better way of building child components
      organisationComponent = organisationComponentRoot.child(id.value.toString)
      roleId <- roleStore.create(Role.Template("owner", organisationComponent))
      _ <- userStore.grantRole(userId, roleId)
      _ <- roleStore.grantPermission(roleId, Permission(Capability.Create, organisationComponent))
      _ <- roleStore.grantPermission(roleId, Permission(Capability.Read, organisationComponent))
      _ <- roleStore.grantPermission(roleId, Permission(Capability.Update, organisationComponent))
      _ <- roleStore.grantPermission(roleId, Permission(Capability.Delete, organisationComponent))
  } yield id


  def toStoreRecipe(recipe: Recipe.Organisation): OrganisationStore.Recipe =
    OrganisationStore.Recipe(
      name = recipe.name,
      slug = recipe.slug,
      picture = recipe.picture,
      mail = recipe.mail,
      about = recipe.about,
      createAt = LocalDateTime.now(clock),
    )
}
