package votelog.app.implementation

import cats.effect.Sync
import cats.*
import cats.effect.kernel.Clock
import cats.implicits.*
import votelog.domain.crudi.ReadOnlyStore.IndexQueryParameters
import votelog.domain.data.Paging
import votelog.domain.data.Paging.{Offset, PageSize}
import votelog.domain.politics.DecisionEvaluation.Score
import votelog.domain.politics.Rankings.{OrganisationRankingResult, OrganisationRankings, ParliamentarianRankingResult}
import votelog.domain.politics.Vote.Registration
import votelog.domain.politics.*
import votelog.infrastructure.logging.Logger
import votelog.persistence.OrganisationStore

class ConfigurableRankings[F[_]: Sync: Clock](
  evaluation: DecisionEvaluation,
  ngoStore: OrganisationStore[F],
  decisionsStore: DecisionsStore[F]
)(implicit ev: Logger[F]) extends Rankings[F] {

  val defaultLanguage = Language.German

  def clocked[T](label: String)(w: F[T]): F[T] =
    for {
      start <- Clock[F].realTime
      w <- w
      end <- Clock[F].realTime
      _ <- Logger[F].debug(s"$label took ${(start - end).toMillis} ms")
    } yield w

  override def personRankings(
    parliamentarian: Parliamentarian.Id,
    lp: LegislativePeriod.Id
  ): F[Rankings.ParliamentarianRankings] = {

    // at one point we'll have to setup a paged fetching of ngos, but for now this works
    val ngoIndex =
      clocked("fetching organisations")(ngoStore.index(IndexQueryParameters(Paging(Offset(0), PageSize(100)), (), List.empty, Set.empty)))
    val ngoIds = ngoIndex.map(_.entities.map(_._1))

    val preferencesByOrg =
      ngoIds.flatMap { ids =>
        ids.traverse( id => ngoStore.preferences(lp, id).map(preferences => (id, preferences)))
      }

    val decisionsByContext: F[List[(Vote.Id, Parliamentarian.Id, Decision)]] =
      decisionsStore
        .decisionsByContext(Context(lp, defaultLanguage))
        .compile
        .toList

    for {
      preferencesByOrg <- preferencesByOrg
      _ <- Logger[F].info("fetching context decisions")
      decisionsByContext <- decisionsByContext
      decisions <- decisionsStore.decisionsByPerson(Context(lp, defaultLanguage))(parliamentarian)
      _ = decisionsStore.decisionsByContext(Context(lp, defaultLanguage))
      ngoIds <- ngoIds
      _ <- Logger[F].info(s"fetching decisions for ngos: $ngoIds")
    } yield {
      val contextDecisions = decisionsByContext.map(_._1).toSet

      val decisionsByVoteId =
        decisions
          .collect {
            case (id, decision) if contextDecisions.contains(id) => (id, decision)
          }.toMap

      val rankings: Map[Ngo.Id, ParliamentarianRankingResult] =
        preferencesByOrg.toMap.view.map { case (id, decisionPreferences) =>
          val preferences = decisionPreferences.toMap.view.mapValues(_.preference).toMap
          val common = decisionsByVoteId.keySet intersect preferences.keySet
          println(s"common between $parliamentarian and $id: ${common.size}: $common")
          id -> calculateRankingByNgo(preferences, decisionsByVoteId)
        }.toMap

      Rankings.ParliamentarianRankings(
        ranking = rankings,
        participated = decisionsByVoteId.size
      )
    }
  }


  def calculateRankingByNgo(
    preferences: Map[Vote.Id, Decision],
    decisions: Map[Vote.Id, Decision],
  ): ParliamentarianRankingResult = {

    val score =
      decisions.map { case (registration, decision) =>
        val maybePreference = preferences.get(registration)
        val scoreForRegistration = evaluation.scoreFor(maybePreference)(decision)
        scoreForRegistration
      }.sum

    ParliamentarianRankingResult(
      score = score,
      preferencesCount = preferences.size,
      participationCount = (preferences.keySet intersect decisions.keySet).size
    )
  }


  override def personRankingByNgo(ngo: Ngo.Id, lp: LegislativePeriod.Id): F[OrganisationRankings] = {

    val context = Context(lp, defaultLanguage)
    val contextDecisions: F[List[Vote.Id]] = decisionsStore.decisionsByContext(context).map(_._1).compile.toList

    val contextPreferences =
      for {
        contextDecisions <- contextDecisions
        preferences <- ngoStore.preferences(lp, ngo)
      } yield preferences.filter { case (id, _) => contextDecisions.contains(id) }

    contextPreferences
      .flatMap { preferences =>

        val decisions: F[List[(Parliamentarian.Id, Score)]] =
          preferences.flatTraverse { case (voteid, preference) =>
            Logger[F].debug(s"fetching decisions for $voteid" ) *>
            decisionsStore.decisionsForVoteRegistration(context)(voteid).map { decisions =>
              decisions.map { case (parliamentarian, decision) =>
                (parliamentarian, evaluation.scoreFor(Some(preference.preference))(decision))
              }
            }
          }

        decisions
          .map { resultsByPerson =>

            val rankingResults =
              resultsByPerson.groupMap { case (id, _) => id }{ case (_, result) => result}
                .view.map { case (id , results) =>
                  id -> OrganisationRankingResult(results.sum, results.size)
                }.toMap

            OrganisationRankings(
              ranking = rankingResults,
              totalPreferences = preferences.size
            )
          }
      }
  }
}
