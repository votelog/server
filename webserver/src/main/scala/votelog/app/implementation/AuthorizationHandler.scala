package votelog.app.implementation

import cats.MonadThrow
import cats.implicits.*
import org.reactormonk.CryptoBits
import votelog.app.implementation.AuthorizationHandler.Error.{CookieInvalid, Unauthorized}
import votelog.domain.authentication.{Token, User}
import votelog.domain.authorization.Role.Permission
import votelog.domain.authorization.{Authorization, Capability, Component}
import votelog.persistence.UserStore

class AuthorizationHandler[F[_]: MonadThrow](
  crypto: CryptoBits,
  userStore: UserStore[F],
  authorization: Authorization[F],
) {

  val fallbackUsername = "guest"

  def readUsername(token: Token): Either[AuthorizationHandler.Error, String] =
    for {
      username <- crypto.validateSignedToken(token.value).toRight(CookieInvalid)
    } yield username


  def fetchUser(name: String): F[Either[String, User]] =
    userStore.findByName(name).map(_.map(_._2).toRight(s"User '$name' not found"))

  def checkAuthorization(user: User, capability: Capability, component: Component): F[Unit] = {
    authorization.hasCapability(user, capability, component).flatMap {
      case true => MonadThrow[F].unit
      case false => MonadThrow[F].raiseError(Unauthorized(user.name, capability, component))
    }
  }

  def checkAuthorization(user: User, permission: Permission): F[Unit] =
    checkAuthorization(user, permission.capability, permission.component)

  def byUserWithFallback(token: Option[Token]): F[Either[String, User]] = {
    val username =
      token
        .toRight(CookieInvalid)
        .flatMap(readUsername)
        .getOrElse(fallbackUsername)

    fetchUser(username)
  }

  def byUser(token: Option[Token]): F[Either[String, User]] = {
    val username =
      token
        .toRight(CookieInvalid)
        .flatMap(readUsername)
        .left.map(_.getMessage)

    username.flatTraverse(fetchUser)
  }

  def withPermissions[I, O](
    buildPermissions: I => Set[Permission],
    f: I => F[O]
  ): User => I => F[O] = {
    user => i =>
      for {
        _ <- buildPermissions(i).toSeq.traverse(checkAuthorization(user, _))
        result <- f(i)
      } yield result
  }

  def authorizedComponent[I, O](
    capability: Capability,
    buildComponent: I => Component,
    f: I => F[O],
  ): User => I => F[O] = {
    (user: User) => (i: I) =>
      authorized(capability, buildComponent(i))(f)(user)(i)
  }

  def authorized[I, O](
    capability: Capability,
    component: Component)(
    f: I => F[O]
  ): User => I => F[O] = {
    (user: User) => (in: I) =>
      for {
        _ <- checkAuthorization(user, capability, component)
        result <- f(in)
      } yield result
  }

  def withPermissionsFromResult[I, O](
    buildPermissions: O => Set[Permission],
    f: I => F[O]
  ): User => I => F[O] = {
    user =>
      i =>
        for {
          o <- f(i)
          _ <- buildPermissions(o).toSeq.traverse(checkAuthorization(user, _))
        } yield o
  }


  def withPermissionsFromParameters[I, O](
    buildPermissions: I => Set[Permission],
    f: I => F[O]
  ): User => I => F[O] = {
    user =>
      i =>
        for {
          _ <- buildPermissions(i).toSeq.traverse(checkAuthorization(user, _))
          result <- f(i)
        } yield result
  }
}

object AuthorizationHandler {

  sealed trait Error extends Throwable

  object Error {
    case object CookieInvalid extends Error { override def getMessage: String = s"Authentication token invalid" }
    case class UnknownUser(name: String) extends Error { override def getMessage: String = s"no user '$name' found "}

    case class Unauthorized(user: String, capability: Capability, component: Component) extends Error {
      override def getMessage: String = s"User '${user}' is not authorized to '${capability.toString}' on component '${component.location}'"
    }
  }
}
