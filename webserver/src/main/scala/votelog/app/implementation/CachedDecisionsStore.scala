package votelog.app.implementation

import cats.effect.*
import com.github.blemale.scaffeine.AsyncLoadingCache
import votelog.domain.politics.{Bill, Context, Decision, DecisionsStore, Parliamentarian, Vote}

class CachedDecisionsStore(
  underlying: DecisionsStore[IO],
  decisionsCache: AsyncLoadingCache[(Context, Vote.Id), List[(Parliamentarian.Id, Decision)]],
) extends DecisionsStore[IO] {

  val ChunkSize = 100

  override def decisionsForVoteRegistration(context: Context)(vote: Vote.Id): IO[List[(Parliamentarian.Id, Decision)]] = {
    IO.fromFuture(IO(decisionsCache.get((context, vote))))
  }

  override def decisionsByPerson(context: Context)(person: Parliamentarian.Id
  ): IO[List[(Vote.Id, Decision)]] =
    underlying.decisionsByPerson(context)(person)

  override def decisionsByContext(context: Context): fs2.Stream[IO, (Vote.Id, Parliamentarian.Id, Decision)] =
    underlying.decisionsByContext(context)

}
