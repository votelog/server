package votelog.app.implementation

import cats.effect.IO
import com.github.blemale.scaffeine.AsyncLoadingCache
import votelog.domain.politics.Rankings.{OrganisationRankings, ParliamentarianRankings}
import votelog.domain.politics.{LegislativePeriod, Ngo, Parliamentarian, Rankings}

class CachedRankings(
  organisationRankingsCache: AsyncLoadingCache[(Ngo.Id, LegislativePeriod.Id), OrganisationRankings],
  parliamentarianRankingsCache: AsyncLoadingCache[(Parliamentarian.Id, LegislativePeriod.Id), ParliamentarianRankings],
) extends Rankings[IO] {
  override def personRankingByNgo(
    ngo: Ngo.Id,
    lp: LegislativePeriod.Id,
  ): IO[OrganisationRankings] = {
    IO.fromFuture(IO(organisationRankingsCache.get((ngo, lp))))
  }

  override def personRankings(
    parliamentarian: Parliamentarian.Id,
    lp: LegislativePeriod.Id
  ): IO[ParliamentarianRankings] = {
    IO.fromFuture(IO(parliamentarianRankingsCache.get((parliamentarian, lp))))
  }
}
