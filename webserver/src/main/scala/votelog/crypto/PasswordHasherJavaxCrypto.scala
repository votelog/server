package votelog.crypto

import cats.effect.Sync
import votelog.crypto.PasswordHasherJavaxCrypto.Salt

import java.util.Base64
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec

class PasswordHasherJavaxCrypto[F[_]: Sync](
  salt: Salt,
) extends PasswordHasher[F] {

  val factory: SecretKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512")

  def hashPassword(password: String): F[String] =
    Sync[F].delay {
      val spec = new PBEKeySpec(password.toCharArray, salt.value.getBytes("UTF-8"), 65536,  128)
      Base64.getEncoder.encodeToString(factory.generateSecret(spec).getEncoded)
    }
}

object PasswordHasherJavaxCrypto {
  case class Salt(value: String)
}
