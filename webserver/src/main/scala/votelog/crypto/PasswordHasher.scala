package votelog.crypto

trait PasswordHasher[F[_]] {
  def hashPassword(password: String): F[String]
}
