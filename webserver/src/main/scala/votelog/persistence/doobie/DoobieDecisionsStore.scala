package votelog.persistence.doobie

import cats.effect.Async
import cats.effect.kernel.MonadCancelThrow
import cats.{Applicative, Apply}
import votelog.domain.politics.*
import doobie.*
import doobie.implicits.*
import cats.implicits.*
import doobie.postgres.implicits.*
import doobie.util.Read
import votelog.domain.crudi.ReadOnlyStore
import votelog.domain.crudi.ReadOnlyStore.PagedIndexQueryParameters
import votelog.domain.politics.Decision.{Absent, Abstain, Excused, No, PresidentialAbstain, Yes}
import votelog.domain.politics.Vote.{Meaning, Registration}
import votelog.persistence.VoteStore
import fs2.Stream
import votelog.orphans.doobie.implicits.*

class DoobieDecisionsStore[F[_]: Async: MonadCancelThrow](
  transactor: doobie.util.transactor.Transactor[F],
) extends DecisionsStore[F] {

  // we have to keep this contained since we use a different format in the
  // votelog database
  private implicit val decisionRead: Read[Decision] =
    Read[Int]
      .map {
        case 1 => Yes
        case 2 => No
        case 3 => Abstain
        case 5 => Absent
        case 7 => PresidentialAbstain
        case 6 => Excused
        case other => sys.error(s"invalid representation for votum: $other")
      }

  override def decisionsForVoteRegistration(context: Context)(vote: Vote.Id): F[List[(Parliamentarian.Id, Decision)]] = {
    sql"""|select voting.person_number, voting.decision from voting, vote
          |where vote.language = ${context.language}
          |and vote.id_legislative_period = ${context.legislativePeriod}
          |and voting.id_vote = $vote
          |and vote.id = voting.id_vote
          |"""
      .stripMargin
      .query[(Parliamentarian.Id, Decision)]
      .accumulate[List]
      .transact(transactor)
  }

  override def decisionsByPerson(context: Context)(person: Parliamentarian.Id): F[List[(Vote.Id, Decision)]] = {
    sql"""|

          |select vote.id, voting.decision from voting, vote, business
          |where voting.person_number = ${person}
          |and vote.id = voting.id_vote
          |and vote.business_number = business.id
          |and business.submission_legislative_period = ${context.legislativePeriod}
          |and business."language" = ${context.language}
          |"""
      .stripMargin
      .query[(Vote.Id, Decision)]
      .stream
      .compile
      .toList
      .transact(transactor)
  }

  def decisionsByContext(context: Context): Stream[F, (Vote.Id, Parliamentarian.Id, Decision)] = {
    sql"""
         |select id_vote, person_number, decision from voting, vote, business
         |where vote.id = voting.id_vote
         |and vote.registration_number = voting.registration_number
         |and business.id = vote.business_number
         |and business.submission_legislative_period = ${context.legislativePeriod}
         |and business."language" = ${context.language}
         |"""
      .stripMargin
      .query[(Vote.Id, Parliamentarian.Id, Decision)]
      .stream
      .transact(transactor)
  }
}
