package votelog.persistence.doobie

import cats.*
import cats.effect.Async
import cats.implicits.*
import doobie.*
import doobie.implicits.*
import doobie.postgres.implicits.*
import votelog.domain.crudi.ReadOnlyStore.{Index, IndexQueryParameters}
import votelog.domain.data.Sorting
import votelog.domain.politics.{Decision, Language, LegislativePeriod, Ngo, Vote}
import votelog.persistence.{OrganisationStore, VoteStore}
import votelog.persistence.OrganisationStore.{DecisionPreference, Recipe}
import doobie.util.invariant.UnexpectedEnd
import votelog.domain.crudi.ReadOnlyStore.Error.UnknownId
import votelog.domain.politics.Decision.{Absent, Abstain, Excused, No, PresidentialAbstain, Yes}
import votelog.domain.politics.Vote.Registration
import votelog.orphans.doobie.implicits.*
import votelog.util.Slug

class DoobieOrganisationStore[F[_]: Async: NonEmptyParallel](
  voteLog: Transactor[F],
  voteStore: VoteStore[F]
) extends OrganisationStore[F] {

  implicit val decisionMeta: Meta[Decision] =
    Meta[String].imap {
        case "yes" => Yes
        case "no" => No
        case "abstain" => Abstain
        case "absent" => Absent
        case "presidentialAbstain" => PresidentialAbstain
        case "excused" => Excused
        case other => sys.error(s"invalid representation for votum: $other")
      }{
        case Excused => "excused"
        case Yes => "yes"
        case No => "no"
        case Abstain => "abstain"
        case Absent => "absent"
        case PresidentialAbstain => "presidentialAbstain"
    }

  private def indexQuery(qp: IndexQueryParameters[Unit, Ngo.Field, Ngo.Field]) = {

    val toFieldName: Ngo.Field => String = {
      case Ngo.Field.Name => "name"
      case Ngo.Field.Mail => "mail"
      case Ngo.Field.Slug => "slug"
      case Ngo.Field.Picture => "picture"
      case Ngo.Field.About => "about"
      case Ngo.Field.CreatedAt => "created"
    }

    def toOrderPair(field: Ngo.Field, direction: Sorting.Direction) = {
      toFieldName(field) -> direction
    }

    val orderBy = buildOrderBy(qp.orderings.filter(o => qp.fields.contains(o._1)).map(toOrderPair.tupled))

    val fields = Ngo.Field.values.map {
      field =>
        qp.fields.find( _ == field).map(toFieldName).getOrElse( field match {
        // when returning a null, the doobie checks if the non-nulled db type could be transformed to the
        // scala type. since we don't specify a concrete type when nulling, the returned type for the `created`
        // column is of type text, so doobie fails to convert the null to an Option[LocalDate]

        // for that reason, we have to make sure we return a null of type timestamp.
        case Ngo.Field.CreatedAt => s"CAST(null AS TIMESTAMP) as ${toFieldName(field)}"
        case _ => s"null as ${toFieldName(field)}"
      })
    }

    val selectFields = buildFields(fields)

    sql"select id $selectFields from organisations $orderBy"
      .query[(Ngo.Id, Ngo.Partial)]
      .accumulate[List]
  }

  override def bySlug(slug: Slug): F[(Ngo.Id, Ngo)] = {
    val idBySlug =
      sql"select id from organisations where slug = ${slug.value}"
        .query[Ngo.Id]
        .unique

    val idAndEntity =
      for {
        id <- idBySlug
        entity <- readQuery(id)
      } yield (id, entity)

    idAndEntity.transact(voteLog)
  }

  private def createQuery(recipe: Recipe): doobie.ConnectionIO[Ngo.Id] =
    sql"""|insert into organisations (name, slug, picture, mail, about, created)
          |values (${recipe.name}, ${recipe.slug}, ${recipe.picture}, ${recipe.mail}, ${recipe.about}, ${recipe.createAt})"""
      .stripMargin
      .update
      .withUniqueGeneratedKeys[Ngo.Id]("id")

  private def updateQuery(id: Ngo.Id, recipe: Recipe) =
    sql"""|update organisations
          |set name = ${recipe.name},
          |mail = ${recipe.mail},
          |slug = ${recipe.slug},
          |about = ${recipe.about}
          |where id = $id"""
      .stripMargin
      .update.run

  private def deleteQuery(id: Ngo.Id) =
    sql"delete from organisations where id = $id".update.run

  private def readQuery(id: Ngo.Id) =
    sql"select name, slug, picture, mail, about, created from organisations where id = $id".query[Ngo].unique

  private def upsertRegistrationPreference(ngoId: Ngo.Id, vote: Vote.Id, preference: Decision) =
    for {
      scored <-
        sql"select count(*) > 0 from organisations_preferred_decisions where organisationid = $ngoId and voteid = $vote"
          .query[Boolean]
          .unique
      _ <-
        if (scored)
          sql"update organisations_preferred_decisions set preference = $preference where organisationid = $ngoId and voteid = $vote".update.run
        else
          sql"insert into organisations_preferred_decisions (organisationid, voteid, preference) values ($ngoId, $vote, $preference)".update.run
    } yield ()

  private def deleteRegistrationPreferenceQuery(ngoId: Ngo.Id, vote: Vote.Id) =
    sql"""delete from organisations_preferred_decisions
         |where organisationid = $ngoId
         |and voteid = $vote"""
      .stripMargin
      .update.run


  private def selectPreferences(
    legislativePeriod: LegislativePeriod.Id,
    ngoId: Ngo.Id
  ): doobie.ConnectionIO[List[(Vote.Id, Decision)]] =
    sql"select voteid, preference from organisations_preferred_decisions where organisationid = $ngoId"
      .query[(Vote.Id, Decision)]
      .accumulate[List]

  private val countQuery = sql"select count(id) from organisations".query[Int].unique


  override def index(params: IndexQueryParameters[Unit, Ngo.Field, Ngo.Field]): F[Index[Ngo.Id, Ngo.Partial]] = {
    val count = countQuery.transact(voteLog)
    val entities = indexQuery(params).transact(voteLog)
    (count, entities).parMapN(Index.apply)
  }


  override def create(r: Recipe): F[Ngo.Id] = createQuery(r).transact(voteLog)

  override def delete(id: Ngo.Id): F[Unit] =
    deleteQuery(id)
      .transact(voteLog)
      .void

  override def update(id: Ngo.Id, r: Recipe): F[Ngo] =
    updateQuery(id, r)
      .flatMap(_ => readQuery(id))
      .transact(voteLog)

  override def read(n: Unit)(id: Ngo.Id): F[Ngo] =
    readQuery(id)
      .transact(voteLog)
      .adaptError {
        case e @ UnexpectedEnd => UnknownId(id.value.toString, e)
      }

  def fetchDecisionPreference(vote: Vote.Id, preference: Decision): F[DecisionPreference] =
    voteStore.read(Language.German)(vote).map { vote =>
      DecisionPreference(vote.business, vote.bill, preference)
    }

  override def preferences(lp: LegislativePeriod.Id, ngo: Ngo.Id): F[List[(Vote.Id, DecisionPreference)]] = {
    val preferences = selectPreferences(lp, ngo).transact(voteLog)

    for {
      preferences <- preferences
      voteIds = preferences.toMap.keySet
      votesById <- voteStore.fetchByIds(Language.German, voteIds)
    } yield {
      val votes = votesById.toMap
      preferences.flatMap  { case (voteId, preference) =>
        votes.get(voteId).map { vote =>
          voteId -> DecisionPreference(vote.business, vote.bill, preference)
        }
      }
    }
  }

  def readVotePreferenceQuery(id: Ngo.Id, vote: Vote.Id): ConnectionIO[Decision] = {
    sql"""select preference from organisations_preferred_decisions where organisationid = $id and voteid = $vote"""
      .query[Decision]
      .unique
  }

  override def updatePreference(ngo: Ngo.Id, vote: Vote.Id, preference: Decision): F[Unit] =
    upsertRegistrationPreference(ngo, vote, preference).transact(voteLog)

  override def deletePreference(ngo: Ngo.Id, vote: Vote.Id): F[Unit] =
    deleteRegistrationPreferenceQuery(ngo, vote).transact(voteLog).void

  override def readPreference(ngo: Ngo.Id, vote: Vote.Id): F[DecisionPreference] =
    readVotePreferenceQuery(ngo, vote).transact(voteLog)
      .flatMap(fetchDecisionPreference(vote, _))
}
