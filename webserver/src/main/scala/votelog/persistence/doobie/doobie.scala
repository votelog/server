package votelog.persistence

import _root_.doobie.Fragment
import _root_.doobie.util.log.*
import _root_.doobie.implicits.toSqlInterpolator
import org.log4s.*
import votelog.domain.crudi.ReadOnlyStore.IndexQueryParameters
import votelog.domain.data.Sorting

import scala.collection.immutable

package object doobie {

  val log4sLogHandler: LogHandler = {
    val logger = getLogger(getClass.getName)
    LogHandler {

      case Success(s, a, e1, e2) =>
        logger.info(s"""Successful Statement Execution:
                          |
                          |  ${s.linesIterator.dropWhile(_.trim.isEmpty).mkString("\n  ")}
                          |
                          | arguments = [${a.mkString(", ")}]
                          |   elapsed = ${e1.toMillis.toString} ms exec + ${e2.toMillis.toString} ms processing (${(e1 + e2).toMillis.toString} ms total)
          """.stripMargin)

      case ProcessingFailure(s, a, e1, e2, t) =>
        logger.error(s"""Failed Resultset Processing:
                            |
                            |  ${s.linesIterator.dropWhile(_.trim.isEmpty).mkString("\n  ")}
                            |
                            | arguments = [${a.mkString(", ")}]
                            |   elapsed = ${e1.toMillis.toString} ms exec + ${e2.toMillis.toString} ms processing (failed) (${(e1 + e2).toMillis.toString} ms total)
                            |   failure = ${t.getMessage}
          """.stripMargin)

      case ExecFailure(s, a, e1, t) =>
        logger.error(s"""Failed Statement Execution:
                            |
                            |  ${s.linesIterator.dropWhile(_.trim.isEmpty).mkString("\n  ")}
                            |
                            | arguments = [${a.mkString(", ")}]
                            |   elapsed = ${e1.toMillis.toString} ms exec (failed)
                            |   failure = ${t.getMessage}
          """.stripMargin)
    }
  }
  def makePage(p: IndexQueryParameters[?, ?, ?]): String = {
    s"""
      | LIMIT ${p.paging.pageSize}
      | OFFSET ${p.paging.offset}
      |""".stripMargin
  }

  def buildOrderBy(orderings: immutable.Seq[(String, Sorting.Direction)]): Fragment = {
    val orderBys = orderings.map {
      case (ordering, direction) =>
        ordering + " " + (direction match {
        case Sorting.Direction.Ascending => "ASC"
        case Sorting.Direction.Descending => "DESC"
      })
    }

    if (orderBys.nonEmpty) fr"ORDER BY ${mkFrag(orderBys,",")}"
    else Fragment.empty
  }

  def buildFields(fields: immutable.Seq[String]): Fragment = mkFrag(fields, ",", ",", "")
  def buildSubsequentFields(fields: immutable.Seq[String]): Fragment =  mkFrag(fields, ",", ",", "")

  def mkFrag(ts: immutable.Seq[String], sep: String): Fragment = mkFrag(ts, "", sep, "")
  def mkFrag(ts: immutable.Seq[String], start: String, sep: String, end: String): Fragment = {
    var init = Fragment.empty
    if (start.nonEmpty) init = init ++ Fragment.const(start)
    val it = ts.iterator
    if (it.hasNext) {
      init = init ++ Fragment.const(it.next())
      while (it.hasNext) {
        init = init ++ Fragment.const(s"$sep${it.next()}")
      }
    }
    if (end.nonEmpty) init = init ++ Fragment.const(end)
    init
  }
}
