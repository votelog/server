package votelog.persistence.doobie

import cats.NonEmptyParallel
import cats.effect.MonadCancelThrow
import cats.implicits.catsSyntaxMonadError
import votelog.domain.crudi.ReadOnlyStore
import votelog.domain.politics.{Context, Faction, Language}
import votelog.persistence.FactionStore
import doobie.implicits.*
import doobie.util.invariant.UnexpectedEnd
import votelog.domain.crudi.ReadOnlyStore.Error.UnknownId
import votelog.domain.crudi.ReadOnlyStore.Index
import votelog.orphans.doobie.implicits.languageMeta // this is needed

class DoobieFactionStore[F[_]: MonadCancelThrow](
  transactor: doobie.util.transactor.Transactor[F]
) extends FactionStore[F] {

  override def index(context: Context): F[ReadOnlyStore.Index[Faction.Id, Faction]] =
    sql"""|select id, parl_group_name from parl_group
          |where language = ${context.language}
    """
      .stripMargin
      .query[(Faction.Id, String)]
      .accumulate[List]
      .map { factions =>
        Index(factions.size, factions.map { case (id, name) => (id, Faction(name)) })
      }
      .transact(transactor)

  override def read(queryParameters: Language)(id: Faction.Id): F[Faction] =
    sql"""select parl_group_name from parl_group where id = ${id}"""
      .stripMargin
      .query[Faction]
      .unique
      .transact(transactor)
      .adaptError {
        case e @ UnexpectedEnd => UnknownId(id.value.toString, e)
      }
}
