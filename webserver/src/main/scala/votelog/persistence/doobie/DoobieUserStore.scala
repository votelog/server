package votelog.persistence.doobie

import cats.{Applicative, Monad, NonEmptyParallel, Traverse}
import cats.effect.{MonadCancelThrow, Sync}
import cats.free.Free
import cats.implicits.*
import doobie.free.connection
import doobie.implicits.*
import doobie.util.invariant.UnexpectedEnd
import doobie.util.meta.Meta
import votelog.crypto.PasswordHasher
import votelog.domain.authentication.User
import votelog.domain.authorization.Role.Permission
import votelog.domain.authorization.{Capability, Component, Role}
import votelog.domain.crudi.ReadOnlyStore.Error.UnknownId
import votelog.domain.crudi.ReadOnlyStore.{Index, IndexQueryParameters}
import votelog.domain.data.Sorting
import votelog.domain.data.Sorting.Direction
import votelog.domain.data.Sorting.Direction.{Ascending, Descending}
import votelog.orphans.doobie.implicits.*
import votelog.persistence.{RoleStore, UserStore}
import votelog.persistence.UserStore.{Password, PreparedRecipe, Recipe}


class DoobieUserStore[F[_]: Sync](
  transactor: doobie.util.transactor.Transactor[F],
  roleStore: DoobieRoleStore[F],
  passwordHasher: PasswordHasher[F],
) extends UserStore[F] {

  val fieldAsString: ((User.Field, Direction)) => String = {

    case (field, direction) =>
      val f = field match {
        case User.Field.Name => "name"
        case User.Field.Email => "email"
      }
      val d = direction match {
        case Ascending => "asc"
        case Descending => "desc"
      }
      s"$f $d"
  }


  implicit val readComponent: Meta[Component] =
    Meta[String]
      .imap(Component.apply)(_.location)

   def readQuery(id: User.Id): doobie.ConnectionIO[User] = {

    val selectUser: doobie.ConnectionIO[(String, String, String)] =
      sql"select name, email, passwordhash from users where id=$id"
        .query[(String, String, String)]
        .unique

    val selectRoles: doobie.ConnectionIO[List[Role.Id]] =
      sql"select roleid from user_roles where userid = $id"
        .query[Role.Id]
        .accumulate[List]

    val readRole = (id: Role.Id) =>
      (roleStore.selectRole(id), roleStore.selectPermissions(id))
        .mapN { case ((name, component), permissions) => Role(name, component, permissions) }

    for {
      nameAndEmailAndPassword <- selectUser
      (name, email, hashedPassword) = nameAndEmailAndPassword
      roleIds <- selectRoles
      roles <- roleIds.map(readRole).sequence
    } yield {
      val permissions: Set[Permission] = roles.flatMap(_.permissions).toSet
      User(name, User.Email(email), hashedPassword, permissions, roles.toSet)
    }
  }

  private def deleteQuery(id: User.Id): doobie.ConnectionIO[Int] =
    sql"delete from users where id = ${id}"
      .update.run

  private def updateQuery(id: User.Id, recipe: PreparedRecipe) =
    sql"""
         |update users set
         |name = ${recipe.name},
         |email = ${recipe.email},
         |passwordhash = ${recipe.password}
         |where id = $id
         |"""
      .stripMargin.update.run

  private def insertQuery(recipe: PreparedRecipe) = {
    sql"""
         |insert into users (name, email, passwordhash)
         |values (${recipe.name}, ${recipe.email}, ${recipe.password})"""
      .stripMargin
      .update
      .withUniqueGeneratedKeys[User.Id]("id")
  }

  def findIdByNameQuery(name: String): doobie.ConnectionIO[Option[User.Id]] =
    sql"select id from users where name = $name"
      .query[User.Id]
      .accumulate[Option]

  def indexQuery(qp: IndexQueryParameters[Unit, User.Field, User.Field]): doobie.ConnectionIO[List[(User.Id, User.Partial)]] = {

    val toFieldName: User.Field => String = {
      case User.Field.Name => "name"
      case User.Field.Email => "email"
    }

    def toOrderPair(field: User.Field, direction: Sorting.Direction) = toFieldName(field) -> direction

    val fields = User.Field.values.map {
      field =>
        qp.fields.find( _ == field).map(toFieldName).getOrElse(s"null as ${toFieldName(field)}")
    }

    val orderBy = buildOrderBy(qp.orderings.filter(o => qp.fields.contains(o._1)).map(toOrderPair.tupled))
    val selectFields = buildFields(fields)

    sql"select id $selectFields from users $orderBy"
      .query[(User.Id, User.Partial)]
      .accumulate[List]
  }

  override def create(recipe: Recipe): F[User.Id] =
    for {
      hashedPassword <- passwordHasher.hashPassword(recipe.password.value)
      updatedRecipe = recipe.prepare(Password.Hashed(hashedPassword))
      id <- insertQuery(updatedRecipe).transact(transactor)
    } yield id

  override def delete(id: User.Id): F[Unit] =
    deleteQuery(id).void.transact(transactor)

  override def update(id: User.Id, recipe: Recipe): F[User] = {

    def updateWithHashedPassword(preparedRecipe: PreparedRecipe) = {
      for {
        _ <- updateQuery(id, preparedRecipe).transact(transactor)
        p <- readQuery(id).transact(transactor)
      } yield p
    }

    val user: F[User] =
      for {
        passwordHash <- passwordHasher.hashPassword(recipe.password.value)
        preparedRecipe = recipe.prepare(Password.Hashed(passwordHash))
        p <- updateWithHashedPassword(preparedRecipe)
      } yield p

    user
  }

  override def read(unit: Unit)(id: User.Id): F[User] =
    readQuery(id)
      .transact(transactor)
      .adaptError {
        case e @ UnexpectedEnd => UnknownId(id.value.toString, e)
      }

  private def countQuery = sql"select count(id) from users".query[Int].unique

  override def index(qp: IndexQueryParameters[Unit, User.Field, User.Field]): F[Index[User.Id, User.Partial]] = {
    val entities = indexQuery(qp).transact(transactor)
    val count = countQuery.transact(transactor)

    (count, entities).mapN(Index.apply[User.Id, User.Partial])
  }

  override def findByName(name: String): F[Option[(User.Id, User)]] =
    findIdByNameQuery(name)
      .flatMap(_.map(id => readQuery(id).map(user => (id, user))).sequence)
      .transact(transactor)

  override def grantRole(
    userId: User.Id,
    roleId: Role.Id
  ): F[Unit] = {
    sql"insert into user_roles (userid, roleid) values ($userId,  $roleId)"
      .update
      .run.transact(transactor).void
  }

  override def revokeRole(
    userId: User.Id,
    roleId: Role.Id
  ): F[Unit] = {
    sql"delete from user_roles where userid = $userId and roleid = $roleId"
      .update.run.transact(transactor).void
  }

}

