package votelog.persistence.doobie

import cats.effect.MonadCancelThrow
import cats.implicits.*
import doobie.*
import doobie.implicits.*
import doobie.postgres.implicits.*
import doobie.util.invariant.UnexpectedEnd
import votelog.domain.crudi.ReadOnlyStore
import votelog.domain.crudi.ReadOnlyStore.Error.UnknownId
import votelog.domain.crudi.ReadOnlyStore.Index
import votelog.domain.data.Sorting
import votelog.domain.politics.Bill.Field.SubmissionDate
import votelog.domain.politics.{Bill, Business, Context, Language, Vote}
import votelog.domain.politics.Vote.Registration
import votelog.orphans.doobie.implicits.*
import votelog.persistence.BillStore
import votelog.persistence.doobie.DoobieBillStore.*

import java.time.LocalDateTime


class DoobieBillStore[F[_]: MonadCancelThrow](
  transactor: doobie.util.transactor.Transactor[F]
) extends BillStore[F] {

  override def index(
    qp: ReadOnlyStore.IndexQueryParameters[Context, Bill.Field, Bill.Field]
  ): F[ReadOnlyStore.Index[Bill.Id, Bill.Partial]] =
    (indexCountQuery(qp), indexQuery(qp))
      .mapN { case (count, index) =>
        Index(count, index)
    }.transact(transactor)


  def indexCountQuery(
    qp: ReadOnlyStore.IndexQueryParameters[Context, Bill.Field, Bill.Field]
  ): ConnectionIO[Int] = {
    sql"""|select count(*)
          |from bill, business
          |where bill.business_short_number = business.business_short_number
          |and bill.language = business.language
          |and bill.bill_type in (6,7,8,9)
          |and business.submission_legislative_period = ${qp.indexContext.legislativePeriod}
          |and bill.language = ${qp.indexContext.language.iso639_1.toUpperCase}
         """
      .stripMargin
      .query[Int]
      .unique
  }

  def votesQuery(language: Language, bill: Bill.Id): ConnectionIO[List[(Vote.Id, Vote.Partial)]] = {
    sql"""|with b as (
          |  select business_number, bill_number from bill where id = $bill and language = ${language}
          |)
          |select vote.id, vote.business_number, subject, meaning_yes, meaning_no, vote_end from vote, b
          |where vote.business_number = b.business_number
          |and vote.bill_number = b.bill_number
          |and vote.language = ${language}
         """
      .stripMargin
      .query[(Int, Business.Id, String, Option[String], Option[String], LocalDateTime)]
      .map { case (id, business, subject, meaningYes, meaningNo, voteEnd) =>
        Vote.Id(id) ->
          Vote.Partial(
            bill = Some(bill),
            business = Some(business),
            subject = Some(subject),
            meaning = Some(Vote.Meaning(meaningYes, meaningNo)),
            end = Some(voteEnd)
          )
      }
      .accumulate[List]
  }

  def indexQuery(
    qp: ReadOnlyStore.IndexQueryParameters[Context, Bill.Field, Bill.Field]
  ): ConnectionIO[List[(Bill.Id, Bill.Partial)]] = {
    val orderBy = buildOrderBy(qp.orderings.filter(o => qp.fields.contains(o._1)).map(toOrderPair.tupled))
    val fields = Bill.Field.values.map {
      field =>
        qp.fields.toList.find( _ == field).map(toFieldName)
          .getOrElse(field match {
            case SubmissionDate => s"CAST(null AS TIMESTAMP) as ${toFieldName(field)}"
            case _ => s"null as ${toFieldName(field)}"
          })
    }

    val selectFields = buildFields(fields)

    sql"""|select id $selectFields from (
          |  select
          |    bill.id,
          |    bill.submission_date,
          |    coalesce(vote.bill_title, bill.title) as bill_title,
          |    bill.bill_type,
          |    bill.business_number
          |  from bill
          |  left outer join (
          |    select distinct business_number, "language" , bill_number, bill_title from vote) as vote
          |	   on bill.business_number = vote.business_number
          |	   and bill.bill_number  = vote.bill_number
          |	   and bill."language" = vote."language"
          |  full join
          |    business
          |    on business.id = bill.business_number
          |    and business."language" = bill."language"
          |  where bill.bill_type in (6,7,8,9)
          |  and business.submission_legislative_period = ${qp.indexContext.legislativePeriod}
          |  and bill.language = ${qp.indexContext.language}
          |) as b
          |$orderBy
          |LIMIT ${qp.paging.pageSize}
          |OFFSET ${qp.paging.offset}
          |"""
      .stripMargin
      .query[(Bill.Id, Bill.Partial)]
      .accumulate[List]
  }

  def readQuery(language: Language, id: Bill.Id): ConnectionIO[Bill] = {
    sql"""|select
          |  coalesce(vote.bill_title, bill.title),
          |  bill.business_number,
          |  bill_type,
          |  submission_date
          |from bill
          |left outer join (
          |  select distinct business_number, "language" , bill_number, bill_title from vote) as vote
          |  on bill.business_number = vote.business_number
          |	 and bill.bill_number  = vote.bill_number
          |	 and bill."language" = vote."language"
          |where bill.id = $id
          |and bill.language=$language
          |"""
      .stripMargin
      .query[Bill]
      .unique
  }

  def votesForBill(language: Language, bill: Bill.Id): F[Map[Vote.Id, Vote.Partial]] =
    votesQuery(language, bill).transact(transactor).map(_.toMap)

  def fetchByIds(language: Language, ids: Set[Bill.Id]): F[List[(Bill.Id, Bill)]] = {

    val idsFilter: Fragment =
      ids.toList.toNel.map { ids =>
        Fragments.in(fr"AND id", ids)
      }.getOrElse(Fragment.empty)

    sql"""|select
          |  bill.id,
          |  coalesce(vote.bill_title, bill.title),
          |  bill.business_number,
          |  bill_type,
          |  submission_date
          |from bill
          |left outer join (
          |  select distinct business_number, "language" , bill_number, bill_title from vote) as vote
          |  on bill.business_number = vote.business_number
          |	 and bill.bill_number  = vote.bill_number
          |	 and bill."language" = vote."language"
          |where bill.language=$language
          |$idsFilter
          |"""
      .stripMargin
      .query[(Bill.Id, Bill)]
      .accumulate[List]
      .transact(transactor)
  }


  def read(language: Language)(id: Bill.Id): F[Bill] =
    readQuery(language, id)
      .transact(transactor)
      .adaptError {
        case e @ UnexpectedEnd => UnknownId(id.value.toString, e)
      }

}

object DoobieBillStore {
  def toOrderPair(field: Bill.Field, direction: Sorting.Direction): (String, Sorting.Direction) =
    toFieldName(field) -> direction

  val toFieldName: Bill.Field => String = {
    case Bill.Field.SubmissionDate => "submission_date"
    case Bill.Field.Title => "bill_title"
    case Bill.Field.BillType => "bill_type"
    case Bill.Field.BusinessId => "business_number"
  }
}
