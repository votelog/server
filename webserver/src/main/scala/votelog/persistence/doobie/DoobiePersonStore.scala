package votelog.persistence.doobie

import cats.effect.MonadCancelThrow
import cats.implicits.*
import votelog.domain.crudi.ReadOnlyStore.Index
import votelog.domain.politics.{Language, Parliamentarian, PersonPartial}
import votelog.persistence.PersonStore
import doobie.*
import doobie.implicits.*
import votelog.orphans.doobie.implicits.*
import votelog.domain.data.Sorting
import doobie.postgres.implicits.JavaTimeLocalDateMeta
import doobie.util.invariant.UnexpectedEnd
import votelog.domain.crudi.ReadOnlyStore.Error.UnknownId
import votelog.domain.politics.Parliamentarian.Field.{DateOfBirth, DateOfElection}

class DoobiePersonStore[F[_]: MonadCancelThrow](
  transactor: doobie.util.transactor.Transactor[F]
) extends PersonStore[F] {

  def selectQuery(id: Parliamentarian.Id, lang: Language): ConnectionIO[Parliamentarian] =
    sql"""
      SELECT
        p.id,
        p.first_name,
        p.last_name,
        c.canton_abbreviation,
        p.gender_as_string,
        mc.party,
        mc.date_election,
        p.date_of_birth,
        p.person_id_code,
        mpg.parl_group_number
      FROM  person p
      LEFT OUTER JOIN member_parl_group mpg
      ON mpg.person_number = p.person_number AND mpg.language=$lang
      left outer join member_council mc
      ON mc.person_number = p.person_number
      AND mc.language=$lang AND mc.id=$id
      and p.language=$lang
      left outer join Canton c
      on mpg.canton_number = c.canton_number
      and c.language = $lang
      where p.person_number = $id
      """
      .stripMargin
      .query[Parliamentarian]
      .unique

  def indexQuery(p: IndexParameters): doobie.ConnectionIO[List[(Parliamentarian.Id, PersonPartial)]] = {

    def toOrderPair(field: Parliamentarian.Field, direction: Sorting.Direction) = toFieldName(field) -> direction

    val orderBy = buildOrderBy(p.orderings.filter(o => p.fields.contains(o._1)).map(toOrderPair.tupled))
    val fields = Parliamentarian.Field.values.map {
      field =>
        p.fields.toList.find( _ == field)
          .map(toFieldName)
          .getOrElse(field match {
            case DateOfBirth | DateOfElection => s"CAST(null AS TIMESTAMP) as ${toFieldName(field)}"
            case _ => s"null as ${toFieldName(field)}"
          })
    }

    val selectFields = buildFields(fields)

    sql"""|
          |select person_number $selectFields from (
          |  select p.*, mpg.parl_group_number as faction_id from (
          |    with lp as (select start_date, end_date from legislative_period where id = ${p.indexContext.legislativePeriod})
          |    select distinct p.*, mc.date_election, mc.party, mc.canton_abbreviation
          |    from person p, member_council mc, lp
          |    where p.language = ${p.indexContext.language}
          |    and mc.language = ${p.indexContext.language}
          |    and council IN (1)
          |    and (mc.date_joining <= lp.end_date AND (mc.date_leaving IS NULL or mc.date_leaving >= lp.start_date))
          |    and p.person_number = mc.person_number
          |  ) as p
          |  join member_parl_group mpg on mpg.language = p.language and mpg.person_number = p.person_number
          |) as p
          |$orderBy
          |LIMIT ${p.paging.pageSize}
          |OFFSET ${p.paging.offset}
          |"""
      .stripMargin
      .query[(Parliamentarian.Id, PersonPartial)]
      .accumulate[List]
  }

  def count(p: IndexParameters): ConnectionIO[Int] = {
    sql"""|select count(*) from (
          |  select p.*, mpg.parl_group_number as faction_id from (
          |    with lp as (select start_date, end_date from legislative_period where id = ${p.indexContext.legislativePeriod})
          |    select distinct p.*, mc.party, mc.canton_abbreviation
          |    from person p, member_council mc, lp
          |    where p.language = ${p.indexContext.language}
          |    and mc.language = ${p.indexContext.language}
          |    and council IN (1)
          |    and (mc.date_joining <= lp.end_date AND (mc.date_leaving IS NULL or mc.date_leaving >= lp.start_date))
          |    and p.person_number = mc.person_number
          |  ) as p
          |  left outer join member_parl_group mpg on mpg.language = p.language and mpg.person_number = p.person_number
          |) as p
          |"""
      .stripMargin
      .query[Int]
      .unique
  }

  override def read(lang: Language)(id: Parliamentarian.Id): F[Parliamentarian] = {
    selectQuery(id, lang)
      .transact(transactor)
      .adaptError {
        case e @ UnexpectedEnd => UnknownId(id.value.toString, e)
      }
  }


  override def index(p: IndexParameters): F[Index[Parliamentarian.Id, PersonPartial]] = {
    (count(p), indexQuery(p)).mapN(Index.apply).transact(transactor)
  }

  val toFieldName: Parliamentarian.Field => String = {
    case Parliamentarian.Field.FirstName => "first_name"
    case Parliamentarian.Field.LastName => "last_name"
    case Parliamentarian.Field.Id => "id"
    case Parliamentarian.Field.DateOfBirth => "date_of_birth"
    case Parliamentarian.Field.Canton => "canton_abbreviation"
    case Parliamentarian.Field.DateOfElection => "date_election"
    case Parliamentarian.Field.Gender => "gender_as_string"
    case Parliamentarian.Field.Party => "party"
    case Parliamentarian.Field.Image => "person_id_code"
    case Parliamentarian.Field.Faction => "faction_id"
  }

}
