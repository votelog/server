package votelog.persistence.doobie


import cats.effect.kernel.MonadCancelThrow
import votelog.domain.politics.{Bill, Business, Context, Decision, DecisionsStore, Language, LegislativePeriod, Parliamentarian, Vote}
import doobie.*
import doobie.implicits.*
import cats.*
import cats.implicits.*
import doobie.postgres.implicits.*
import doobie.util.Read
import doobie.util.invariant.UnexpectedEnd
import votelog.domain.crudi.ReadOnlyStore
import votelog.domain.crudi.ReadOnlyStore.Error.UnknownId
import votelog.domain.crudi.ReadOnlyStore.PagedIndexQueryParameters
import votelog.domain.politics.Vote.{Meaning, Registration}
import votelog.persistence.VoteStore

import java.time.LocalDateTime

class DoobieVoteStore[F[_]: MonadCancelThrow](
  transactor: doobie.util.transactor.Transactor[F]
) extends VoteStore[F] {

  import votelog.orphans.doobie.implicits.*

  case class FlatVote(
    bill: Option[Bill.Id],
    business: Option[Business.Id],
    subject: Option[String],
    meaningYes: Option[String],
    meaningNo: Option[String],
    end: Option[LocalDateTime],
  ) {
    def toPartial: Vote.Partial =
      Vote.Partial(
        bill = bill,
        business = business,
        subject = subject,
        meaning = Some(Meaning(meaningYes, meaningNo)),
        end = end,
      )
  }

  implicit val votePartialRead: Read[Vote.Partial] =
    Read[FlatVote].map(_.toPartial)

  def indexCountQuery(context: Context): ConnectionIO[Int] = {
    sql"""|select count(*) from vote
          |where id_legislative_period = ${context.legislativePeriod}
          |and language = ${context.language}
          |"""
      .stripMargin
      .query[Int]
      .unique
  }

  override def index(
    params: PagedIndexQueryParameters[Context]
  ): F[ReadOnlyStore.Index[Vote.Id, Vote.Partial]] = {

    val indexQuery =
      sql"""|
            |select id, bill_id, business_number, subject, meaning_yes, meaning_no, vote_end
            |from (
            |  select vote.id, bill.id as bill_id, vote.business_number, vote.subject, vote.meaning_yes, vote.meaning_no, vote.vote_end
            |  from vote, bill
            |  where vote.business_number = bill.business_number
            |  and vote.bill_number = bill.bill_number
            |  and vote.id_legislative_period = ${params.context.legislativePeriod}
            |  and vote.language = ${params.context.language}
            |  and bill.language = ${params.context.language}
            |) as v
            |ORDER BY id
            |LIMIT ${params.paging.pageSize}
            |OFFSET ${params.paging.offset}
            |"""
        .stripMargin
        .query[(Vote.Id, Vote.Partial)]
        .accumulate[List]

    (indexCountQuery(params.context), indexQuery).mapN {
      case (totalEntities, entities) =>
        ReadOnlyStore.Index(totalEntities, entities)
    }.transact(transactor)
  }

  override def fetchByIds(
    language: Language,
    ids: Set[Vote.Id]
  ): F[List[(Vote.Id, Vote)]] = {

    val idsFilter: Fragment =
      ids.toList.toNel.map { ids =>
        Fragments.in(fr"AND id", ids)
      }.getOrElse(Fragment.empty)

    sql"""|with v as (
          |  select
          |    id,
          |    registration_number,
          |	   bill_number,
          |	   business_number,
          |	   subject,
          |	   meaning_yes,
          |	   meaning_no,
          |	   vote_end
          |	from vote
          |	where language = $language
          |	$idsFilter)
          |select v.id, registration_number, bill.id, v.business_number, v.subject, v.meaning_yes, v.meaning_no, v.vote_end
          |from v left outer join bill on bill.bill_number = v.bill_number
          |and bill.business_number = v.business_number
          |"""
      .stripMargin
      .query[(Vote.Id, Vote)]
      .accumulate[List]
      .transact(transactor)
  }

  override def read(language: Language)(id: Vote.Id): F[Vote] = {
    sql"""|with v as (
          |  select
          |   id,
          |   registration_number,
          |	  bill_number,
          |	  business_number,
          |	  subject,
          |	  meaning_yes,
          |	  meaning_no,
          |	  vote_end
          |	from vote
          |	where language = $language
          |	and id = $id)
          |select v.registration_number, bill.id, v.business_number, v.subject, v.meaning_yes, v.meaning_no, v.vote_end
          |from v left outer join bill on bill.bill_number = v.bill_number
          |and bill.business_number = v.business_number
          |and bill.language = $language
          |"""
      .stripMargin
      .query[Vote]
      .unique
      .transact(transactor)
      .adaptError {
        case e @ UnexpectedEnd => UnknownId(id.value.toString, e)
      }
  }
}
