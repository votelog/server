package votelog.persistence.doobie

import cats.effect.{Async, MonadCancelThrow}
import cats.*
import cats.implicits.*
import doobie.*
import doobie.implicits.*
import doobie.postgres.implicits.*
import votelog.domain.authorization.Role.Permission
import votelog.domain.authorization.{Component, Role}
import votelog.domain.crudi.ReadOnlyStore.Index
import votelog.domain.data.Paging
import votelog.persistence.RoleStore
import votelog.orphans.doobie.implicits.*

import cats.free.Free
import doobie.free.connection

class DoobieRoleStore[F[_]: MonadCancelThrow](
  transactor: Transactor[F]
) extends RoleStore[F] {

  override def grantPermission(
    roleId: Role.Id,
    permission: Permission,
  ): F[Unit] = {
    import permission.{component, capability}

    sql"insert into permissions (roleid, component, capability) values ($roleId, $component, $capability)"
      .update.run.transact(transactor).void
  }

  override def revokePermission(
    roleId: Role.Id,
    permission: Permission,
  ): F[Unit] = {
    import permission.{component, capability}
    sql"delete from permissions where roleid = $roleId and component = $component and capability = $capability"
      .update.run.transact(transactor).void
  }

  override def create(template: Role.Template): F[Role.Id] =
    sql"insert into roles (name, component) values (${template.name}, ${template.component})"
      .update
      .withUniqueGeneratedKeys[Role.Id]("id")
      .transact(transactor)

  override def delete(id: Role.Id): F[Unit] =
    sql"delete from roles where id = $id"
      .update
      .run
      .transact(transactor)
      .void

  override def update(
    id: Role.Id,
    template: Role.Template
  ): F[Role] = {
    for {
      _ <-
        sql"update roles set name = ${template.name}, component = ${template.component} where id = $id"
          .update
          .run
      permissions <- selectPermissions(id)
    } yield Role(template.name, template.component, permissions)
  }.transact(transactor)

  def selectPermissions(id: Role.Id): Free[connection.ConnectionOp,Set[Permission]] = {
    sql"select capability, component from permissions where roleid = $id"
      .query[Permission]
      .accumulate[List]
      .map(_.toSet)
  }

  override def index(paging: Paging): F[Index[Role.Id, String]] = {
    val entities =
      sql"SELECT id, name FROM roles LIMIT ${paging.pageSize} OFFSET ${paging.offset}"
        .query[(Role.Id, String)]
        .accumulate[List]

    val size = sql"SELECT count(*) FROM roles"
      .query[Int]
      .unique

    (size, entities)
      .mapN { case (size, entities) => Index(size, entities) }
      .transact(transactor)
  }

  def selectRole(id: Role.Id): ConnectionIO[(String, Component)] =
    sql"SELECT name, component FROM roles WHERE id = $id"
      .query[(String, Component)].unique

  override def read(queryParameters: ReadParameters)(id: Role.Id): F[Role] =
    (selectRole(id), selectPermissions(id))
      .mapN { case ((name, component), permissions) => Role(name, component, permissions) }
      .transact(transactor)
}
