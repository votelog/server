package votelog.persistence.doobie

import cats.effect.MonadCancelThrow
import cats.implicits.catsSyntaxMonadError
import votelog.domain.crudi.ReadOnlyStore
import votelog.domain.politics.{Context, Language, Party}
import votelog.persistence.PartyStore
import doobie.implicits.*
import doobie.util.invariant.UnexpectedEnd
import votelog.domain.crudi.ReadOnlyStore.Error.UnknownId
import votelog.domain.crudi.ReadOnlyStore.Index
import votelog.orphans.doobie.implicits.languageMeta

class DoobiePartyStore[F[_]: MonadCancelThrow](
  transactor: doobie.util.transactor.Transactor[F]
) extends PartyStore[F] {
  override def index(context: Context): F[ReadOnlyStore.Index[Party.Id, Party]] =
    sql"""|select id, party_name, party_abbreviation from party where language = ${context.language}"""
      .stripMargin
      .query[Party]
      .accumulate[List]
      .map { parties =>
        Index(parties.size, parties.map(_.id) zip parties)
      }
      .transact(transactor)

  override def read(language: Language)(id: Party.Id): F[Party] =
    sql"""|select id, party_name, party_abbreviation
          |from party
          |where language = $language
          |and id = ${id}
          |"""
      .stripMargin
      .query[Party]
      .unique
      .transact(transactor)
      .adaptError {
        case e @ UnexpectedEnd => UnknownId(id.value.toString, e)
      }
}
