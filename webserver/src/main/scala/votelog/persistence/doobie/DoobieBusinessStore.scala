package votelog.persistence.doobie

import cats.*
import cats.effect.MonadCancelThrow
import cats.implicits.*
import doobie.{Fragment, Fragments}
import doobie.free.connection.ConnectionIO
import doobie.implicits.*
import votelog.domain.crudi.ReadOnlyStore.Index
import votelog.domain.politics.{Bill, Business, Context, Language}
import votelog.persistence.BusinessStore
import doobie.postgres.implicits.*
import doobie.util.invariant.UnexpectedEnd
import votelog.domain.crudi.ReadOnlyStore.Error.UnknownId
import votelog.orphans.doobie.implicits.*
import votelog.domain.data.Sorting
import votelog.domain.data.Sorting.Direction.Ascending
import votelog.domain.politics.Business.Field.SubmissionDate
import votelog.persistence.doobie.DoobieBusinessStore.{toFieldName, toOrderPair}

class DoobieBusinessStore[F[_]: MonadCancelThrow: NonEmptyParallel](
  transactor: doobie.util.transactor.Transactor[F]
) extends BusinessStore[F] {

  val defaultOrdering = List((Business.Field.Id, Ascending))

  private def selectQuery(language: Language)(id: Business.Id): ConnectionIO[Business] =
    sql"""|select title, business_short_number, description, submitted_by, submission_date
          |from business where id=$id and language=$language"""
      .stripMargin
      .query[Business].unique
  

  private def fetchByIdQuery(language: Language, ids: Set[Business.Id]) = {

    val idsFilter: Fragment =
      ids.toList.toNel.map { ids =>
        Fragments.in(fr"AND id", ids)
      }.getOrElse(Fragment.empty)

    sql"""|select id, title, business_short_number, description, submitted_by, submission_date
          |from business
          |where language=$language
          |$idsFilter
          |"""
      .stripMargin
      .query[(Business.Id, Business)]
  }


  def indexQuery(qp: IndexParameters): doobie.ConnectionIO[List[(Business.Id, Business.Partial)]] = {
    val orderBy = buildOrderBy(
      (qp.orderings.filter(o => qp.fields.contains(o._1)) ++ defaultOrdering).map(toOrderPair.tupled)
    )

    val fields = Business.Field.values.map {
      field =>
        qp.fields.toList.find( _ == field).map(toFieldName)
          .getOrElse(field match {
            case SubmissionDate => s"CAST(null AS TIMESTAMP) as ${toFieldName(field)}"
            case _ => s"null as ${toFieldName(field)}"
          })
    }

    val selectFields = buildFields(fields)

    sql"""select id $selectFields from business
         |where business_type in (1,2,4,5,6,10)
         |and submission_legislative_period = ${qp.indexContext.legislativePeriod}
         |and language = ${qp.indexContext.language}
         |$orderBy
         |OFFSET ${qp.paging.offset}
         |LIMIT ${qp.paging.pageSize}
         |""".stripMargin
      .queryWithLogHandler[(Business.Id, Business.Partial)](log4sLogHandler)
      .accumulate[List]
  }

  def countQuery(qp: IndexParameters): doobie.ConnectionIO[Int] = {
    sql"""select count(*) from business
         |where business_type in (1,2,4,5,6,10)
         |and submission_legislative_period = ${qp.indexContext.legislativePeriod}
         |and language = ${qp.indexContext.language}
         |"""
      .stripMargin
      .query[Int].unique
  }

  override def read(queryParameters: ReadParameters)(id: Business.Id): F[Business] =
    selectQuery(queryParameters)(id)
      .transact(transactor)
      .adaptError {
        case e @ UnexpectedEnd => UnknownId(id.value.toString, e)
      }

  override def index(indexQueryParameters: IndexParameters): F[Index[Business.Id, Business.Partial]] = {
    val index = indexQuery(indexQueryParameters).transact(transactor)
    val count = countQuery(indexQueryParameters).transact(transactor)

    (count, index).parMapN { case (count, index) => Index[Business.Id, Business.Partial](count, index)}
  }


  override def fetchByIds(
    language: Language,
    ids: Set[Business.Id]
  ): F[List[(Business.Id, Business)]] = {
    fetchByIdQuery(language, ids)
      .accumulate[List]
      .transact(transactor)
  }


  override def billsForBusiness(context: Context, business: Business.Id): F[List[(Bill.Id, Bill.Partial)]] = {
    sql"""|select bill.id, bill.title, bill.bill_type, bill.submission_date
          |from bill, business
          |where bill.business_number = business.id
          |and business.id = ${business}
          |and bill.language = ${context.language}
          |and business.language = ${context.language}
          |and bill.bill_type in (6,7,8,9)
          |and business.submission_legislative_period = ${context.legislativePeriod}
       """
      .stripMargin
      .query[(Bill.Id, String, Bill.Type)]
      .map { case (id, title, billType) => (id, Bill.Partial(Some(title), Some(business), Some(billType), None)) }
      .accumulate[List]
      .transact(transactor)
  }
}

object DoobieBusinessStore {
  def toOrderPair(field: Business.Field, direction: Sorting.Direction): (String, Sorting.Direction) =
    toFieldName(field) -> direction

  val toFieldName: Business.Field => String = {
    case Business.Field.SubmissionDate => "submission_date"
    case Business.Field.ShortNumber => "business_short_number"
    case Business.Field.SubmittedBy => "submitted_by"
    case Business.Field.Title => "title"
    case Business.Field.Description => "description"
    case Business.Field.Id => "id"
  }
}
