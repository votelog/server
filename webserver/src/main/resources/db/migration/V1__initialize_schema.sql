CREATE TABLE public.users (
	id uuid NOT NULL DEFAULT uuid_generate_v1(),
	"name" varchar NOT NULL,
	email varchar NOT NULL,
	passwordhash varchar NOT NULL,
	CONSTRAINT users_email_key UNIQUE (email),
	CONSTRAINT users_name_key UNIQUE (name),
	CONSTRAINT users_pkey PRIMARY KEY (id)
);

CREATE TABLE public.organisations (
	id uuid NOT NULL DEFAULT uuid_generate_v1(),
	"name" varchar NOT NULL,
	about varchar NULL,
	picture varchar NULL,
	mail varchar NULL,
	created timestamp NULL,
	slug varchar NULL,
	CONSTRAINT organisations_pkey PRIMARY KEY (id)
);

CREATE TABLE public.organisations_preferred_decisions (
	organisationid uuid NOT NULL,
	voteid int4 NOT NULL,
	preference varchar NOT NULL,
	CONSTRAINT preferred_decisions_pkey PRIMARY KEY (organisationid, voteid),
	CONSTRAINT preferred_decisions_organisationid_fkey FOREIGN KEY (organisationid) REFERENCES public.organisations(id) ON DELETE CASCADE
);

CREATE TABLE public.roles (
	id uuid NOT NULL DEFAULT uuid_generate_v1(),
	"component" varchar NOT NULL,
	"name" varchar NOT NULL,
	CONSTRAINT roles_pkey PRIMARY KEY (id)
);

CREATE TABLE public.user_roles (
    userid uuid NOT NULL,
	roleid uuid NOT NULL,
	CONSTRAINT user_roles_pkey PRIMARY KEY (userid, roleid),
    CONSTRAINT user_roles_roleid_fkey FOREIGN KEY (roleid) REFERENCES public.roles(id) ON DELETE CASCADE,
    CONSTRAINT user_roles_userid_fkey FOREIGN KEY (userid) REFERENCES public.users(id) ON DELETE CASCADE
);

CREATE TABLE public.permissions (
	roleid uuid NOT NULL,
	capability varchar NOT NULL,
	component varchar NOT NULL,
	CONSTRAINT permissions_pkey PRIMARY KEY (roleid, capability, component),
    CONSTRAINT permissions_roleid_fkey FOREIGN KEY (roleid) REFERENCES public.roles(id) ON DELETE CASCADE
);
